@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Search no Speedy</h2></div>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
@endif

{{ Form::open(array('url'=>'accountid/searchpost', 'class'=>'stdform', 'method'=>'get')) }}
	<input type="text" name="accountid" class="smallinput" placeholder="masukkan no speedy" required/>
	{{ Form::submit('search') }}
{{ Form::close() }}
@stop