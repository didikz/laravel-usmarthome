<h3>Informasi Order Device Layanan Order Home Automation</h3>
<p>
Order device baru untuk layanan Home Automation telah masuk dengan data berikut.
</p>
<p>
	No Order                   : {{{ $invoice }}}<br>
	No Speedy                  : {{{ $speedy }}}<br>
	Paket                      : {{{ $paket }}}<br>
	Nama Pelanggan             : {{{ $pelanggan }}}<br>
	Alamat pelanggan           : {{{ $alamat }}}<br>
	Jumlah Device yang dipesan : {{{ $deviceCount }}} device<br><br>
</p>
<p>
Silahkan login ke dalam <a href="http://www.usmarthome.co.id/omas/public/">Order Management Tool</a> untuk melakukan proses order device selanjutnya.
</p>