<div class="vernav2 iconmenu">
    <ul>
    	<li @if(Request::segment(1)=='dashboard') {{ 'class="current"' }} @endif><a href="{{URL::to('dashboard')}}" class="icon icon-flatscreen"> Home</a></li>
    
        @for ($i=0; $i < count($nestedMenu); $i++)
            <li @if(Request::segment(1)==$nestedMenu[$i]['parentRoute']) {{ 'class="current"' }} @endif><a href="#{{{ $nestedMenu[$i]['parentRoute'] }}}">{{{ $nestedMenu[$i]['parentName'] }}}</a>
                <span class="arrow"></span>
                <ul id="{{{$nestedMenu[$i]['parentRoute'] }}}">
                    
                    @for($j=0; $j < count($nestedMenu[$i]['child']) ; $j++)

                        <li @if(Request::path()==$nestedMenu[$i]['child'][$j]['childRoute']) {{ 'class="current"' }} @endif><a href="{{{ URL::to($nestedMenu[$i]['child'][$j]['childRoute']) }}}">{{{ $nestedMenu[$i]['child'][$j]['childName'] }}}</a></li>
 
                    @endfor
                </ul>
            </li>
        @endfor

        @for($k=0; $k<count($usualMenu); $k++)
            <li @if(Request::segment(1)==$usualMenu[$k]['parentRoute']) {{ 'class="current"' }} @endif><a href="{{{ URL::to($usualMenu[$k]['parentRoute']) }}}">{{{ $usualMenu[$k]['parentName'] }}}</a></li>
        @endfor

    </ul>

    <a class="togglemenu"></a>

</div>   
