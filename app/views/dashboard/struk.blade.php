<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>struk</title>
    <script>

    	function printDiv(divName) {

         var printContents       = document.getElementById(divName).innerHTML;
         var originalContents    = document.body.innerHTML;
         
         document.body.innerHTML = printContents;

		     window.print();

		     //document.body.innerHTML = originalContents;
		}
    
    </script>
    
    <!--[if IE]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
  <body>
  <div id="printable">	
  <div style="width:800px; text-align:center;font-family:Arial, Helvetica, sans-serif; font-size:14px; border-collapse:collapse"><strong>LAYANAN USMARTHOME PT. TELKOM INDONESIA<br />
  BUKTI PEMESANAN PERANGKAT</strong></div><br>
  <table width="800" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; border-collapse:collapse" cellpadding="5" border="0"> 
  <tr>
    <td width="100" valign="top">
	<table width="400" border="0" style=" font-family:Arial, Helvetica, sans-serif; font-size:12px" cellpadding="5">
      <tr>
        <td width="93"><strong>INVOICE</strong></td>
        <td width="10">:</td>
        <td width="307">{{{ $invoice }}}</td>
      </tr>
      <tr>
        <td><strong>TANGGAL</strong></td>
        <td>:</td>
        <td>{{{ date('Y-m-d') }}}</td>
      </tr>
      <tr>
        <td><strong>PEMESANAN</strong></td>
        <td>:</td>
        <td>PERANGKAT HOME AUTOMATION</td>
      </tr>
      
    </table></td>
    <td width="367" valign="top">
	<table width="400" border="0" style=" font-family:Arial, Helvetica, sans-serif; font-size:12px" cellpadding="5">
      <tr>
        <td width="93"><strong>PELANGGAN</strong></td>
        <td width="10">:</td>
        <td width="250">{{{ $cust_name }}}</td>
      </tr>
      <tr>
        <td><strong>NO. SPEEDY</strong></td>
        <td>:</td>
        <td>{{{ $speedy }}}</td>
      </tr>
      <tr>
        <td><strong>ALAMAT</strong></td>
        <td>:</td>
        <td>{{{ $cust_address }}}</td>
      </tr>
    </table>
    <br/></td>
  </tr>
  <tr>
    <td colspan="2" valign="top" style=" height:relative;">
	<table border="1" style=" font-family:Arial, Helvetica, sans-serif; font-size:12px; border-collapse:collapse" width="823" cellpadding="5" >
		<thead>
			<tr>
  			<th style="background-color:#C4C4C4">No</th>
  			<th style="background-color:#C4C4C4">Nama Perangkat</th>
  			<th style="background-color:#C4C4C4">Harga Satuan</th>
  			<th style="background-color:#C4C4C4">Jumlah Pesan</th>
  			<th style="background-color:#C4C4C4">Total</th></tr>
		</thead>
		<tbody>
		<?php $no=1; $ordertotal = 0; ?>	
		@for($i=1; $i<= $count; $i++)
		  <tr>
		    <td>{{{ $i }}}</td>
		    <td>{{{ $nama_perangkat[$i] }}}</td>
		    <td>Rp. {{{ number_format($ordered_price[$i], 2, ',', '.') }}}</td>
		    <td>{{{ $qty[$i] }}} perangkat</td>
		    <td>Rp. {{{ number_format($ordered_price[$i] * $qty[$i], 2, ',', '.') }}}</td>
		  </tr>
    <?php 
      $ordertotal += $ordered_price[$i] * $qty[$i];
    ?>   
		@endfor
		</tbody>
		<tfoot>
			<tr>
				<td colspan="4"><center><strong>TOTAL</strong></center></td>
				<td><strong>Rp. {{{ number_format($ordertotal, 2, ',', '.') }}}</strong></td>
			</tr>
		</tfoot>
	</table>
	</td>
  </tr>
  <tr>
    <td colspan="4"><strong>Keterangan</strong>: Setelah menerima bukti pemesanan ini, segera lakukan transfer pembayaran ke nomor rekening Bank Mandiri 1240006018155 a/n PT. PINS INDONESIA. Mohon dapat menghubungi 021-34833999 untuk konfirmasi setelah melakukan transfer</td>
  </tr>
</table>
</div><br>
<input type="button" onclick="printDiv('printable')" value="Print" />
</body>
</html>