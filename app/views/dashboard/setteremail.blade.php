<h3>Informasi Instalasi Device Layanan Order Home Automation</h3>
<p>
Yth {{{ $name }}},<br>
Anda ditugaskan untuk menyiapkan kunjungan dan instalasi device di pelanggan sesuai dengan data order berikut:
</p>
<p>
	No Order                   : {{{ $invoice }}}<br>
	No Speedy                  : {{{ $speedy }}}<br>
	Nama Pelanggan             : {{{ $cust_name }}}<br>
	Alamat pelanggan           : {{{ $address }}}<br>
	Telepon Pelanggan          : {{{ $phone }}}<br>
	
</p>
<p>
Silahkan login ke dalam <a href="http://www.usmarthome.co.id/omas/public/">Order Management Tool</a> untuk melakukan proses order device selanjutnya.
</p>