<p>Notifikasi Instalasi Selesai</p>
<p>
	Data order layanan berikut, <br><br>
	No. Order: {{{ $invoice }}}<br>
	No. Speedy: {{{ $speedy }}}<br>
	Nama: {{{ $name }}}<br>
	Telepon: {{{ $phone }}}<br>

    Telah selesai dilakukan instalasi oleh setter mitra, untuk selanjutnya login ke <a href="http://www.usmarthome.co.id/omas/public">Order Management Tool</a> untuk Mapping atau melanjutkan proses order device selanjutnya.
</p>
<p>Terima kasih</p>