@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h1>Edit Device</h1></div><br>
	<form class="stdform" method="post" action="../update/{{{ $data->id_devices }}}" enctype="multipart/form-data">
		<p>
           <label>Nama Devices</label>
           <span class="field"><input type="text" name="nama_devices" class="smallinput" value="{{{ $data->nama_device }}}" required/> *)</span>
              <!--  <small class="desc">Order ID</small> -->
    	</p>
    	<p>
           <label>Spesifikasi</label>
           <span class="field"><input type="text" name="spesifikasi" class="smallinput" value="{{{ $data->spesifikasi }}}" required/> *)</span>
              <!--  <small class="desc">Order ID</small> -->
    	</p>
    	<p>
           <label>Harga</label>
           <span class="field"><input type="text" name="harga" class="smallinput" value="{{{ $data->harga }}}" required/> *)</span>
              <!--  <small class="desc">Order ID</small> -->
    	</p>
    	<p>
           <label>Jumlah Stok</label>
           <span class="field"><input type="text" name="stock" class="smallinput" value="{{{ $data->stock }}}" required/> *)</span>
              <!--  <small class="desc">Order ID</small> -->
    	</p>
    	<p>
           <label>Status</label>
           <span class="field">
           	<select name="status_device">
           	@if($data->status == 'hide')
           		<option value="hide" selected>Hide</option>
           		<option value="unhide">Unhide</option>
           	@else
           		<option value="hide">Hide</option>
           		<option value="unhide" selected>Unhide</option>
           	@endif
           	</select> *)</span>
              <!--  <small class="desc">Order ID</small> -->
    	</p>
    	<p>
           <label>Image</label>
           <span class="field">
           	@if($data->image!=='')
	   			<img src="../../deviceimages/{{{ $data->image }}}" width="100" height="100">
	   			<input type="hidden" name="image_old" value="{{{ $data->image }}}" >
	   		@else
	   			image unavailable
	   		@endif	
           	
           </span>
              <!--  <small class="desc">Order ID</small> -->
    	</p>
    	<p>
        	<label>Images upload</label>
            <span class="field">
            	<input type="file" name="image" />
            </span>
             <small class="desc">Jika tidak ada perubahan gambar, tidak perlu upload gambar baru</small>
        </p>
    	<p class="stdformbutton">
      	<button class="submit radius2">Submit</button>
        <input type="reset" class="reset radius2" value="Reset" />
    </p>
	</form>
@stop