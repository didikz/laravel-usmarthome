@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Detail Devices</h2></div><br>
<form class="stdform">
  <label>Nama Perangkat:</label>
    <span class="field">
      <input type="text" class="smallinput" value="{{{ $data->nama_device }}}" readonly/>
    </span>

  <label>Deskripsi:</label>
    <span class="field">
      <!-- <input type="text" class="longinput" value="{{{ $data->spesifikasi }}}" readonly/> -->
      <textarea>{{{ $data->spesifikasi }}}</textarea>
    </span>  

  <label>Harga:</label>
    <span class="field">
      <input type="text" class="smallinput" value="Rp. {{{ number_format($data->harga, 2, ',', '.') }}}" readonly/>
    </span>

  <label>Update Terakhir:</label>
    <span class="field">
      <input type="text" class="smallinput" value="{{{ $data->last_update }}}" readonly/>
    </span>

@if(Session::get('role')!=='AM' || Session::get('roleid')!=='5')

  <label>Status: </label>
    <span class="field">
      <input type="text" class="smallinput" value="{{{ $data->status }}}" readonly/>
    </span>
@endif

  <label>Jumlah Stock:</label>
    <span class="field">
      <input type="text" class="smallinput" value="{{{ $data->stock }}}" readonly/>
    </span>
  <p class="stdformbutton">
        <input type="button" class="stdbtn" value="Kembali" onclick="window.history.back()">
  </p>    
</form><br>

<div class="contenttitle2"><h2>Gambar Perangkat</h2></div><br>	
@if($data->image!=='')

    <center><img src="../../deviceimages/{{{ $data->image }}}" width="200" height="200"></center>
	
@else
    <center><img src="../../assets/images/noimagedefault.jpg" width="200" height="200"></center>
@endif

@stop
