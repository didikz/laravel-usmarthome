@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h1>List Devices</h1></div><br>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
@endif

@if(Session::get('role')=='mitra')
  <div><input type="button" class="stdbtn btn_lime" onClick="window.location.href='managementdevices/new'" value="New devices"></div><br><hr><br>
@endif

<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
  <thead>
      <tr>
      	 <th width="1%" class="head0 nosort"></th>
         <th width="3%" class="head1 nosort">No.</th>
         <th width="10%" class="head1">Nama Perangkat</th>
         <th width="20%" class="head1">Deskripsi</th> 
         <th width="5%" class="head1">Harga</th>
         <th width="5%" class="head1">Stock</th>
         @if(Session::get('role')!=='AM')
          <th width="5%" class="head1">Status</th>
         @endif
         <th width="10%" class="head1">Action</th>
      </tr>
  </thead>
  <?php $no=1; ?>
  @foreach($data as $device)
    
	  <tr>
		  <td></td>
		  <td>{{{ $no }}}</td>
		  <td>{{{ $device->nama_device }}}</td>
		  <td>{{{ $device->spesifikasi }}}</td>
		  <td>Rp. {{{ number_format($device->harga, 2, ',', '.') }}}</td>
		  <td @if($device->stock==0) {{ 'style="color:red;"'}} @endif>{{{ $device->stock }}}</td>
      @if(Session::get('role')!=='AM')
  		  <td>{{{ $device->status }}}</td>
      @endif
		  <td>
      @if(Session::get('role')=='mitra')  
        <a href="{{{ URL::to('managementdevices/edit/'.$device->id_devices)}}}">Edit</a> | 
		  	<a href="{{{ URL::to('managementdevices/delete/'.$device->id_devices)}}}" onClick="return confirm('Hapus data device {{{ $device->nama_device }}} ?')">Delete</a> | 
      @elseif(Session::get('role')=='AM')
        <a href="{{{ URL::to('inquiry/new/'.$device->id_devices) }}}">Inquiry order | </a>  
      @endif  
		  	<a href="{{{ URL::to('managementdevices/detail/'.$device->id_devices)}}}">Detail</a></td>
	  </tr>
	<?php $no++; ?>  
  @endforeach
  <tbody>
  </tbody>
</table>
@stop