@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>New Device</h2></div><br>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif
<form class="stdform" method="post" action="save" enctype="multipart/form-data">
	<p>
       <label>Nama Perangkat</label>
       <span class="field"><input type="text" name="nama_devices" class="smallinput" required/> *)</span>
	</p>
	<p>
       <label>Harga</label>
       <span class="field"><input type="text" name="harga" class="smallinput" required/> *)</span>
	</p>
	<p>
       <label>Jumlah Stok</label>
       <span class="field"><input type="text" name="stock" class="smallinput" required/> *)</span>
	</p>
  <p>
       <label>Deskripsi</label>
       <span class="field"><textarea name="spesifikasi" class="smallinput" required></textarea> *)</span>
  </p>
	<p>
       <label>Status</label>
       <span class="field">
       	<select name="status_device">
          <option value="unhide">Unhide</option>
       		<option value="hide">Hide</option>
       	</select> *)</span>
           <small class="desc">Unhide = available for order, Hide = Unavailable for order</small>
	</p>
	<p>
    	<label>Gambar Perangkat: </label>
        <span class="field">
        	<input type="file" name="image" />
        </span>
         <small class="desc">Gambar boleh kosong</small>
    </p>
	<p class="stdformbutton">
  	<button class="submit radius2">Submit</button>
    <input type="reset" class="reset radius2" value="Reset" />
</p>
</form>
@stop