@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Data Binding Perangkat Pelanggan</h2></div><br>
	<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
	<thead>
	    <tr>
	       <th width="5%" class="head1">No.</th>
	       <th width="10%" class="head1">Username</th>
	       <th width="10%" class="head1">No. Speedy</th>
	       <th width="10%" class="head1">Nama</th>
     	   <th width="10%" class="head1">MAC Address</th> 
	       <th width="10%" class="head1">Jumlah Perangkat</th>
	       <th width="5%" class="head1">Action</th>
	    </tr>
	</thead>
	<tbody>
	<?php $no=1; ?>
	@foreach($users as $user)
		<tr>
			<td>{{{ $no }}}</td>
			<td>{{{ $user->username }}}</td>
			<td>{{{ $user->AccountID }}}</td>
			<td>{{{ $user->CustomerName }}}</td>
			<td>{{{ $gws[$no-1] }}}</td>
			<td>{{{ $count[$no-1] }}} perangkat</td>
			<td><a href="{{{ URL::to('binding/detail/'.$user->AccountID.'/'.$rmids[$no-1]) }}}">Detail</a></td>
		</tr>
	<?php $no++; ?>
	@endforeach
		
	</tbody>
</table>
@stop