@extends('layouts.master')

@section('content')
<?php 
	  $m1=new DateTime($data->lastUpdt);
      $m2=new DateTime(date("Y-m-d H:i:s"));
      $sel = $m1->diff($m2);
      $duration = $sel->d.' hari, '.$sel->m.' bulan, '.$sel->y.' tahun';
?>
<div class="contenttitle2"><h2>Detail Data Binding</h2></div>
<form class="stdform">

   <label>No. Speedy:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $data->AccountID }}}" readonly/></span>
	
   <label>Nama Pelanggan:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $data->CustomerName }}}" readonly/></span>

   <label>Username:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $data->username }}}" readonly/></span>

   <label>Status Langganan:</label>
   <span class="field"><input type="text" class="smallinput" value="@if($data->active==1) Aktif @else Tidak aktif @endif" readonly/></span>

</form>	
<br />
<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
<thead>
    <tr>
       <th width="5%" class="head1">No.</th>
       <th width="10%" class="head1">Perangkat</th>
       <th width="10%" class="head1">MAC Address</th>
       <th width="15%" class="head1">Status Perangkat</th>
 	   <th width="10%" class="head1">Waktu Binding</th> 
    </tr>
</thead>
<tbody>
<?php $no=1; ?>	
@foreach($devices as $device)
	<tr>
		<td>{{{ $no }}}</td>
		<td>{{{ $device->dvcname }}}</td>
		<td>{{{ $device->dvcaddress }}}</td>
		<td>{{{ $device->dvcstate }}}</td>
		<td>{{{ $device->lastupd }}}</td>
	</tr>	
<?php $no++; ?>
@endforeach	
</tbody>	
</table>
	
@stop