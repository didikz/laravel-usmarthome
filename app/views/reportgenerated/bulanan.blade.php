<?php 

$totalActivation=0; $totalModification=0; $totalSuspension=0; $totalResumption=0; $totalTermination=0; $totalDaily=0; 
$i   = 0;
$ii  = 0;
$iii = 0;
$iv  = 0;
$v   = 0;
$vi  = 0;

?>
<table cellpadding='0' cellspacing='0' border='0' width="80%" class='stdtable' style='font-size:10px; padding:0;'>
<tr>
  <td colspan="{{{ $days+2 }}}"><strong>{{{ $title }}}</strong></td>
</tr>
    <thead>
    <tr>
    	<th class='head1'>Order</th>
    	@for($hari=1; $hari<=$days; $hari++)
    	<th class='head1'>{{{ $hari }}}</th>
    	@endfor
    	<th class='head1'>Total</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    	<td>ACTIVATION</td>
    	@foreach($activation as $act)
	      	<?php $totalActivation += $act; ?>
      		<td>{{{ $act }}}</td>
      		<?php $i++; ?>
      	@endforeach
      	<?php ?>
      	<td>{{{ $totalActivation }}}</td>
    </tr>
    <tr>
    	<td>MODIFICATION</td>
    	@foreach($modification as $mc)
	      	<?php $totalModification += $mc ?>
      		<td>{{{ $mc }}}</td>
      		<?php $ii++; ?>
      	@endforeach
      	<td>{{{ $totalModification }}}</td>
    </tr>
    <tr>
    	<td>SUSPENSION</td>
    	@foreach($suspension as $susp)
	      	<?php $totalSuspension += $susp ?>
      		<td>{{{ $susp }}}</td>
      		<?php $iii++; ?>
      	@endforeach
      	<td>{{{ $totalSuspension }}}</td>
    </tr>
    <tr>
    	<td>RESUMPTION</td>
    	@foreach($resumption as $res)
	      	<?php $totalResumption += $res ?>
      		<td>{{{ $res }}}</td>
      		<?php $iv++; ?>
      	@endforeach
      	<td>{{{ $totalResumption }}}</td>
    </tr>
    <tr>
    	<td>TERMINATION</td>
    	@foreach($termination as $term)
	      	<?php $totalTermination += $term ?>
      		<td>{{{ $term }}}</td>
      		<?php $v++; ?>
      	@endforeach
      	<td>{{{ $totalTermination }}}</td>
    </tr>
    <tfoot>
    <tr>
    	<th class='head1'>TOTAL</th>
    	@foreach($daily as $dailyCount)
	      	<?php $totalDaily += $dailyCount; ?>
      		<th class='head1'>{{{ $dailyCount }}}</th>
      		<?php $vi++; ?>	
      	@endforeach
      	<th class='head1'>{{{ $totalDaily }}}</th>
    </tr>
    </tfoot>	
    </tbody>
</table>