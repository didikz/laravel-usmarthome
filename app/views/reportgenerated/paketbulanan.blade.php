<table cellpadding='0' cellspacing='0' border='0' width="80%" class='stdtable' style='font-size:10px; padding:0;'>
    <tr>
        <td colspan="{{{ $data['days']+2 }}}"><strong>{{{ $title }}}</strong></td>
    </tr>
    <thead>
    <tr>
    	<th class='head1'>Paket</th>
    	@for($hari=1; $hari<=$data['days']; $hari++)
    	<th class='head1'>{{{ $hari }}}</th>
    	@endfor
    	<th class='head1'>Total</th>
    </tr>
    <tbody>
	<?php $totalPackage=0; $i= 0; ?>
	@for($packages=1; $packages <= $data['jumlahPaket']; $packages++)
    	<tr>
    		<td>{{{ $paket[$packages] }}}</td>
    		@foreach($count[$packages] as $item)
    		<?php $totalPackage += $item; $i++; ?>
	    		<td>{{{ $item }}}</td>
    		@endforeach
    		<td>{{{ $totalPackage }}}</td>
    		<?php $totalPackage=0; $i=0;?>
    	</tr>
    @endfor 
    </tbody>
</table>