<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
  <tr>
    <td colspan="14"><strong>{{{ $title }}}</strong></td>
  </tr>
  <thead>
      <tr>
         <th class="head1">Paket</th> 
         <th class="head1">Jan</th>
         <th class="head1">Feb</th>
         <th class="head1">Mar</th>
         <th class="head1">Apr</th>
         <th class="head1">Mei</th>
         <th class="head1">Jun</th>
         <th class="head1">Jul</th>
         <th class="head1">Agt</th>
         <th class="head1">Sep</th>
         <th class="head1">Okt</th>
         <th class="head1">Nov</th>
         <th class="head1">Des</th>
         <th class="head1">Total</th>
      </tr>
  </thead>
  <tbody>
  <?php $totalPackage=0; $i= 0;?>	
  @for($packages=1; $packages <= $data['jumlahPaket']; $packages++)
  	<tr>
	  	<td>{{{ $paket[$packages] }}}</td>
	  	@foreach ($count[$packages] as $item)
	  	
        <?php $totalPackage += $item; ?>
  	  	<td>
  	  		<center>{{{ $item }}}</center>
  	  	</td>
	  	<?php $i++; ?>
      @endforeach
      <?php $i=0; ?>
	  	<td> <center>{{{ $totalPackage }}}</center> </td>
	  	<?php $totalPackage=0; ?>
	</tr>
  @endfor 
  </tbody>
 </table>