<?php 
$totalActivation=0; $totalModification=0; $totalSuspension=0; $totalResumption=0; $totalTermination=0; $totalMonthly=0; 
$i   = 0;
$ii  = 0;
$iii = 0;
$iv  = 0;
$v   = 0;
$vi  = 0;
?>
<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
<tr>
  <td colspan="14"><strong>{{{ $title }}}</strong></td>
</tr>
  <thead>
      <tr>
         <th class="head1">Order</th> 
         <th class="head1">Januari</th>
         <th class="head1">Februari</th>
         <th class="head1">Maret</th>
         <th class="head1">April</th>
         <th class="head1">Mei</th>
         <th class="head1">Juni</th>
         <th class="head1">Juli</th>
         <th class="head1">Agustus</th>
         <th class="head1">September</th>
         <th class="head1">Oktober</th>
         <th class="head1">November</th>
         <th class="head1">Desember</th>
         <th class="head1">Total</th>
      </tr>
  </thead>
  <tbody>
  <tr>
  	<td><strong>ACTIVATION</strong></td>
  	@foreach($activation as $act)
    	<?php $totalActivation += $act; ?>
  		<td>{{{ $act }}}</td>
  		<?php $i++; ?>
  	@endforeach
  	<?php ?>
  	<td>{{{ $totalActivation }}}</td>
  </tr>
  <tr>
  	<td><strong>MODIFICATION</strong></td>
  	@foreach($modification as $mc)
    	<?php $totalModification += $mc ?>
  		<td>{{{ $mc }}}</td>
  		<?php $ii++; ?>
  	@endforeach
  	<td>{{{ $totalModification }}}</td>
  </tr>
  <tr>
  	<td><strong>SUSPENSION</strong></td>
  	@foreach($suspension as $susp)
    	<?php $totalSuspension += $susp ?>
  		<td>{{{ $susp }}}</td>
  		<?php $iii++; ?>
  	@endforeach
  	<td>{{{ $totalSuspension }}}</td>
  </tr>
  <tr>
  	<td><strong>RESUMPTION</strong></td>
  	@foreach($resumption as $res)
    	<?php $totalResumption += $res ?>
  		<td>{{{ $res }}}</td>
  		<?php $iv++; ?>
  	@endforeach
  	<td>{{{ $totalResumption }}}</td>
  </tr>
  <tr>
  	<td><strong>TERMINATION</strong></td>
  	@foreach($termination as $term)
    	<?php $totalTermination += $term ?>
  		<td>{{{ $term }}}</td>
  		<?php $v++; ?>
  	@endforeach
  	<td>{{{ $totalTermination }}}</td>
  </tr>
  <tfoot>
  <tr>
  	<th class='head1'><strong>TOTAL</strong></th>
  	@foreach($monthly as $monthCount)
    	<?php $totalMonthly += $monthCount; ?>
  		<th class='head1'>{{{ $monthCount }}}</th>
  		<?php $vi++; ?>	
  	@endforeach
  	<th class='head1'>{{{ $totalMonthly }}}</th>
  </tr>
  </tfoot>	
  </tbody>
</table>