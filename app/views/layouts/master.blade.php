<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>usmarthome dashboard</title>
{{ HTML::style('assets/css/style.default.css')}}
{{ HTML::style('assets/css/plugins/jquery.ui.css')}}
{{ HTML::style('assets/css/plugins/uniform.tp.css')}}

{{ HTML::script('assets/js/plugins/jquery-1.7.min.js') }}
{{ HTML::script('assets/js/plugins/jquery-ui-1.8.16.custom.min.js') }}
{{ HTML::script('assets/js/plugins/jquery.cookie.js') }}
{{ HTML::script('assets/js/plugins/jquery.dataTables.min.js') }}
{{ HTML::script('assets/js/plugins/jquery.uniform.min.js') }}
{{ HTML::script('assets/js/custom/general.js') }}
{{ HTML::script('assets/js/custom/tables.js') }}
{{ HTML::script('assets/js/custom/elements.js')}}

<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/plugins/excanvas.min.js"></script><![endif]-->
<!--[if IE 9]>
    <link rel="stylesheet" media="screen" href="css/style.ie9.css"/>
<![endif]-->
<!--[if IE 8]>
    <link rel="stylesheet" media="screen" href="css/style.ie8.css"/>
<![endif]-->
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</head>

<body class="withvernav">

<div class="bodywrapper">
    <div class="topheader">
        <div class="left">
        <div class="logo">
            {{HTML::image('assets/images/logo2.png')}}
          </div>
            
            <br clear="all" />
            
        </div><!--left-->
        
        <div class="right">
        	<!-- <div class="notification">
                <a class="count" href="ajax/notifications.html"><span>9</span></a>
        	</div> -->
            <div class="userinfo">
                {{HTML::image('assets/images/thumbs/default.png')}}
            	
                <span>{{Session::get('username')}}</span>
            </div><!--userinfo-->
            
            <div class="userinfodrop">
            	<div class="avatar">
                	<a href="#">{{HTML::image('assets/images/thumbs/defaultbig.png')}}</a>
                    
                </div><!--avatar-->
                <div class="userdata">
                    
                	<h4>{{Session::get('username')}}</h4>
                    <span class="email">{{Session::get('username')}}</span>
                    <ul>
                    	<!-- <li><a href="#">Edit Profil</a></li>
                        <li><a href="#">Account Settings</a></li>
                        <li><a href="#">Help</a></li> -->
                        <li><a href="{{URL::to('signout')}}">Sign Out</a></li>
                    </ul>
                </div><!--userdata-->
            </div><!--userinfodrop-->
        </div><!--right-->
    </div><!--topheader-->
    
    
    <div class="header">
    	<ul class="headermenu">
   
        </ul>
        
      <!--headerwidget-->
  </div>
    <!--header-->
    
   <!-- menunya disini -->
   @include('dashboard.menu')
        
    <div class="centercontent">
    
        <div class="pageheader">
            <h1 class="pagetitle">Anda login sebagai {{ Session::get('role') }}<i></i></h1>
            <span class="pagedesc"></span>
            
         </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
        
              @yield('content')

        </div> <!-- end contentwrapper -->
        	
	</div><!-- centercontent -->
    
</body>

</html>