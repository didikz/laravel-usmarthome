@extends('layouts.master')

@section('content')
<?php 
	  $m1=new DateTime($data->lastUpdt);
      $m2=new DateTime(date("Y-m-d H:i:s"));
      $sel = $m1->diff($m2);
      $duration = $sel->d.' hari, '.$sel->m.' bulan, '.$sel->y.' tahun';

    if ($data->PackageID=='1') {
		// device untuk PSB
		$paket = 'PSB';
	} elseif ($data->PackageID=='2') { //Ala carte Basic - 1
		$paket = 'Ala carte Basic - 1';
	} elseif($data->PackageID=='3') { // Ala carte Bronze - 2
		$paket = 'Ala carte Bronze - 2';
	} elseif($data->PackageID=='4') { // Ala carte Silver - 3
		$paket = 'Ala carte Silver - 3';
	} elseif($data->PackageID=='5') { // Ala carte Gold - 4
		$paket = 'Ala carte Gold - 4';
	}
?>
<div class="contenttitle2"><h2>Detail Data Pelanggan</h2></div>

<form class="stdform">

	<label>No. Speedy:</label>
	<span class="field"><input type="text" class="smallinput" value="{{{ $data->AccountID }}}" readonly/></span>

	<label>Nama Pelanggan:</label>
	<span class="field"><input type="text" class="smallinput" value="{{{ $data->CustomerName }}}" readonly/></span>

	<label>Alamat:</label>
	<span class="field"><input type="text" class="largeinput" value="{{{ $data->Address }}}" readonly/></span>

	<label>No. Telepon:</label>
	<span class="field"><input type="text" class="smallinput" value="{{{ $data->phone }}}" readonly/></span>

	<label>Username:</label>
	<span class="field"><input type="text" class="smallinput" value="{{{ $data->username }}}" readonly/></span>

	<label>Status Berlangganan:</label>
	<span class="field"><input type="text" class="smallinput" value=" @if($data->active==1)Aktif @else Tidak aktif @endif" readonly/></span>

	<label>Facebook:</label>
	<span class="field"><input type="text" name="custid" class="smallinput" value="{{{ $data->facebook }}}" readonly/></span>

	<label>Twitter:</label>
	<span class="field"><input type="text" class="smallinput" value="{{{ $data->twitter }}}" readonly/></span>

</form>
	
<br />
<div class="contenttitle2"><h2>Order History</h2></div>
<table class="stdtable">
<thead>
<tr>
<th width="5%" class="head1">No.</th>
<th width="10%" class="head1">Jenis Layanan</th>
<th width="10%" class="head1">Waktu Transaksi</th>
<th width="10%" class="head1">Paket</th>
<!--<th width="10%" class="head1">Durasi Waktu</th>
<th width="20%" class="head1">Keterangan</th>
<th width="5%" class="head1">User</th>-->
</tr>
</thead>
<tbody>
<?php $no=1; ?>	
@foreach($history as $item)
<?php
if ($item->PackageID=='1') {
	// device untuk PSB
	$paket = 'PSB';
} elseif ($item->PackageID=='2') { //Ala carte Basic - 1
	$paket = 'Ala carte Basic - 1';
} elseif($item->PackageID=='3') { // Ala carte Bronze - 2
	$paket = 'Ala carte Bronze - 2';
} elseif($item->PackageID=='4') { // Ala carte Silver - 3
	$paket = 'Ala carte Silver - 3';
} elseif($item->PackageID=='5') { // Ala carte Gold - 4
	$paket = 'Ala carte Gold - 4';
}
?>
<tr>
	<td>{{{ $no }}}</td>
	<td>{{{ $item->TransType }}}</td>
	<td>{{{ $item->lastUpdt }}}</td>
	<td>
	{{{ $paket }}}
	</td>
</tr>
<?php $no++; ?>	
@endforeach	
</tbody>
</table>

@stop
