@extends('layouts.master')

@section('content')

<div class="contenttitle2"><h2>Data Pelanggan {{{ $data['type'] }}}</h2></div><br>
  
<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
  <thead>
      <tr>
         <th width="5%" class="head1">No.</th>
         <th width="14%" class="head1">Nama</th>
         <th width="10%" class="head1">Account ID</th>
         <th width="5%" class="head1">Username</th>
         <th width="14%" class="head1">Alamat</th>
         <th width="10%" class="head1">MAC</th>
         <th width="10%" class="head1">Updated</th>
         <th width="10%" class="head1">Action</th>
      </tr>
  </thead>
  <tbody>
  <?php $no=1; ?>
  @foreach($customers as $customer)
  	<tr>
  		<td>{{{ $no }}}</td>
  		<td>{{{ $customer->CustomerName }}}</td>
  		<td>{{{ $customer->AccountID }}}</td>
  		<td>{{{ $customer->username }}}</td>
  		<td>{{{ $customer->alamat }}}</td>
  		<td>{{{ $mac[$no-1] }}}</td>
  		<td>{{{ $customer->updatedBy }}}</td>
  		<td><a href="{{{ URL::to('pelanggan/detail/'.$customer->AccountID) }}}">Detail</a></td>
  	</tr>
  <?php $no++; ?>	
  @endforeach	
  </tbody>
</table>
@stop