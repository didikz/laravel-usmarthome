@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>List Setter</h2></div>
<br>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div><br>
@endif
	<div><input type="button" class="stdbtn btn_lime" onClick="window.location.href='setter/new'" value="New Setter"></div><br><hr>
	<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
      <thead>
          <tr>
             <th width="1%" class="head1">No.</th>
             <th width="8%" class="head1">Nama</th>
             <th width="8%" class="head1">Email</th>
             <th width="12%" class="head1">Alamat</th>
             <th width="5%" class="head1">Telepon</th>
             <th width="5%" class="head1">Username</th>
             <th width="10%" class="head1">Last Update</th>
             <th width="10%" class="head1">Action</th>
          </tr>
      </thead>
      <tbody>
      	<?php $no=1; ?>
      @foreach($data as $setter)
      	<tr>
      		<td>{{{ $no }}}</td>
      		<td>{{{ $setter->setter_name }}}</td>
      		<td>{{{ $setter->email }}}</td>
      		<td>{{{ $setter->setter_address }}}</td>
      		<td>{{{ $setter->setter_phone }}}</td>
      		<td>{{{ $setter->username }}}</td>
      		<td>{{{ $setter->last_update }}}</td>
      		<td><a href="{{{ URL::to('setter/edit/'.$setter->id_setter)}}}">Edit</a> | 
      			<a href="{{{ URL::to('setter/delete/'.$setter->id_setter) }}}" onClick="return confirm('Apakah anda akan menghapus Setter {{{ $setter->setter_name }}}?')">Delete</a> | 
      			<a href="{{{ URL::to('setter/detail/'.$setter->id_setter) }}}">Detail</a></td>
      	</tr>
      	<?php $no++; ?>
      @endforeach	
      </tbody>
    </table>  	
@stop