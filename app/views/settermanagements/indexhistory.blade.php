@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>History Setter</h2></div>
<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
  <thead>
      <tr>
      	<th width="1%" class="head1"></th>
         <th width="5%" class="head1">No.</th>
         <th width="20%" class="head1">Nama Setter</th> 
         <th width="20%" class="head1">Alamat</th>
         <th width="20%" class="head1">Telpon</th>
         <th width="10%" class="head1">Total Mapping</th>
         <th width="10%" class="head1">Action</th>
      </tr>
  </thead>
  <tbody>
  <?php $no=1; ?>
  @foreach ($data as $setter)
  	<tr>
  		<td></td>
  		<td>{{{ $no }}}</td>
  		<td>{{{ $setter->setter_name }}}</td>
  		<td>{{{ $setter->setter_address }}}</td>
  		<td>{{{ $setter->setter_phone }}}</td>
  		<td>{{{ $count[$no-1] }}} order</td>
  		<td><a href="{{{ URL::to('setter/history/'.$setter->id_setter)}}}">Detail</a></td>
  	</tr>
  <?php $no++; ?>	
  @endforeach	
  </tbody>
</table>      	
@stop