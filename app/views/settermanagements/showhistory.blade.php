@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Detail Setter History</h2></div>
<div id="updates" class="subcontent">
	<div class="notibar announcement">
		 <p><strong>Nama Setter: </strong> {{{ $master['setter_name'] }}}&nbsp; &nbsp; &nbsp; 
		  <strong>Alamat: </strong>  {{{ $master['setter_address'] }}}&nbsp; &nbsp; &nbsp; 
		  <strong>Telepon: </strong> {{{ $master['setter_phone'] }}}</p>
	</div>
</div>
<div>
<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
	<thead>
		<tr>
			<th width="1%" class="head1"></th>
			 <th width="1%" class="head1">No.</th>
			 <th width="10%" class="head1">MAC Address Gateway</th>
			 <th width="10%" class="head1">No. Speedy</th>
			 <th width="10%" class="head1">Tanggal</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; ?>
	@foreach ($data as $history)
		<tr>
			<td></td>
			<td>{{{ $no }}}</td>
			<td>{{{ $history->gwaddress }}}</td>
			<td>{{{ $history->AccountID }}}</td>
			<td>{{{ $history->last_update }}}</td>
		</tr>
	<?php $no++; ?>	
	@endforeach
	</tbody>
</table>	
</div>
@stop