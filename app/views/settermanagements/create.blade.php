@extends('layouts.master')

@section('content')
<div class="contenttitle2">
	<h1>New Setter</h1><br>
</div>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
@endif
<form class="stdform" method="post" action="save" />
	<p>
       <label>Nama Setter</label>
       <span class="field"><input type="text" name="settername" class="smallinput" value="{{{ Input::old('settername') }}}" required/> *)</span>
          <!--  <small class="desc">Order ID</small> -->
    </p>
    <p>
       <label>Email</label>
       <span class="field"><input type="text" name="email" class="smallinput" value="{{{ Input::old('email') }}}" required/> *)</span>
          <!--  <small class="desc">Order ID</small> -->
    </p>
    <p>
       <label>Alamat</label>
       <span class="field"><input type="text" name="setteraddress" class="longinput" value="{{{ Input::old('setteraddress') }}}" required/> *)</span>
          <!--  <small class="desc">Order ID</small> -->
    </p>
    <p>
       <label>Telpon</label>
       <span class="field"><input type="text" name="setterphone" class="smallinput" value="{{{ Input::old('setterphone') }}}" required/> *)</span>
          <!--  <small class="desc">Order ID</small> -->
    </p>
    <p>
       <label>Username setter</label>
       <span class="field"><input type="text" name="setterusername" class="smallinput" required/> *)</span>
          <!--  <small class="desc">Order ID</small> -->
    </p>
    <p>
       <label>Password setter</label>
       <span class="field"><input type="password" name="setterpassword" class="smallinput" required/> *)</span>
          <!--  <small class="desc">Order ID</small> -->
    </p>
    <p class="stdformbutton">
      	<button class="submit radius2">Submit</button>
        <input type="reset" class="reset radius2" value="Reset" />
    </p>	
</form>
@stop