@extends('layouts.master')

@section('content')
<div class="contenttitle2">
	<h1>Edit Setter</h1><br>
</div>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
@endif
<form class="stdform" method="post" action="../update/{{{ $data->id_setter }}}" />
	<p>
       <label>Nama Setter</label>
       <span class="field"><input type="text" name="settername" class="smallinput" value="{{{ $data->setter_name }}}" required/> *)</span>
          <!--  <small class="desc">Order ID</small> -->
    </p>
    <p>
       <label>Email</label>
       <span class="field"><input type="text" name="email" class="smallinput" value="{{{ $data->email }}}" required/> *)</span>
          <!--  <small class="desc">Order ID</small> -->
    </p>
    <p>
       <label>Alamat</label>
       <span class="field"><input type="text" name="setteraddress" class="longinput" value="{{{ $data->setter_address }}}" required/> *)</span>
          <!--  <small class="desc">Order ID</small> -->
    </p>
    <p>
       <label>Telpon</label>
       <span class="field"><input type="text" name="setterphone" class="smallinput" value="{{{ $data->setter_phone }}}" required/> *)</span>
          <!--  <small class="desc">Order ID</small> -->
    </p>
    <p>
       <label>Username setter</label>
       <span class="field"><input type="text" name="setterusername" class="smallinput" value="{{{ $data->username }}}" readonly/></span>
           <small class="desc">username tidak dapat diganti</small>
    </p>
    <p>
       <label>Password setter</label>
       <span class="field"><input type="password" name="setterpassword" class="smallinput"/></span>
           <small class="desc">kosongkan password jika tidak ada perubahan</small>
    </p>
    <p class="stdformbutton">
      	<button class="submit radius2">Submit</button>
        <input type="reset" class="reset radius2" value="Reset" />
    </p>	
</form>
@stop