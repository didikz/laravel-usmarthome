@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Detail Setter</h2></div>
<form class="stdform">
  
   <label>Nama Setter</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $data->setter_name }}}" readonly/></span>

   <label>Email</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $data->email }}}" readonly/></span>

   <label>Alamat</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $data->setter_address }}}" readonly/></span>

   <label>Telpon</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $data->setter_phone }}}" readonly/></span>

   <label>Username setter</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $data->username }}}" readonly/></span>
        
</form>

@stop
