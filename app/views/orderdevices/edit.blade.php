@extends('layouts.master')

@section('content')

<div class="contenttitle2"><h2>Edit Order Device</h2></div>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif

<form class="stdform" method="post" action="{{ URL::to('orderdevices/update/'.$invoice) }}" />
	<input type="hidden" name="kuota" value="{{ $kuota }}" />
	<input type="hidden" name="type" value="{{ $order->OrderType }}" />
   <p>
     <label>No Speedy:</label>
     <span class="field"><input type="text" name="custid" class="smallinput" value="{{{ $order->cust_id }}}" readonly/></span>
  </p>
  <p>
     <label>Jenis Pelanggan:</label>
     <span class="field">
     @if($order->cust_type == 'pelanggan')
        Pelanggan <input type="radio" value="pelanggan" name="cust_type" checked />
        Dinas <input type="radio" value="dinas" name="cust_type" />
     @else
        Pelanggan <input type="radio" value="pelanggan" name="cust_type" />
        Dinas <input type="radio" value="dinas" name="cust_type" checked />
     @endif   
     </span>
  </p>
  <p>
     <label>Nama Pelanggan:</label>
     <span class="field"><input type="text" name="cust_name" class="smallinput" value="{{{ $order->cust_name }}}" readonly/>  *)</span>
  </p>
  <p>
     <label>Alamat Pemasangan:</label>
     <span class="field"><input type="text" name="alamat" class="largeinput" value="{{{ $order->alamat }}}" readonly/>  *)</span>
  </p>
  <p>
     <label>Telepon Pelanggan:</label>
     <span class="field"><input type="text" name="cust_phone" class="smallinput" value="{{{ $order->cust_phone }}}" required/>  *)</span>
  </p>
  <p>
     <label>Developer Company:</label>
     <span class="field"><input type="text" name="developer_company" class="smallinput" value="{{{ $order->dev_company }}}" /></span>
  </p>
  <p>
     <label>Developer Name:</label>
     <span class="field"><input type="text" name="developer_name" class="smallinput" value="{{{ $order->dev_name }}}"/ ></span>
  </p>
  <p>
     <label>Developer Telp:</label>
     <span class="field"><input type="text" name="developer_telp" class="smallinput" value="{{{ $order->dev_telp }}}"/></span>
  </p>
  <p>
     <label>Developer Email:</label>
     <span class="field"><input type="text" name="developer_email" class="smallinput" value="{{{ $order->dev_email }}}"/></span>
  </p>
  <!-- <p> -->
     <label>Pilih Device: </label>
     <span class="field">
     	
     	<table width="500">
     		<tr>
     			<th width="20%">Nama Perangkat</th>
          <th width="5%">Harga</th>
     			<th width="5%">Jumlah Order</th>
     			<th width="5%">Stock Tersedia</th>
              
     		</tr>
     		<?php $no=1; ?>
     		@foreach($devices as $device)
     		<tr>
     			<input type="hidden" name="id_device_{{{ $no }}}" value="{{{ $device->id_devices }}}">
     			<input type="hidden" name="namedevice_{{{ $no }}}" value="{{{ $device->nama_device }}}" />
     			<input type="hidden" name="oldOrder_{{{ $no }}}" value="{{{ $orderedDevice[$no-1] }}}">
     			<td><a href="{{{ URL::to('managementdevices/detail/'.$device->id_devices)}}}">{{{ $device->nama_device }}}</a></td>
          <td>Rp. {{{ number_format($device->harga, 2, ',', '.') }}}</td>
     			<td><input type="text" name="jumlah_{{{ $no }}}" size="1" placeholder="0" value="{{{ $orderedDevice[$no-1] }}}"></td>
     			<td><center>{{{ $device->stock }}}</center></td>
              
     		</tr>
     		<?php $no++; ?>
     		@endforeach
     	</table>
     	<!-- </select> -->
     </span>
     <small class="desc">Jumlah device yang bisa anda masukkan adalah <font style="color:red;"> {{{ $kuota }}} device.</font> device. Khusus order aktivasi, wireless gateway harus masuk dalam perangkat yang dipesan (1 Perangkat) </small>
         
  <!-- </p> -->
   <p>
     <label>Keterangan:</label>
     <span class="field"><input type="text" name="keterangan" class="largeinput" /></span>
  </p>
  <p class="stdformbutton">
    	<button class="submit radius2">Update</button>
      <input type="reset" class="reset radius2" value="Reset" />
  </p>
   </form>

@stop
