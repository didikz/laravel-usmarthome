@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>New Order Devices</h2></div>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
@endif

<form class="stdform" method="post" action="{{ URL::to('orderdevices/alacarteconfirm') }}" />
      <input type="hidden" name="transid" value="{{{ $fab->TransId }}}">
      <input type="hidden" name="idfab" value="{{{ $fab->id }}}">
      <input type="hidden" name="kuota" value="@if($delta!==0) {{{ $delta }}} @else {{{ $kuota }}} @endif">
      <input type="hidden" name="idbooking" value="" />
   
	   <p>
         <label>No Speedy:</label>
         <span class="field"><input type="text" name="custid" class="smallinput" value="{{{ $fab->AccountID }}}" readonly/></span>
      </p>
      <p>
         <label>Tipe Order:</label>
         <span class="field"><input type="text" name="type" class="smallinput" value="alacarte" readonly/></span>
      </p>
      <p>
         <label>Jenis Pelanggan:</label>
         <span class="field"><input type="text" name="cust_type" class="smallinput" value="{{{ $customer->cust_type }}}" readonly/></span>
      
      <p>
         <label>Nama Pelanggan:</label>
         <span class="field"><input type="text" name="cust_name" class="smallinput" value="{{{ $fab->CustomerName }}}" readonly/>  *)</span>
      </p>
      <p>
         <label>Alamat Pemasangan:</label>
         <span class="field"><input type="text" name="alamat" class="largeinput" value="{{{ $fab->Address }}}" readonly/>  *)</span>
      </p>
      <p>
         <label>Telepon Pelanggan:</label>
         <span class="field"><input type="text" name="cust_phone" class="smallinput" value="{{{ $customer->cust_phone }}}" readonly/>  *)</span>
      </p>
      <p>
         <label>Developer Company:</label>
         <span class="field"><input type="text" name="developer_company" class="smallinput" value="{{{ $fab->DeveloperCompany }}}" readonly/></span>
      </p>
      <p>
         <label>Developer Name:</label>
         <span class="field"><input type="text" name="developer_name" class="smallinput" value="{{{ $fab->DeveloperName }}}" readonly/></span>
      </p>
      <p>
         <label>Developer Telp:</label>
         <span class="field"><input type="text" name="developer_telp" class="smallinput" value="{{{ $fab->DeveloperTelp }}}" readonly/></span>
      </p>
      <p>
         <label>Developer Email:</label>
         <span class="field"><input type="text" name="developer_email" class="smallinput" value="{{{ $fab->DeveloperEmail }}}" readonly/></span>
      </p>

         <label>Pilih Device: </label>
         <span class="field">
         	
         	<table width="700" class="stdtable">
         		<tr>
         			<th width="30%">Nama Perangkat</th>
         			<th width="10%">Jumlah Order</th>
         			<th width="10%">Stock Tersedia</th>
         		</tr>
         		<?php $no=1; ?>
         		@foreach($data as $device)
         		<tr>
         			<input type="hidden" name="id_device_{{{ $no }}}" value="{{{ $device->id_devices }}}">
         			<input type="hidden" name="namedevice_{{{ $no }}}" value="{{{ $device->nama_device }}}" />
         			
         			<td>{{{ $device->nama_device }}}</td>
         			<td><input type="text" name="jumlah_{{{ $no }}}" size="1" placeholder="0"></td>
         			<td>{{{ $device->stock }}}</td>
                  
                  <input type="hidden" name="price_{{{ $no }}}" id="price_{{{ $no }}}" value="{{{ $device->harga }}}">
                  
         		</tr>
         		<?php $no++; ?>
         		@endforeach
         	</table>
         </span>
             <small class="desc">Jumlah device yang bisa anda masukkan adalah <font style="color:red;"> @if($delta!==0) {{{ $delta }}} @else {{{ $kuota }}} @endif device</font></small>
     
       <p>
         <label>Keterangan:</label>
         <span class="field"><input type="text" name="keterangan" class="largeinput" /></span>
      </p>
      <p class="stdformbutton">
        	<button class="submit radius2">Confirm</button>
          <input type="reset" class="reset radius2" value="Reset" />
      </p>
   </form>
@stop
