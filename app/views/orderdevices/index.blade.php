@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Order Devices</h2></div><br>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
@endif

<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
	<thead>
		<tr>
			<th width="1%" class="head0 nosort"></th>
			<th width="1%" class="head1 nosort">No.</th>
			<th width="2%" class="head1">Tipe Order</th>
			<th width="2%" class="head1">No. Speedy</th>
			<th width="5%" class="head1">Nama Pelanggan</th>
			<th width="5%" class="head1">Tanggal Order</th>
			<!-- <th width="5%" class="head1">Umur order</th> -->
			<th width="5%" class="head1">Jumlah Device</th>
			<th width="5%" class="head1">Status Order</th>
			<th width="5%" class="head1">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; ?>
	@foreach($data as $order)
	<?php
		$requestDate    = new DateTime($order->tanggal_order);
		$nowDate        = new DateTime(date("Y-m-d H:i:s"));
		$selisih        = $requestDate->diff($nowDate);
		
	?>
		<tr>
			<td></td>
			<td>{{{ $no }}}</td>
			<td>{{{ $order->OrderType }}}</td>
			<td>{{{ $order->cust_id }}}</td>
			<td>{{{ $order->cust_name }}}</td>
			<td>{{{ $order->tanggal_order }}}</td>
			<td>{{{ $qty[$no-1] }}} device</td>
			<td>{{{ $order->status }}}</td>
			<td>
			@if(Session::get('role')=='mitra')
				@if($order->status=='Open' || $order->status=='Confirmation')
					<a href="orderdevices/response/{{{ $order->id_status }}}" style="color:red;">Response</a> | 
					<a href="orderdevices/edit/{{{ $order->invoice }}}">Edit</a> |	
				@endif
			@elseif(Session::get('role')=='setter')
				@if($order->status=='Preparation'|| $order->status=='Customer Visit')
					@if(in_array($order->invoice, $assigned))
					<a href="orderdevices/response/{{{ $order->id_status }}}" style="color:red;">Response</a> | 
					@endif
				@endif
			@elseif(Session::get('role')=='ESA DSC')
				@if($order->status=='Installation Done')
					<a href="orderdevices/response/{{{ $order->id_status }}}" style="color:red;">Response</a> | 
				@endif
			@endif
				
				<a href="orderdevices/detail/{{{ $order->id_status }}}">Detail</a></td>
		</tr>
	<?php $no++; ?>	
	@endforeach
	</tbody>
</table>
@stop
