@extends('layouts.master')

@section('content')

<div class="contenttitle2">
	<h1>Detail Device Order</h1>
</div>

<div id="updates" class="subcontent">
  <div class="notibar announcement">
     <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>Invoice: #</strong> {{{ $status->invoice }}} &nbsp; &nbsp; &nbsp; 
      <strong>Status: </strong> {{{ $status->status }}} &nbsp; &nbsp; &nbsp; 
      <strong>Umur order: </strong> {{{ $selisih }}} &nbsp; &nbsp; &nbsp; 
      <strong>Keterangan: </strong>{{{ $status->keterangan }}}</p>
  </div>
</div>
<form class="stdform">

   <label>Order ID:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->transid }}}" readonly/></span>

   <label>Tipe Order:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->OrderType }}}" readonly/></span>

   <label>Tanggal Order:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->tanggal_order }}}" readonly/></span>

   <label>No. Speedy:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->cust_id }}}" readonly/></span>

   <label>Nama Pelanggan:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->cust_name }}}" readonly/></span>

   <label>Telepon Pelanggan:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->cust_phone }}}" readonly/></span>

   <label>Alamat:</label>
   <span class="field"><input type="text" class="largeinput" value="{{{ $order->alamat }}}" readonly/></span>

   <label>Perusahaan Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->dev_company }}}" readonly/></span>

   <label>Nama Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->dev_name }}}" readonly/></span>

   <label>Telepon Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->dev_telp }}}" readonly/></span>

   <label>Email Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->dev_email }}}" readonly/></span>

   <label>Keterangan Order:</label>
   <span class="field"><input type="text" class="longinput" value="{{{ $order->keterangan_order }}}" readonly/></span>
</form>
<br>
<div class="contenttitle2"><h2>Pesanan Perangkat</h2></div>
<table class="stdtable">
<thead>
  <tr>
    <th style="background-color:#C4C4C4">No</th>
    <th style="background-color:#C4C4C4">Perangkat</th>
    <th style="background-color:#C4C4C4">Jumlah</th>
    <th style="background-color:#C4C4C4">Total</th>
  </tr>
</thead>
<tbody>

@for($i=0;$i<count($name);$i++)
  <tr>
    <td>{{{ $i+1 }}}</td>
    <td>{{{ $name[$i] }}}</td>
    <td>{{{ $qty[$i] }}} device</td>
    <td>Rp. {{{ number_format($price[$i], 2, ',', '.') }}} </td>
  </tr> 
@endfor
<tfoot>
<tr>
  <th colspan="3" class="head1"><strong><center>Total Order</center></strong></th>
  <th class="head1"><strong>Rp. {{{ number_format($total, 2, ',', '.') }}}</strong></th>
</tr>
</tfoot>
</tbody>
</table>
<br>
<div><input type="button" class="stdbtn btn_yellow" value="History Pesanan" onClick="window.location.href='{{ URL::to('orderdevices/detail/log/'.$order->invoice)}}'"/>
  &nbsp;&nbsp;&nbsp;<input type="button" class="stdbtn btn_orange" value="Cetak Bukti Pembayaran" onClick="window.open('{{ URL::to('orderdevices/detail/struk/'.$order->invoice)}}')"/></div>
<br>  
<div class="contenttitle2"><h2>Status Order Log</h2></div>
<table class="stdtable">
<thead>
  <tr>
    <th style="background-color:#C4C4C4">No.</th>
    <th style="background-color:#C4C4C4">Status Order</th>
    <th style="background-color:#C4C4C4">Waktu Mulai</th>
    <th style="background-color:#C4C4C4">Waktu Selesai</th>
    <th style="background-color:#C4C4C4">Updated by</th>
  </tr>
</thead>
<tbody>
<?php $no=1; ?>  
@foreach($logs as $log)
  <tr>
    <td>{{{ $no }}}</td>
    <td>{{{ $log->status }}}</td>
    <td>{{{ $log->start_update }}}</td>
    <td>{{{ $log->last_update }}}</td>
    <td>{{{ $log->updated_by }}}</td>
  </tr>
<?php $no++; ?>  
@endforeach  
</tbody>
</table> 
@stop
