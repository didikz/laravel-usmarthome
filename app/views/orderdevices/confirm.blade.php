@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Review Order</h2></div><br>

<div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>Apakah data order di bawah ini sudah benar? Jika tidak, klik <button class="stdbtn radius2" onclick="window.history.back()">Kembali</button> untuk mengulangi order</p>
      </div><!-- notification announcement -->
  </div>

<form class="stdform">

   <label>No Speedy:</label>
   <span class="field"><input type="text" name="custid" class="smallinput" value="{{{ $data['speedy'] }}}" readonly/></span>

   <label>Tipe Order:</label>
   <span class="field"><input type="text" name="type" class="smallinput" value="{{{ $data['order'] }}}" readonly/></span>

   <label>Jenis Pelanggan:</label>
   <span class="field"><input type="text" name="cust_type" class="smallinput" value="{{{ $data['cust_type'] }}}" readonly/></span>

   <label>Nama Pelanggan:</label>
   <span class="field"><input type="text" name="cust_name" class="smallinput" value="{{{ $data['cust_name'] }}}" readonly/></span>

   <label>Alamat Pemasangan:</label>
   <span class="field"><input type="text" name="alamat" class="largeinput" value="{{{ $data['cust_address'] }}}" readonly/></span>

   <label>No. Telepon:</label>
   <span class="field"><input type="text" name="cust_phone" class="smallinput" value="{{{ $data['cust_phone'] }}}" readonly/></span>

   <label>Developer Company:</label>
   <span class="field"><input type="text" name="developer_company" class="smallinput" value="{{{ $data['dev_company'] }}}" readonly/></span>

   <label>Developer Name:</label>
   <span class="field"><input type="text" name="developer_name" class="smallinput" value="{{{ $data['dev_name'] }}}" readonly/></span>

   <label>Developer Telp:</label>
   <span class="field"><input type="text" name="developer_telp" class="smallinput" value="{{{ $data['dev_telp'] }}}" readonly/></span>

   <label>Developer Email:</label>
   <span class="field"><input type="text" name="developer_email" class="smallinput" value="{{{ $data['dev_email'] }}}" readonly/></span>

   <label>Keterangan Order:</label>
   <span class="field"><input type="text" name="keterangan" class="longinput" value="{{{ $data['keterangan'] }}}" readonly/></span>

</form>
<div class="contenttitle2"><h2>Perangkat yang dipesan</h2></div>
<table class="stdtable" cellpadding="0" cellspacing="0" border="0">
  <thead>
    <tr>
      <th style="background-color:#C4C4C4">No</th>
      <th style="background-color:#C4C4C4">Nama Device</th>
      <th style="background-color:#C4C4C4">Qty</th>
      <th style="background-color:#C4C4C4">Total Harga Perangkat</th>
    </tr>
  </thead>
  <tbody>
    <?php $totalPrice = 0; ?>
    @for($i=1; $i <= $data['ordercount']; $i++)
      <tr>
        <td>{{{ $i }}}</td>
        <td>{{{ $name[$i-1] }}}</td>
        <td>{{{ $qty[$i-1] }}} Perangkat</td>
        <td>Rp. {{{ number_format($price[$i-1] * $qty[$i-1], 2, ',', '.') }}}</td>
      </tr>
     <?php $totalPrice += $price[$i-1] * $qty[$i-1]; ?>
    @endfor
    <tr>
      <td colspan="3"><strong><center>Total Device Order</center></strong></td>
      <td><strong>Rp. {{{ number_format($totalPrice, 2, ',', '.') }}}</strong></td>
    </tr>
  </tbody> 
</table><br>
<strong>Keterangan:</strong> Harga device diambil pada harga saat melakukan booking order device (jika melalui booking device)

<?php
  $ordertype = $data['order'];
  if ($ordertype=='alacarte') {
     $action = 'alacartesave';
  } else {
    $action = 'save';
  }
?>

<form method="post" action="{{{ $action }}}">
       <input type="hidden" name="transid" value="{{{ $data['transid'] }}}">
       <input type="hidden" name="idfab" value="{{{ $data['idfab'] }}}">
       <input type="hidden" name="developer_company" value="{{{ $data['dev_company'] }}}">  
       <input type="hidden" name="developer_name" value="{{{ $data['dev_name'] }}}">
       <input type="hidden" name="developer_telp" value="{{{ $data['dev_telp'] }}}"> 
       <input type="hidden" name="developer_email" value="{{{ $data['dev_email'] }}}">    
       <input type="hidden" name="speedy" value="{{{ $data['speedy'] }}}">
       <input type="hidden" name="cust_name" value="{{{ $data['cust_name'] }}}"> 
       <input type="hidden" name="cust_phone" value="{{{ $data['cust_phone'] }}}">
       <input type="hidden" name="alamat" value="{{{ $data['cust_address'] }}}">
       <input type="hidden" name="idbooking" value="{{{ $data['idbooking'] }}}" />
       <input type="hidden" name="order" value="{{{ $data['order'] }}}" />
       <input type="hidden" name="cust_type" value="{{{ $data['cust_type'] }}}" />
          
        @for($i=1; $i <= $data['ordercount']; $i++)
        
	       <input type="hidden" name="iddevice_{{{ $i }}}" value="{{{ $id[$i-1] }}}">
	       <input type="hidden" name="jumlah_{{{ $i }}}" value="{{{ $qty[$i-1] }}}">

         @if($data['order']=='aktivasi' || $data['order']=='modifikasi')
         <input type="hidden" name="harga_{{{ $i }}}" value="{{{ $price[$i-1] }}}">
         @endif
        	
        @endfor
        
           <input type="hidden" name="keterangan" value="{{{ $data['keterangan'] }}}">
       
        <input type="hidden" name="device_count" value="{{{ $data['ordercount'] }}}">
         <p class="stdformbutton">
          	<button class="submit radius2">Submit Order</button>
          	
        </p>	
</form>
@stop