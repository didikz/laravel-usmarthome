@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Log Ordered Device</h2></div>
<table class="stdtable">
	<thead>
	<tr>
		<th class="head1">No.</th>
		<th class="head1">Nama Perangkat</th>
		<th class="head1">Jumlah</th>
		<th class="head1">Total Harga</th>
		<th class="head1">Status Pesanan</th>
		<th class="head1">Update</th>
	</tr>
	</thead>
	<tbody>
	<?php $no=1; ?>	
		@foreach($logs as $log)
		<tr>
			<td>{{{ $no }}}</td>
			<td>{{{ $log->nama_device }}} device</td>
			<td>{{{ $log->qty }}}</td>
			<td>Rp. {{{ number_format($log->qty * $log->ordered_price, 2, ',', '.') }}}</td>
			<td>
				@if($log->edited_status=='0')
					Diproses
				@else
					Tidak diproses
				@endif
			</td>
			<td>{{{ $log->updated_at }}}</td>
		</tr>
		<?php $no++; ?>
		@endforeach
	</tbody>	
</table><br>
<input type="button" class="stdbtn" value="Kembali" onclick="window.history.back()">
@stop
