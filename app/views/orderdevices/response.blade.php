@extends('layouts.master')

@section('content')

<div class="contenttitle2"><h2>Response Order Devices</h2></div><br><br>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
@endif

<form method="post" class="stdform" action="save">
	<input type="hidden" name="invoice" value="{{{ $status->invoice }}}">
	<input type="hidden" name="idstatus" value="{{{ $status->id_status }}}" >
	<input type="hidden" name="date" value="{{{ $status->last_update}}}">
	<input type="hidden" name="currentstatus" value="{{{ $status->status }}}">
	<input type="hidden" name="currentupdater" value="{{{ $status->updated_by }}}">
	<input type="hidden" name="currentketerangan" value="{{{ $status->keterangan }}}">
	   <label>Ubah Status</label>
	   <span class="field">
	   	<select name="status_order_device" class="radius3">
	   	@if($status->status=='Open')
	   		<option value="Open" selected>Open</option>
	   		<option value="Confirmation">Confirmation</option>
	   	@elseif($status->status=='Confirmation')
        <option value="Confirmation" selected>Confirmation</option>
        <option value="Preparation">Preparation</option>
      @elseif($status->status=='Preparation')
        <option value="Preparation" selected>Preparation</option>
        @if(Session::get('role')=='setter')
          <option value="Customer Visit">Customer Visit</option>
        @endif
      @elseif($status->status=='Customer Visit')
        <option value="Customer Visit" selected>Customer Visit</option>
          @if(Session::get('role')=='setter')
            <option value="Installation Done">Installation Done</option>
          @endif
      @elseif($status->status=="Installation Done")
        <option value="Installation Done" selected>Installation Done</option>
        @if(Session::get('role')=='ESA DSC')
          <option value="Close">Close</option>
        @endif
      @elseif($status->status=='Close')
          <option>Closed</option>
      @elseif($status->status=='Cancelled')
        <option value="Cancelled" selected>Cancelled</option>
	   	@endif		
	   	</select>
	   </span>
	      <!--  <small class="desc">Order ID</small> -->
	</p>
   @if($status->status=='Confirmation' || $setters!=='0')
  <p>
     <label>Assign Setter:</label>
     <span class="field">
        <select name="id_setter">
        @foreach($setters as $setter)
          <option value="{{{ $setter->id_setter }}}">{{{ $setter->setter_name }}}</option>
        @endforeach
        </select>
     </span>
         <small class="desc">tugaskan setter untuk customer visit</small>
  </p>
  @endif
  @if(!empty($status->keterangan))
  <p>
     <label>Keterangan saat ini:</label>
     <span class="field">{{{ $status->keterangan }}}</span>
  </p>
  @endif
	<p>
	   <label>Keterangan:</label>
	   <span class="field"><input type="text" name="keterangan_order_device"></span>
	      <!--  <small class="desc">Order ID</small> -->
	</p>
	<p class="stdformbutton">
	  	<button class="submit radius2">Submit</button>
	  	<input type="reset" class="reset radius2" value="Reset" />
	</p>
</form><br>
<div class="contenttitle2"><h2>Data Pelanggan</h2></div>
<form class="stdform">
   <label>Order ID:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->transid }}}" readonly/></span>

   <label>Tanggal Order:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->tanggal_order }}}" readonly/></span>

   <label>No Speedy:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->cust_id }}}" readonly/></span>

   <label>Nama Pelanggan:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->cust_name }}}" readonly/></span>

   <label>Telepon Pelanggan:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->cust_phone }}}" readonly/></span>

   <label>Alamat:</label>
   <span class="field"><input type="text" class="largeinput" value="{{{ $order->alamat }}}" readonly/></span>

   <label>Perusahaan Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->dev_company }}}" readonly/></span>

   <label>Nama Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->dev_name }}}" readonly/></span>

   <label>Telepon Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->dev_telp }}}" readonly/></span>

   <label>Email Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $order->dev_email }}}" readonly/></span>
</form>

@stop
