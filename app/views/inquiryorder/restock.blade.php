@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Restock of inquiry</h2></div>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif

<form class="stdform" method="post" action="{{{ URL::to('inquiry/update/'.$inquiry['id']) }}}">
	<p>
       <label>AM:</label>
       <span class="field"><input type="text" name="am" class="smallinput" value="{{{ $inquiry['nama_lengkap'] }}}" readonly/></span>
  	</p>
	<p>
       <label>Perangkat:</label>
       <span class="field"><input type="text" name="devicename" class="smallinput" value="{{{ $inquiry['nama_device'] }}}" readonly/></span>
  	</p>
  	<p>
       <label>Jumlah Inquiry:</label>
       <span class="field"><input type="text" name="qty" class="smallinput" value="{{{ $inquiry['qty'] }}}" readonly/></span>
  	</p>
  	<p>
       <label>Tanggal Inquiry:</label>
       <span class="field"><input type="text" name="created_at" class="smallinput" value="{{{ $inquiry['created_at'] }}}" readonly/></span>
  	</p>
    <p>
       <label>Old Restock:</label>
       <span class="field"><input type="text" name="oldrestock" class="smallinput" value="{{{ $inquiry['restock'] }}}" disabled/></span>
    </p>
  	<p>
       <label>Add Restock:</label>
       <span class="field"><input type="text" name="restockqty" class="smallinput" placeholder="0" required/> *)</span>
       <small class="desc">masukkan jumlah tambahan stock device untuk inquiry ini. Jumlah restock tidak boleh melebihi jumlah inquiry.<br>Inquiry akan di-closed jika add restock sama dengan jumlah inquiry</small>
  	</p>
  	<p>
       <label>Keterangan:</label>
       <span class="field"><input type="text" name="keterangan" class="largeinput" value=""/></span>
  	</p>
    <!-- <p>
      <label>Set Inquiry Status: </label>
      <span class="field">
        <select name="setstatus">
          <option value="null">-- Pilih Status --</option>
          <option value='3'>Closed</option>
        </select>
      </span>
      <small class="desc">Closed: untuk menutup order inquiry.</small>
    </p> -->
  	<p class="stdformbutton">
    	<button class="submit radius2">Submit</button>
      <input type="reset" class="reset radius2" value="Reset" />
  </p>
</form>
@stop
