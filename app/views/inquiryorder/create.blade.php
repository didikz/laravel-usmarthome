@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>New Inquiry Order </h2></div>
<form class="stdform" method="post" action="{{{ URL::to('inquiry/save') }}}">
	<input type="hidden" name="iddevice" value="{{{ $device['id_devices'] }}}">
	<p>
       <label>Perangkat:</label>
       <span class="field"><input type="text" name="devicename" class="smallinput" value="{{{ $device['nama_device'] }}}" readonly/> *)</span>
  	</p>
  	<p>
       <label>Jumlah Inquiry:</label>
       <span class="field"><input type="text" name="qty" class="smallinput" value="" required/> *)</span>
  	</p>
  	<p>
       <label>Keterangan:</label>
       <span class="field"><input type="text" name="keterangan" class="largeinput" value=""/></span>
  	</p>
  	<p class="stdformbutton">
    	<button class="submit radius2">Submit</button>
      <input type="reset" class="reset radius2" value="Reset" />
  </p>
</form>
@stop
