@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>List Inquiry Order</h2></div>

@if(Session::get('role')=='AM')
<div><input type="button" class="stdbtn btn_lime" onClick="window.location.href='inquiry/new'" value="Create inquiry"></div><br><hr><br>
@endif

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif

<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
	<thead>
		<tr>
		<th class="head1">No.</th>
		<th class="head1">Perangkat</th>
		<th class="head1">Jumlah</th>
		<th class="head1">Restock</th>
		<th class="head1">Tanggal Order</th>
		<th class="head1">Status</th>
		<th class="head1">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; ?>
	@foreach($inquiry as $data)
		<tr>
			<td>{{{ $no }}}</td>
			<td>{{{ $data->nama_device }}}</td>
			<td>{{{ $data->qty }}} item</td>
			<td>{{{ $data->restock }}} item</td>
			<td>{{{ $data->created_at }}}</td>
			<td>
			@if($data->inquiry_status=='0')
				<font color="orange">Belum diproses</font>
			@elseif($data->inquiry_status=='1')
				<font color="green">Sedang diproses</font>
			@elseif($data->inquiry_status=='2')
				<font color="red">Dibatalkan</font>
			@else
				<font color="blue">Closed</font>		
			@endif
			</td>
			<td>
				<a href="{{ URL::to('inquiry/detail/'.$data->id_device) }}">Detail</a>
			</td>
		</tr>
	<?php $no++; ?>	
	@endforeach
	</tbody>
</table>
@stop
