@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>New Inquiry Order </h2></div>
<form class="stdform" method="post" action="{{{ URL::to('inquiry/save') }}}">
	<p>
       <label>Perangkat:</label>
       <select name="iddevice">
        @foreach($device as $item)
       		<option value="{{ $item->id_devices }}">{{{ $item->nama_device }}}</option>
       	@endforeach
	   </select>
  	</p>
  	<p>
       <label>Jumlah Inquiry:</label>
       <span class="field"><input type="text" name="qty" class="smallinput" value="" required/> *)</span>
  	</p>
  	<p>
       <label>Keterangan:</label>
       <span class="field"><input type="text" name="keterangan" class="largeinput" value=""/></span>
  	</p>
  	<p class="stdformbutton">
    	<button class="submit radius2">Submit</button>
      <input type="reset" class="reset radius2" value="Reset" />
  </p>
</form>
@stop
