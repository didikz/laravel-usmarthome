@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Restock of all inquiry</h2></div>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif

<form class="stdform" method="post" action="{{{ URL::to('inquiry/restockall/update/'.$data['id']) }}}">
	<p>
       <label>Perangkat:</label>
       <span class="field"><input type="text" name="devicename" class="smallinput" value="{{{ $data['devicename'] }}}" readonly/></span>
  	</p>
  	<p>
       <label>Total Inquiry:</label>
       <span class="field"><input type="text" name="qty" class="smallinput" value="{{{ $data['qty'] }}}" readonly/></span>
  	</p>
  	<p>
       <label>Add Restock:</label>
       <span class="field"><input type="text" name="restock" class="smallinput" placeholder="0" required/> *)</span>
       <small class="desc">Restock harus sama dengan total inquiry</small>
  	</p>
    <!-- <p>
      <label>Set Inquiry Status: </label>
      <span class="field">
        <select name="setstatus">
          <option value="null">-- Pilih Status --</option>
          <option value='3'>Closed</option>
        </select>
      </span>
      <small class="desc">Closed: untuk menutup order inquiry.</small>
    </p> -->
  	<p class="stdformbutton">
    	<button class="submit radius2">Submit</button>
        <input type="reset" class="reset radius2" value="Reset" />&nbsp;
		<input type="button" class="stdbtn" value="Kembali" onclick="window.history.back()"/>
    </p>
</form>
@stop
