@extends('layouts.master')
@section('content')
<div class="contenttitle2"><h2>Detail Inquiry Device</h2></div>
<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
<thead>
	<tr>
		<th class="head1">No</th>
		<th class="head1">AM</th>
		<th class="head1">Perangkat</th>
		<th class="head1">Jumlah Inquiry</th>
		<th class="head1">Restock</th>
		<th class="head1">Status</th>
		<th class="head1">Action</th>
	</tr>
</thead>
<tbody>
	<?php $no=1; ?>
	@foreach($inquiry as $item)
	<tr>
		<td>{{{ $no }}}</td>
		<td>{{{ $item->nama_lengkap }}}</td>
		<td>{{{ $item->nama_device }}}</td>
		<td>{{{ $item->qty }}} item</td>
		<td>{{{ $item->restock }}} item</td>
		<td>
			@if($item->inquiry_status=='0')
				<font color="orange">Belum diproses</font>
			@elseif($item->inquiry_status=='1')
				<font color="green">Sedang diproses</font>
			@else
				<font color="red">Dibatalkan</font>
			@endif
		</td>
		<td>
			<a href="{{ URL::to('inquiry/restock/'.$item->id) }}">Restock</a> | 
			<a href="{{ URL::to('inquiry/detail/'.$item->id_device) }}">Detail</a>
		</td>
	</tr>
	<?php $no++; ?>
	@endforeach	
</tbody>
</table><br>
<div><input type="button" class="stdbtn" value="Kembali" onclick="window.history.back()"/></div>
@stop
