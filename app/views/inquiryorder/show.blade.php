@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Detail Inquiry</h2></div>
<form class="stdform">
	
       <label>Perangkat:</label>
       <span class="field"><input type="text" class="smallinput" value="{{{ $inquiry['nama_device'] }}}" readonly/></span>
  
       <label>Jumlah Inquiry:</label>
       <span class="field"><input type="text" class="smallinput" value="{{{ $inquiry['qty'] }}} item perangkat" readonly/></span>

       <label>Tanggal Inquiry:</label>
       <span class="field"><input type="text" class="smallinput" value="{{{ $inquiry['created_at'] }}}" readonly/></span>

       <label>Restock:</label>
       <span class="field"><input type="text" class="smallinput" value="{{{ $inquiry['restock'] }}} item perangkat" readonly/></span>

       <label>Status:</label>
       <span class="field">
       	@if($inquiry['inquiry_status']=='0')
       		<input type="text" class="smallinput" value="Belum diproses" readonly/>
       	@elseif($inquiry['inquiry_status']=='1')
       		<input type="text" class="smallinput" value="Sedang diproses" readonly/>
       	@elseif($inquiry['inquiry_status']=='2')
       		<input type="text" class="smallinput" value="Dibatalkan" readonly/>
        @else
          <input type="text" class="smallinput" value="Closed" readonly/>
       	@endif
       </span>
       <label>Keterangan:</label>
       <span class="field"><input type="text" class="largeinput" value="{{{ $inquiry['keterangan'] }}}" readonly/></span>
  	<p>
  		<span class="field">
  			<input type="button" class="stdbtn" value="Kembali" onclick="window.history.back()"/>
  		</span>
  	</p>
  	
</form>
@stop
