@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>List Inquiry Order</h2></div>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif

<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
	<thead>
		<tr>
		<th class="head1">No.</th>
		<th class="head1">Perangkat</th>
		<th class="head1">Jumlah</th>
		<th class="head1">Restock</th>
		<th class="head1">Tanggal Order</th>
		<th class="head1">AM</th>
		<th class="head1">Status</th>
		<th class="head1">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; ?>
	@foreach($inquiry as $data)
		<tr>
			<td>{{{ $no }}}</td>
			<td>{{{ $data->nama_device }}}</td>
			<td>{{{ $data->qty }}} item</td>
			<td>{{{ $data->restock }}} item</td>
			<td>{{{ $data->created_at }}}</td>
			<td>{{{ $data->nama_lengkap }}}</td>
			<td>
			@if($data->inquiry_status=='0')
				<font color="orange">Belum diproses</font>
			@elseif($data->inquiry_status=='1')
				<font color="green">Sedang diproses</font>
			@elseif($data->inquiry_status=='2')
				<font color="red">Dibatalkan</font>
			@else
				<font color="blue">Closed</font>		
			@endif
			</td>
			<td>
			@if(Session::get('role')=='mitra')
				@if($data->qty > $data->restock)
					<a href="{{ URL::to('inquiry/restock/'.$data->id) }}">Restock</a>  |  
				@endif	
			@endif	
				<a href="{{ URL::to('inquiry/detail/'.$data->id_device) }}">Detail</a>
			</td>
		</tr>
	<?php $no++; ?>	
	@endforeach
	</tbody>
</table><br>
<div class="contenttitle2"><h2>Inquiry Device</h2></div>
<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
	<thead>
		<tr>
		<th class="head1">No.</th>
		<th class="head1">Perangkat</th>
		<th class="head1">Stock</th>
		<th class="head1">Total Inquiry</th>
		<th class="head1">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $noo=1; ?>
	@foreach($master as $item)
		<tr>
			<td>{{{ $noo }}}</td>
			<td>{{{ $item->nama_device }}}</td>
			<td>{{{ $item->stock }}} item</td>
			<td>
			@if($count[$noo-1]>0)
				<font color="red">{{{ $count[$noo-1] }}} Item</font>
			@else
				<font color="green">{{{ $count[$noo-1] }}} Item</font>
			@endif
			</td>
			</td>
			<td>
			@if($count[$noo-1]>0)
				<a href="{{{ URL::to('inquiry/restockall/'.$item->id_devices) }}}">Restock All</a>  |  
			@endif
				<a href="{{{ URL::to('inquiry/detailall/'.$item->id_devices) }}}">Detail</a>
			</td>
		</tr>
	<?php $noo++; ?>	
	@endforeach
	</tbody>
</table>
@stop
