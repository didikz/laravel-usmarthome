@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Data Perangkat Pelanggan</h2></div><br>
	<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
	<thead>
	    <tr>
	       <th width="5%" class="head1">No.</th>
	       <th width="10%" class="head1">No Speedy</th>
	       <th width="10%" class="head1">Nama</th> 
	       <!-- <th width="10%" class="head1">Setter</th> -->
	       <th width="10%" class="head1">Telepom</th>
	       <th width="10%" class="head1">Perangkat yang dipesan</th>
	       <th width="5%" class="head1">Action</th>
	    </tr>
	</thead>
	<tbody>
	<?php $no=1; ?>
	@foreach($orders as $user)
		<tr>
			<td>{{{ $no }}}</td>
			<td>{{{ $user->cust_id }}}</td>
			<td>{{{ $user->cust_name }}}</td>
			<td>{{{ $user->cust_phone }}}</td>
			<!-- <td>-</td>
			<td>-</td> -->
			<td>{{{ $count[$no-1] }}} perangkat</td>
			<td><a href="{{{ URL::to('perangkat/detail/'.$user->invoice) }}}">Detail</a></td>
		</tr>
	<?php $no++; ?>
	@endforeach
		
	</tbody>
</table>
@stop