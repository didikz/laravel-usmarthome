@extends('layouts.master')

@section('content')
<div class="widgetbox" >
	<div class="title"><h2>Detail Data Perangkat</h2></div>
</div>
<div id="updates" class="subcontent">
  <div class="notibar announcement">
     <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>Nama: </strong> {{{ $order->cust_name }}} &nbsp; &nbsp; &nbsp; 
      <strong>No Speedy: </strong> {{{ $order->cust_id }}} &nbsp; &nbsp; &nbsp; 
  </div>
<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
	<thead>
	    <tr>
	       <th width="5%" class="head1">No.</th>
	       <th width="10%" class="head1">Nama Perangkat</th> 
	       <th width="10%" class="head1">Jumlah Perangkat</th>
	       <th width="10%" class="head1">Harga Total</th>
	    </tr>
	</thead>
	<tbody>
	<?php $no=1; ?>	
	@foreach($ordered as $perangkat)
		<tr>
			<td>{{{ $no }}}</td>
			<td>{{{ $perangkat['nama_device'] }}}</td>
			<td>{{{ $perangkat['qty'] }}} buah</td>
			<td>Rp. {{{ number_format($perangkat['qty'] * $perangkat['harga'], 2, ',', '.') }}}</td>
		</tr>
	<?php $no++; ?>	
	@endforeach	
	</tbody>
</table>	
</div>
@stop