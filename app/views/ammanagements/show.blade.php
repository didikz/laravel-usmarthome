@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Detail Account Manager</h2></div>
<form class="stdform">
   <label>NIK</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $am->nik }}}" readonly/></span>

   <label>Nama Lengkap</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $am->nama_lengkap }}}" readonly/></span>

   <label>Alamat</label>
   <span class="field"><input type="text" class="largeinput" value="{{{ $am->alamat }}}" readonly /></span>

   <label>No. Telepon</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $am->telepon }}}" readonly/></span>

   <label>Email</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $am->email }}}" readonly/></span>

   <label>Witel</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $am->witel }}}" readonly/></span>

   <label>Login Terakhir</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $am->last_login }}}" readonly/></span>

   <label>Jumlah Order</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $count }}} order" readonly/></span>
</form>
<br>
<div class="contenttitle2"><h2>Order History</h2></div>
<table class="stdtable">
	<thead>
		<tr>
      <th style="background-color:#C4C4C4">No.</th>
			<th style="background-color:#C4C4C4">No Speedy</th>
      <th style="background-color:#C4C4C4">Pelanggan</th>
      <th style="background-color:#C4C4C4">Nama Developer</th>
			<th style="background-color:#C4C4C4">Tanggal order</th>
			<th style="background-color:#C4C4C4">Jumlah Perangkat</th>
		</tr>
	</thead>
	<tbody>
  @if(count($order)==0)
  <tr>
    <td colspan="6"><center><strong>Belum ada order history</strong></center></td>
  </tr>
  @else
    <?php $i=0; ?>  
    @foreach($order as $data)
      <tr>
        <td>{{{ $i+1 }}}</td>
        <td>{{{ $data->cust_id }}}</td>
        <td>{{{ $data->cust_name }}}</td>
        <td>{{{ $data->dev_name }}}</td>
        <td>{{{ $data->tanggal_order }}}</td>
        <td>{{{ $countDevice[$i] }}} perangkat</td>
      </tr>
    <?php $i++; ?>  
    @endforeach 
  @endif  
  
	</tbody>
</table>
</div>
	
@stop
