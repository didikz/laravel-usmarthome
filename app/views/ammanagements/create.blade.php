@extends('layouts.master')

@section('content')
<div class="contenttitle2">
<h2>New Account Manager</h2>
</div>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif
<form class="stdform" method="post" action="save" />
	<p>
       <label>Nama AM:</label>
       <span class="field"><input type="text" name="am_name" class="smallinput" value="{{{ Input::old('am_name') }}}" required/> *)</span>
  </p>
  <p>
     <label>NIK:</label>
     <span class="field"><input type="text" name="nik" class="smallinput" value="{{{ Input::old('nik') }}}" required/> *)</span>
  </p>
  <p>
     <label>Email:</label>
     <span class="field"><input type="text" name="email" class="smallinput" value="{{{ Input::old('email') }}}"required/> *)</span>
  </p>
  <p>
     <label>Telepon</label>
     <span class="field"><input type="text" name="telepon" class="smallinput" value="{{{ Input::old('telepon') }}}"required/> *)</span>
  </p>
  <p>
     <label>Alamat</label>
     <span class="field"><input type="text" name="alamat" class="largeinput" value="{{{ Input::old('alamat') }}}"required/> *)</span>
  </p>
  <p>
     <label>Witel</label>
     <span class="field">
     	<select name="witel" id="witel">
     	@foreach($propinsi as $witel)
     		<option value="{{{ $witel }}}" @if(Input::old('witel')== $witel) selected @endif>{{{ $witel }}}</option>
     	@endforeach
     	</select> *)
     </span>
  </p>
  <p>
     <label>Username AM</label>
     <span class="field"><input type="text" name="am_username" class="smallinput" required/> *)</span>
  </p>
  <p>
     <label>Password AM</label>
     <span class="field"><input type="password" name="am_password" class="smallinput" required/> *)</span>
  </p>
  <p class="stdformbutton">
    	<button class="submit radius2">Submit</button>
      <input type="reset" class="reset radius2" value="Reset" />
  </p>	
</form>
@stop