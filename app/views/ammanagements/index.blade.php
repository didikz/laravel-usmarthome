@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Account Managers</h2></div>
@if(Session::get('role')=='CO AM')
      <div><input type="button" class="stdbtn btn_lime" onClick="window.location.href='am/new'" value="New AM"></div><br><hr><br>
@endif
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif
<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
  <thead>
      <tr>
      	 <th width="1%" class="head0 nosort"></th>
         <th width="1%" class="head1 nosort">No.</th>
         <th width="8%" class="head1">Nama</th>
         <th width="5%" class="head1">NIK</th>
         <th width="8%" class="head1">Email</th>
         <th width="8%" class="head1">Telepon</th>
         <th width="8%" class="head1">Witel</th>
         <th width="10%" class="head1">last Login</th>
         <th width="5%" class="head1">Order</th>
         <th width="10%" class="head1">Action</th>
      </tr>
  </thead>
  <tbody>
  <?php $no=1; ?>	
  @foreach($ams as $am)
  	<tr>
  		<td></td>
  		<td>{{{ $no }}}</td>
  		<td>{{{ $am->nama_lengkap }}}</td>
  		<td>{{{ $am->nik }}}</td>
  		<td>{{{ $am->email }}}</td>
  		<td>{{{ $am->telepon }}}</td>
  		<td>{{{ $am->witel }}}</td>
  		<td>{{{ $am->last_login }}}</td>
  		<td>{{{ $count[$no-1].' order' }}}</td>
  		<td><a href="{{{ URL::to('am/edit/'.$am->id_am)}}}">Edit</a> | 
  			<a href="{{{ URL::to('am/delete/'.$am->id_am)}}}" onCLick="return confirm('apakah anda ingin menghapus AM {{ $am->nama_lengkap }}')">Delete</a> | 
  			<a href="{{{ URL::to('am/detail/'.$am->id_am)}}}">Detail</a></td>
  	</tr>
  <?php $no++; ?>	
  @endforeach
  </tbody>
</table>
@stop
