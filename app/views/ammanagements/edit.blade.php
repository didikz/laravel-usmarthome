@extends('layouts.master')

@section('content')
<div class="contenttitle2">
<h2>Edit Account Manager</h2>
</div>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif
<form class="stdform" method="post" action="../update/{{{ $am->id_am }}}" />
	<p>
     <label>Nama AM:</label>
     <span class="field"><input type="text" name="am_name" class="smallinput" value="{{{ $am->nama_lengkap }}}" required/> *)</span>
  </p>
  <p>
     <label>NIK:</label>
     <span class="field"><input type="text" name="nik" class="smallinput" value="{{{ $am->nik }}}" required/> *)</span>
  </p>
  <p>
     <label>Email:</label>
     <span class="field"><input type="text" name="email" class="smallinput" value="{{{ $am->email }}}"required/> *)</span>
  </p>
  <p>
     <label>Telepon</label>
     <span class="field"><input type="text" name="telepon" class="smallinput" value="{{{ $am->telepon }}}"required/> *)</span>
  </p>
  <p>
     <label>Alamat</label>
     <span class="field"><input type="text" name="alamat" class="smallinput" value="{{{ $am->alamat }}}"required/> *)</span>
  </p>
  <p>
     <label>Witel</label>
     <span class="field">
     	<select name="witel" id="witel">
     	@foreach($propinsi as $witel)
     		<option value="{{{ $witel }}}" @if($am->witel == $witel) selected @endif>{{{ $witel }}}</option>
     	@endforeach
     	</select> *)
     </span>
  </p>
  <p>
     <label>Username AM</label>
     <span class="field"><input type="text" name="am_username" class="smallinput" value="{{{ $am->username }}}" readonly/></span>
         <small class="desc">username tidak dapat dirubah</small>
  </p>
  <p>
     <label>Password AM</label>
     <span class="field"><input type="password" name="am_password" class="smallinput"/></span>
         <small class="desc">Kosongkan password jika tidak ada perubahan</small>
  </p>
  <p class="stdformbutton">
    	<button class="submit radius2">Submit</button>
      <input type="reset" class="reset radius2" value="Reset" />
  </p>	
</form>
@stop
