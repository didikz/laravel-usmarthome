@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Detail Booking Order</h2></div>
<form class="stdform">
	<label>Tipe Order:</label>
    <span class="field"><input type="text" class="smallinput" value="{{{ $booking->OrderType }}}" readonly/></span>
	<label>Nama Pelanggan:</label>
    <span class="field"><input type="text" class="smallinput" value="{{{ $booking->customer_name }}}" readonly/></span>
    <label>Nomor Speedy:</label>
    <span class="field"><input type="text" class="smallinput" value="{{{ $booking->AccountID }}}" readonly/></span>
    <label>Tanggal Booking:</label>
    <span class="field"><input type="text" class="smallinput" value="{{{ $booking->created_at }}}" readonly/></span>
    <label>Status:</label>
    <span class="field"><input type="text" class="smallinput" value="@if($booking->status=='0') Open @else Closed @endif" readonly/></span>
</form><br>

<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
	<thead>
	  <tr>
	    <th style="background-color:#C4C4C4">No.</th>
	    <th style="background-color:#C4C4C4">Perangkat dibooking</th>
	    <th style="background-color:#C4C4C4">Jumlah Perangkat</th>
	    <th style="background-color:#C4C4C4">Total</th>
	  </tr>
	</thead>
	<tbody>
	<?php $no = 1; $total = 0; ?>
	@foreach($devices as $device)
		<tr>
			<td>{{{ $no }}}</td>
			<td>{{{ $device->nama_device }}}</td>
			<td>{{{ $device->qty }}} Perangkat</td>
			<td>Rp. {{{ number_format($device->harga_booking * $device->qty, 2, ',', '.') }}}</td>
		</tr>
	<?php $no++; $total+= $device->harga_booking * $device->qty; ?>
	@endforeach	
		<tr>
		  <td colspan="3"><strong><center>Total Order</center></strong></td>
		  <td><strong>Rp. {{{ number_format($total, 2, ',', '.') }}}</strong></td>
		</tr>
	</tbody>
</table><br>
<button class="stdbtn radius2" onclick="window.history.back()">Kembali</button>		
@stop