@extends('layouts.master')

@section('content')

<div class="contenttitle2"><h2>Booking Order</h2></div>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
 @endif

@if(Session::get('roleid')=='5')
	<div><input type="button" class="stdbtn btn_lime" onClick="window.location.href='bookingorder/new'" value="new"></div><br><hr><br>
@endif

<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
	<thead>
	  <tr>
	     <th width="5%" class="head1">No.</th>
	     <th width="10%" class="head1">Tipe Order</th>
	     <th width="10%" class="head1">No. Speedy</th>
	     <th width="15%" class="head1">Nama Pelanggan</th>
	     <th width="10%" class="head1">Jumlah Device</th>
	     <th width="10%" class="head1">Tanggal Booking</th>
	     <th width="10%" class="head1">Status Booking</th>
	     <th width="10%" class="head1">Action</th>
	  </tr>
	</thead>
	<tbody>
	<?php $no=1; ?>
	@foreach($bookings as $booking)
		<tr>
			<td>{{{ $no }}}</td>
			<td>{{{ $booking->OrderType }}}</td>
			<td>{{{ $booking->AccountID }}}</td>
			<td>{{{ $booking->customer_name }}}</td>
			<td>{{{ $sum[$no-1] }}} device</td>
			<td>{{{ $booking->created_at }}}</td>
			<td>
				@if('0'==$booking->status)
					Open
				@else
					Closed
				@endif
			</td>
			<td>
				@if('0'==$booking->status)
					<a href="{{{ URL::to('bookingorder/edit/'.$booking->id) }}}">Edit</a> | 
					<a href="{{{ URL::to('bookingorder/delete/'.$booking->id) }}}" onClick="return confirm('Hapus booking order pelanggan {{ $booking->customer_name }}')">Delete | </a>
				@endif
				
				<a href="{{{ URL::to('bookingorder/detail/'.$booking->id) }}}">Detail</a>
			</td>
		</tr>
	<?php $no++; ?>	
	@endforeach	
	</tbody>
</table>

@stop
