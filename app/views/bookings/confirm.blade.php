@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>{{{ $data['title'] }}}</h2></div><br>

<div id="updates" class="subcontent">
  <div class="notibar announcement">
         <p>Apakah data booking di bawah ini sudah benar? Jika tidak, klik <button class="stdbtn radius2" onclick="window.history.back()">Kembali</button> untuk mengulangi order</p>
  </div>
</div>

<form class="stdform">
	<label>Tipe Order</label>
	<span class="field"><input type="text" class="smallinput" value="{{{ $data['ordertype'] }}}" readonly/></span>

	<label>No. Speedy</label>
	<span class="field"><input type="text" class="smallinput" value="{{{ $data['accountid'] }}}" readonly/></span>

	<label>Nama Pelanggan</label>
	<span class="field"><input type="text" class="smallinput" value="{{{ $data['custname'] }}}" readonly/></span>
</form><br>

<div class="contenttitle2"><h2>Perangkat yang dibooking</h2></div>

<form method="post" action="{{{ URL::to('bookingorder/save') }}}">
		<input type="hidden" name="ordertype" value="{{{ $data['ordertype'] }}}" />
		<input type="hidden" name="custname" value="{{{ $data['custname'] }}}" />
		<input type="hidden" name="accountid" value="{{{ $data['accountid'] }}}"/>
		<input type="hidden" name="orderedCount" value="{{{ $data['orderedCount'] }}}" />

<table class="stdtable" cellpadding="0" cellspacing="0" border="0">
  <thead>
    <tr>
      <th style="background-color:#C4C4C4">No</th>
      <th style="background-color:#C4C4C4">Nama Device</th>
      <th style="background-color:#C4C4C4">Jumlah Order</th>
      <th style="background-color:#C4C4C4">Total Harga Perangkat</th>
    </tr>
  </thead>
  <tbody>
    <?php $totalPrice = 0; ?>

    @for($i=1; $i <= count($data['id_device']); $i++)
		
		<input type="hidden" name="id_device_{{{ $i }}}" value="{{{ $data['id_device'][$i] }}}" />
		<input type="hidden" name="jumlah_{{{ $i }}}" value="{{{ $data['qty_device'][$i] }}}" />
		<input type="hidden" name="harga_{{{ $i }}}" value="{{{ $data['harga_device'][$i] }}}" />
		<input type="hidden" name="stock_{{{ $i }}}" value="{{{ $data['stock_device'][$i] }}}" />
      
      <tr>
        <td>{{{ $i }}}</td>
        <td>{{{ $data['nama_device'][$i] }}}</td>
        <td>{{{ $data['qty_device'][$i] }}}</td>
        <td>Rp. {{{ number_format($data['harga_device'][$i] * $data['qty_device'][$i], 2, ',', '.') }}}</td>
      </tr>
    
     <?php $totalPrice += $data['harga_device'][$i] * $data['qty_device'][$i]; ?>
    
    @endfor

    <tr>
      <td colspan="3"><strong><center>Total Device Order</center></strong></td>
      <td><strong>Rp. {{{ number_format($totalPrice, 2, ',', '.') }}}</strong></td>
    </tr>
  </tbody> 
</table>

	<p class="stdformbutton">
	  	<button class="stdbtn btn_orange">Submit Booking</button>
	</p>
	
	</form>
@stop
