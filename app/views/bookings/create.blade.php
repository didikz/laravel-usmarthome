@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>New Booking Order</h2></div>	
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
 @endif

 {{ Form::open(array('url'=>'bookingorder/confirm', 'class'=>'stdform')) }}
	<p>
         <label>Tipe Order:</label>
         <span class="field">
         	Aktivasi <input type="radio" value="activation" name="ordertype" checked />
         	Modifikasi <input type="radio" value="modification" name="ordertype" />
         </span>
    </p>
	<p>
         <label>No. Speedy:</label>
         <span class="field"><input type="text" name="accountid" class="smallinput" value="{{{ Input::old('accountid') }}}"/></span>
         <small class="desc">No. Speedy boleh dikosongi</small>
    </p>
    <p>
         <label>Nama Pelanggan:</label>
         <span class="field"><input type="text" name="custname" class="smallinput" value="{{{ Input::old('custname')}}}" required/> *)</span>
    </p>
    <label>Booking Device: </label>
         <span class="field">
			<table width="400">
				<tr>
					<th width="20%">Device</th>
                    <th width="5%">Harga</th>
					<th width="5%">Jumlah</th>
					<th width="5%">Stock</th>  
				</tr>
				<?php $no=1; ?>
				@foreach ($devices as $device)
				<tr>
		 			<input type="hidden" name="id_device_{{{ $no }}}" value="{{{ $device->id_devices }}}">
		 			<input type="hidden" name="stock_{{{ $no }}}" value="{{{ $device->stock }}}" />
		 			<td><a href="{{{ URL::to('managementdevices/detail/'.$device->id_devices)}}}">{{{ $device->nama_device }}}</a></td>
                    <td>Rp. {{{ number_format($device->harga, 2, ',', '.') }}}</td>
		 			<td><input type="text" name="jumlah_{{{ $no }}}" size="1" placeholder="0" value="{{{ Input::old('jumlah_'.$no) }}}"></td>
		 			<td><center>{{{ $device->stock }}}</center></td>
		 		</tr>
		 		<?php $no++; ?>
				@endforeach
		    </table>
    </span>
    <small class="desc">Untuk order aktivasi, wireless gateway harus masuk dalam perangkat yang dipesan (1 perangkat)</small>
    <p class="stdformbutton">
        {{ Form::submit('Confirm') }} &nbsp;
        <button class="stdbtn radius2" onclick="window.history.back()">Kembali</button>
    </p>
{{ Form::close() }}
@stop
