@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Edit Booking Order</h2></div>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
 @endif

 
 <form method="POST" action="{{ URL::to('bookingorder/update/'.$bookings->id)}}" class="stdform">
	<input type="hidden" name="id_order" value="{{{ $bookings->id }}}">
    <p>
         <label>Tipe Order:</label>
         <span class="field">
            @if($bookings->OrderType == 'activation')
                Aktivasi <input type="radio" value="activation" name="ordertype" checked />
                Modifikasi <input type="radio" value="modification" name="ordertype" />
            @else
                Aktivasi <input type="radio" value="activation" name="ordertype" />
                Modifikasi <input type="radio" value="modification" name="ordertype" checked />
            @endif       
         </span>
    </p>
    <p>
         <label>No Speedy:</label>
         <span class="field"><input type="text" name="accountid" class="smallinput" value="{{{ $bookings->AccountID }}}"/></span>
         <small class="desc">Masukkan nomor speedy jika nomor speedy sudah tersedia untuk proses order</small>
    </p>
    <p>
         <label>Nama Pelanggan:</label>
         <span class="field"><input type="text" name="custname" class="smallinput" value="{{{ $bookings->customer_name }}}"/> *)</span>
    </p>
    <label>Booking Device: </label>
         <span class="field">
			<table width="400">
				<tr>
					<th width="20%">Device</th>
                    <th width="5%">Harga</th>
					<th width="5%">Jumlah</th>
					<th width="5%">Stock</th>  
				</tr>
				<?php $no=1; ?>
				@foreach ($devices as $device)
				<tr>
		 			<input type="hidden" name="id_device_{{{ $no }}}" value="{{{ $device->id_devices }}}" />
		 			<input type="hidden" name="booked_{{{ $no }}}" value="{{{ $bookedDevice[$no-1] }}}" />
                    <input type="hidden" name="stock_{{{ $no }}}" value="{{{ $device->stock }}}" />
                    <input type="hidden" name="harga_{{{ $no }}}" value="{{{ $device->harga }}}" />
		 			
                    <td><a href="{{{ URL::to('managementdevices/detail/'.$device->id_devices)}}}">{{{ $device->nama_device }}}</a></td>
                    <td>Rp. {{{ number_format($device->harga, 2, ',', '.') }}}</td>
		 			<td><input type="text" name="jumlah_{{{ $no }}}" size="1" placeholder="0" value="{{{ $qty[$no-1]}}}" /></td>
		 			<td><center>{{{ $device->stock }}}</center></td>
		 		</tr>
		 		<?php $no++; ?>
				@endforeach
		    </table>
    </span>
    <small class="desc">Untuk order aktivasi, wireless gateway harus masuk dalam perangkat yang dipesan (1 perangkat)</small>
    <p class="stdformbutton">
        {{ Form::submit('Edit') }} &nbsp;
        <button class="stdbtn radius2" onclick="window.history.back()">Kembali</button>
    </p>
{{ Form::close() }}
@stop
