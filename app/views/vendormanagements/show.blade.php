@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Detail Mitra</h2></div><br>

<form class="stdform">
	
       <label>Nama Mitra</label>
       <span class="field"><input type="text" class="smallinput" value="{{{ $vendor->vendor_name }}}"/></span>
  
       <label>Lokasi Mitra</label>
       <span class="field"><input type="text" class="smallinput" value="{{{ $vendor->vendor_location }}}"/></span>
    
       <label>Alamat Mitra</label>
       <span class="field"><input type="text" class="longinput" value="{{{ $vendor->vendor_address }}}"/></span>
    
       <label>Telepon Mitra</label>
       <span class="field"><input type="text" class="smallinput" value="{{{ $vendor->vendor_phone }}}" /></span>
    
       <label>Email Mitra</label>
       <span class="field"><input type="text" class="smallinput" value="{{{ $vendor->vendor_email }}}" /></span>

       <label>Created at</label>
       <span class="field"><input type="text" class="smallinput" value="{{{ $vendor->created_at }}}" /></span>
	
	<p class="stdformbutton">
		<input type="button" class="stdbtn" value="Kembali" onclick="window.history.back()">
    </p>
		
</form>

@stop
