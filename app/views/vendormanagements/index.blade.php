@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Mitra Management</h2></div><br>

@if(Session::get('role')=='admin' || Session::get('roleid')=='1')
      <div><input type="button" class="stdbtn btn_lime" onClick="window.location.href='managementmitra/new'" value="New Mitra"></div><br><hr><br>
@endif

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif
<br>
<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
<thead>
    <tr>
       <th width="2%" class="head1">No.</th>
       <th width="10%" class="head1">Nama Mitra</th>
       <th width="10%" class="head1">Lokasi Mitra</th>
       <th width="10%" class="head1">Telepon Mitra</th>
       <th width="10%" class="head1">Email Mitra</th>
       <th width="10%" class="head1">Action</th>
    </tr>
</thead>
<tbody>
<?php $no=1; ?>
@foreach($vendor as $item)
	<tr>
		<td>{{{ $no }}}</td>
		<td>{{{ $item->vendor_name }}}</td>
		<td>{{{ $item->vendor_location }}}</td>
		<td>{{{ $item->vendor_phone }}}</td>
		<td>{{{ $item->vendor_email }}}</td>
		<td>
			<a href="{{{ URL::to('managementmitra/edit/'.$item->id) }}}">Edit</a> | 
			<a href="{{{ URL::to('managementmitra/delete/'.$item->id) }}}" onCLick="return confirm('apakah anda ingin menghapus Mitra {{ $item->vendor_name }}')">Delete</a> | 
			<a href="{{{ URL::to('managementmitra/detail/'.$item->id) }}}">Detail</a> | 
			<a href="{{{ URL::to('managementmitra/users/'.$item->id) }}}">Users</a>
		</td>
	</tr>
<?php $no++; ?>	
@endforeach
</tbody>
</table>
@stop
