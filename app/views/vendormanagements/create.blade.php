@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Create New Mitra</h2></div><br>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
  <span class="close"></span>
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif
<br>
<form class="stdform" method="post" action="{{{ URL::to('managementmitra/save') }}}">
	<p>
       <label>Nama Mitra</label>
       <span class="field"><input type="text" name="vendor_name" class="smallinput" value="" required/> *)</span>
    </p>
    <p>
       <label>Lokasi Mitra</label>
       <span class="field">
       	<select name="vendor_location">
       		<option value="0">-- Pilih lokasi --</option>
       		@foreach($witel as $location)
       			<option value="{{{ $location }}}">{{{ $location }}}</option>
       		@endforeach
       	</select>
       </span>
    </p>
    <p>
       <label>Alamat Mitra</label>
       <span class="field"><input type="text" name="vendor_address" class="longinput" value="" required/> *)</span>
    </p>
    <p>
       <label>Telepon Mitra</label>
       <span class="field"><input type="text" name="vendor_phone" class="smallinput" value="" required/> *)</span>
    </p>
    <p>
       <label>Email Mitra</label>
       <span class="field"><input type="text" name="vendor_email" class="smallinput" value="" required/> *)</span>
    </p>
    <p class="stdformbutton">
    	<button class="submit radius2">Submit</button>
      <input type="reset" class="reset radius2" value="Reset" />
    </p>
	
</form>
@stop
