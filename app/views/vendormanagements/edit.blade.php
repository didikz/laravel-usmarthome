@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Edit Mitra</h2></div><br>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
  <span class="close"></span>
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif
<br>
<form class="stdform" method="post" action="{{{ URL::to('managementmitra/update/'.$vendor->id) }}}">
	<p>
       <label>Nama Mitra</label>
       <span class="field"><input type="text" name="vendor_name" class="smallinput" value="{{{ $vendor->vendor_name }}}" required/> *)</span>
    </p>
    <p>
       <label>Lokasi Mitra</label>
       <span class="field">
       	<select name="vendor_location">
       		<option value="0">-- Pilih lokasi --</option>
       		@foreach($witel as $location)
	       			<option value="{{{ $location }}}" @if($vendor->vendor_location === $location) selected @endif>{{{ $location }}}</option>
       		@endforeach
       	</select>
       </span>
    </p>
    <p>
       <label>Alamat Mitra</label>
       <span class="field"><input type="text" name="vendor_address" class="longinput" value="{{{ $vendor->vendor_address }}}" required/> *)</span>
    </p>
    <p>
       <label>Telepon Mitra</label>
       <span class="field"><input type="text" name="vendor_phone" class="smallinput" value="{{{ $vendor->vendor_phone }}}" required/> *)</span>
    </p>
    <p>
       <label>Email Mitra</label>
       <span class="field"><input type="text" name="vendor_email" class="smallinput" value="{{{ $vendor->vendor_email }}}" required/> *)</span>
    </p>
    <p class="stdformbutton">
    	<button class="submit radius2">Submit</button>
      <input type="reset" class="reset radius2" value="Reset" />
    </p>
	
</form>
@stop
