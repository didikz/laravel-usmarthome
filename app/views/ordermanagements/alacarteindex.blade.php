@extends('layouts.master')

@section('content')
  <div class="contenttitle2"><h2>Order List</h2></div> <br>

  @if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
  @endif
 
<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
<thead>
<tr>
   <th width="1%" class="head0 nosort"></th>
   <th width="1%" class="head0 nosort">No.</th>
   <th width="8%" class="head1">Paket</th> 
   <th width="5%" class="head1">No Speedy</th>
   <th width="10%" class="head1">Nama Pelanggan</th>
   <th width="10%" class="head1">Perangkat Terpasang</th>
   <th width="10%" class="head1">Sisa Kuota Perangkat</th>  
   <th width="8%" class="head1 nosort">Action</th>
</tr>
</thead>
<tbody>
<?php $no=1; ?>
@foreach ($dataFab as $data)
  <tr class="gradeX">
        <td></td>
        <td>{{ $no }}</td>
        <td>{{ $package[$no-1] }}</td>
        <td>{{ $data->AccountID }}</td>
        <td>{{ $data->CustomerName }}</td>
        <td>{{{ $totalDevice[$no-1] }}} Perangkat</td>
        <td @if($kuota[$no-1] == '0') style="color: red;" @else style="color: blue;" @endif>
        	{{{ $kuota[$no-1] }}} perangkat
        </td>
        <td>
        	@if($kuota[$no-1] == '0')
        		-
        	@else
        		<a href="{{ URL::to('orderdevices/alacarte/new/'.$data->AccountID)}}">Ala Carte Order</a>
        	@endif
          
        </td>
      </tr>
   <?php $no++; ?>
@endforeach
</tbody>
</table>  

@stop