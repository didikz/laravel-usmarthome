@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Data Order Layanan</h2></div>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
@endif

<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
	      <thead>
          <tr>
             <th width="5%" class="head1">Order ID</th>
             <th width="5%" class="head1">Status</th>
             <th width="5%" class="head1">Order</th> 
             <th width="5%" class="head1">No. Speedy</th>
             <th width="8%" class="head1">Pelanggan</th>
             <th width="10%" class="head1">Tanggal Order</th>
             <th width="8%" class="head1 nosort">Action</th>
          </tr>
	      </thead>
	      <tbody>
	      	
	      		<tr class="gradeX">
	      			<td>{{ $data['TransId'] }}</td>
              		<td>{{ $data['Status'] }}</td>
              		<td>{{ $data['TransType'] }}</td>
              		<td>{{ $data['AccountID'] }}</td>
              		<td>{{ $data['CustomerName'] }}</td>
              		<td>{{ $data['lastUpdt'] }}</td>
              		<td>
                    @if(Request::segment(2)=='alacarte')
                      <a href="{{ URL::to('orderdevices/alacarte/new/'.$data->AccountID)}}">Ala Carte Order</a>
                    @else
                      <a href="{{ URL::to('orderdevices/new/'.$data['AccountID'].'/'.Request::segment(2)) }}">Order Device</a>
                    @endif
                      
                  </td>
              	</tr>
             
	      	
	      </tbody>
      </table>
@stop