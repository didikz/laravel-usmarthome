@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Speedy Reminder</h2></div>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
@endif

{{ Form::open(array('url'=>'ordermanagement/'.$order.'/savereminder', 'class'=>'stdform')) }}
	<input type="text" name="accountid" class="smallinput" placeholder="masukkan no speedy" value="{{{ Input::old('accountid') }}}" required/>
	{{ Form::submit('Save') }}
{{ Form::close() }}
<p>Speedy reminder merupakan fitur pengingat anda berupa notifikasi email untuk melakukan order device berdasarkan nomor speedy.</p>

@stop