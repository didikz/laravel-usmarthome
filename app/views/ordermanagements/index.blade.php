@extends('layouts.master')

@section('content')
  <div class="contenttitle2"><h2>Order {{$order}}</h2></div> <br>
  @if(Session::get('role')=='AM')
    <div><input type="button" class="stdbtn btn_lime" onClick="window.location.href='modifikasi/alacarte'" value="Alacarte"></div><br><hr><br>
  @endif

  @if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
  @endif
 
 <table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
  <thead>
    <tr>
       <th width="1%" class="head0 nosort"></th>
       <th width="1%" class="head0 nosort">No.</th>
       <th width="10%" class="head1">Paket</th> 
       <th width="5%" class="head1">No Speedy</th>
       <th width="10%" class="head1">Nama Pelanggan</th>
       <th width="10%" class="head1">Waktu Order</th>
       <th width="10%" class="head1">Umur Order</th>
       <th width="8%" class="head1">Status Order</th>
       <th width="8%" class="head1 nosort">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $no=1; ?>
    @foreach ($dataFab as $data)
      <tr class="gradeX">
            <td></td>
            <td>{{ $no }}</td>
            <td>{{ $packageName[$no-1] }}</td>
            <td>{{ $data->AccountID }}</td>
            <td>{{ $data->CustomerName }}</td>
            <td>{{ $data->lastUpdt }}</td>
            <td>{{ $durasi[$no-1] }}</td>
            <td>{{ $statusOrder[$no-1] }}</td>
            <td>
              
              @if(Session::get('role')=='AM' && $data->Status=='Instalasi' && $statusOrder[$no-1]=='Open')
                <a href="{{ URL::to('orderdevices/new/'.$data->AccountID.'/modification') }}">Order | </a>
              @endif
              <a href="{{ URL::to('ordermanagement/'.$order.'/detail/'.$data->id) }}">Detail</a>
            </td>
          </tr>
       <?php $no++; ?>
    @endforeach
  </tbody>
</table>  

@stop