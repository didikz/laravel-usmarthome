@extends('layouts.master')

@section('content')
<?php
      $requestDate = new DateTime($status['last_update']);
      $nowDate = new DateTime(date("Y-m-d H:i:s"));
      $selisih = $requestDate->diff($nowDate);
?>
<div class="contenttitle2"><h2>Detail Data Layanan</h2></div> <br>
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
         <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>Nomor order: </strong>{{ $status['id_req_status'] }} &nbsp; &nbsp; &nbsp; 
          <strong>Status: </strong> {{$status['status_order']}} &nbsp; &nbsp; &nbsp; 
          <strong>Umur order: </strong> {{ $selisih->d.' hari, '.$selisih->h.' jam, '.$selisih->i.' menit' }} &nbsp; &nbsp; &nbsp; 
          <strong>Keterangan: </strong>{{ $status['keterangan'] }}</p>
  </div>
</div>

<form class="stdform">

   <label>Order ID:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['TransId'] }}}" readonly /></span>

   <label>Transaksi</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['TransType'] }}}" readonly /></span>

   <label>Status</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fabstatus }}}" readonly /></span>

   <label>No Speedy:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['AccountID'] }}}" readonly /></span>

   <label>Paket:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $paket }}}" readonly /></span>

   <label>Customer ID:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['Cust_id'] }}}" readonly /></span>

   <label>Promo ID:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['PromoID'] }}}" readonly /></span>

   <label>Nama Pelanggan:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['CustomerName'] }}}" readonly /></span>

   <label>Alamat:</label>
   <span class="field"><input type="text" class="largeinput" value="{{{ $fab['Address'] }}}" readonly /></span>

   <label>Developer ID:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['DeveloperID'] }}}" readonly /></span>

   <label>Perusahaan Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['DeveloperCompany'] }}}" readonly /></span>

   <label>Nama Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['DeveloperName'] }}}" readonly /></span>

   <label>Telepon Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['DeveloperTelp'] }}}" readonly /></span>

   <label>Email Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['DeveloperEmail'] }}}" readonly /></span>

<br>        
<table class="stdtable" cellpadding="0" cellspacing="0" border="0">
<thead>
  <tr>
  <th width="10%" class="head1">Status Order</th>
  <th width="10%" class="head1">Waktu Mulai</th>
  <th width="10%" class="head1">Waktu Selesai</th>
  <th width="10%" class="head1">Durasi Waktu</th>
  <th width="20%" class="head1">Keterangan</th>
  
</tr>
</thead>
<tbody>
@foreach($statusLog as $log)
<?php
    $start = new DateTime($log->start_update);
    $end = new DateTime($log->last_update);
    $interval = $start->diff($end);
    $duration = $interval->d.' hari, '.$interval->h.' jam, '.$interval->i.' menit';  
?>    
  <tr>
    <td>{{ $log->status_order }}</td>
    <td>{{ $log->start_update }}</td>
    <td>{{ $log->last_update }}</td>
    <td>{{ $duration }}</td>
    <td>{{ $log->keterangan }}</td>
    
  </tr>
@endforeach  
</tbody>
</table>
@stop