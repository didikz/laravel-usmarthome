@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Pencarian no Speedy</h2></div><br>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div><br>
@endif

@if($order == 'modifikasi')
<div class="contenttitle2"><h3>Order Alacarte</h3></div>

{{ Form::open(array('url'=>'ordermanagement/modifikasi/alacarte', 'class'=>'stdform', 'method'=>'get')) }}
	<input type="text" name="accountid" class="smallinput" placeholder="masukkan no speedy untuk pemesanan alacarte" value="{{{ Input::old('accountid') }}}" required/>
	{{ Form::submit('search') }}
{{ Form::close() }}<br><hr><br>

@endif

<div class="contenttitle2"><h3>Order {{{ $order }}}</h3></div>

{{ Form::open(array('url'=>'ordermanagement/'.$order.'/searchspeedy', 'class'=>'stdform', 'method'=>'get')) }}
	<input type="text" name="accountid" class="smallinput" placeholder="masukkan no speedy untuk pemesananan order" value="{{{ Input::old('accountid') }}}" required/>
	{{ Form::submit('search') }}
{{ Form::close() }}

@stop
