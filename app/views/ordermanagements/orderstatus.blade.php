@extends('layouts.master')

@section('content')
<h1>Ubah status order {{{ $type }}}</h1>
<form class="stdform" method="post" action="{{ URL::to('ordermanagement/'.$type.'/status/store') }}">
        <input type="hidden" name="idreqstatus" value="{{ $data->id_req_status }}">
        <input type="hidden" name="accountid" value="{{ $data->AccountID}}" >
        <input type="hidden" name="transid" value="{{ $data->TransId }}" >
        <p>
          <label>Trans Type: </label>
          <span class="field">{{ $data->TransType }}</span>
        </p>
        <p>
          <label>Status: </label>
          <span class="field">{{ $data->Status }}</span>
        </p>
        <p>
          <label>Trans ID: </label>
          <span class="field">{{ $data->TransId }}</span>
        </p>
        <p>
          <label>Customer Name: </label>
          <span class="field">{{ $data->CustomerName }}</span>
        </p>

        <p>
          <label>Status Order: </label>
          @if($data->status_order == 'Closed')
              <span class="field">Order closed</span>
          @else
              <span class="field">
                <select name="statusorder">
                @if($data->status_order == 'Cancel')
                	<option value="Cancel" selected>Cancel</option>
                @else
                	<option value="Open" selected disabled>Open</option>
                  	<option value="Cancel">Cancel</option>	
                @endif  	  	 
                </select>
              </span>
              </p>
              <p>
                <label>Keterangan: </label>
                <span class="field"><input type="text" name="keterangan" class="mediuminput" value="{{ $data->keterangan }}"></span>
              </p>
              <p class="stdformbutton">
                <button class="submit radius2">Submit</button>
                <input type="reset" class="reset radius2" value="Reset" />
              </p>
          @endif
</form>
@stop