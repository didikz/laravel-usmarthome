@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Mapping New Gateway</h2></div><br>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif

<form class="stdform" method="post" action="../../save">
	<input type="hidden" name="accountid" value="{{{ $data->AccountID }}}" />
	<input type="hidden" name="fabid" value="{{{ $data->id }}}" />
	<input type="hidden" name="devemail" value="{{{ $data->DeveloperEmail }}}" />
	<input type="hidden" name="package" value="{{{ $data->PackageID }}}"/> 	
	<input type="hidden" name="reqid" value="{{{ $reqid }}}" />
	<input type="hidden" name="transid" value="{{{ $data->TransId }}}" />
	<input type="hidden" name="idsetter" value="{{{ $idsetter }}}" />
	<input type="hidden" name="idassign" value="{{{ $idassign }}}" />
	
    <p>
       <label>Email</label>
       <span class="field"><input type="text" name="telkomid" class="smallinput" value="{{{ $data->DeveloperEmail }}}" required/> *)</span>
		   <small class="desc">Email pelanggan digunakan sebagai username login aplikasi</small>
    </p>
    <p>
       <label>MAC Address</label>
       <span class="field"><input type="text" name="macaddress" class="smallinput" value="{{{ Input::old('macaddress') }}}" required/> *)</span>
       <small class="desc">Isikan Mac address yang telah dipasang. Contoh: 00CDB3442414</small>
    </p>
    <p>
       <label>Alamat</label>
       <span class="field"><input type="text" name="alamat" class="longinput" value="{{{ $data->Address }}}" required/> *)</span>
    </p>
    <p>
       <label>No. Telepon</label>
       <span class="field"><input type="text" name="telpon" class="smallinput" value="{{{ $phone }}}" required/> *)</span>
    </p>
    <p>
       <label>Facebook</label>
       <span class="field"><input type="text" name="facebook" class="smallinput" value="{{{ Input::old('facebook') }}}"/></span>
       <small class="desc">Contoh: https://www.facebook.com/profile</small>
    </p>
    <p>
       <label>Twitter</label>
       <span class="field"><input type="text" name="twitter" class="smallinput" value="{{{ Input::old('twitter') }}}"/></span>
       <small class="desc">Contoh: https://www.twitter.com/username</small>
    </p>
    <p class="stdformbutton">
      	<button class="submit radius2">Submit</button>
        <input type="reset" class="reset radius2" value="Reset" />
    </p>
</form>
@stop
