@extends('layouts.master')

@section('content')

<div class="contenttitle2"><h2>Order List</h2></div><br>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif

<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
<thead>
    <tr>
       <th width="2%" class="head1">No.</th>
       <th width="10%" class="head1">TransType</th>
       <th width="10%" class="head1">No. Speedy</th>
       <th width="10%" class="head1">Nama Pelanggan</th>
       <th width="10%" class="head1">Last Update</th>
       <th width="5%" class="head1">Action</th>
    </tr>
</thead>
<tbody>
<?php $no=1; ?>
@foreach($fabs as $fab)
	<tr>
		<td>{{{ $no }}}</td>
		<td>{{{ $fab->TransType }}}</td>
		<td>{{{ $fab->AccountID }}}</td>
		<td>{{{ $fab->CustomerName }}}</td>
		<td>{{{ $fab->lastUpdt }}}</td>
		<td>
		@if($exist[$no-1]=='0')
			<a href="{{{ URL::to('mapping/new/'.$fab->id.'/'.$fab->id_req_status)}}}" style="color:red;">Start Mapping</a>
		@else
			Mapping done
		@endif	
		</td>
	</tr>
<?php $no++; ?>		
@endforeach
</tbody>
</table>

@stop