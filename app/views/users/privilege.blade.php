@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>User Role & Privilege</h2></div><br>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif

<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
	<thead>
		<tr>
			<th class="head1">No.</th>
			<th class="head1">Role</th>
			<th class="head1">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; ?>
	@foreach($role as $item)	
		<tr>
			<td>{{{ $no }}}</td>
			<td>{{{ $item->role_name}}}</td>
			<td>
				<a href="{{{ URL::to('users/privilege/set/'.$item->id) }}}">Privilege</a>
			</td>
		</tr>
	<?php $no++; ?>	
	@endforeach	
	</tbody>
</table>

@stop
