@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Set Privileges</h2></div><br>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif

<form class="stdform" method="post" action="{{{ URL::to('users/privilege/update/'.$role->id) }}}">
	<p>
	 <label>Role: </label>
	 <span class="field"><input type="text" class="smallinput" value="{{{ $role->role_name }}}" readonly/></span>
  	</p>
  	<p>
	 <label>Privilege Menu: </label>
	 <span class="formwrapper">
		
	 	@foreach($module as $modul)
	 		@if(in_array($modul->id, $privileges))
	 			<input type="checkbox" name="cek_{{ $modul->id }}" checked /> {{{ $modul->module_name }}}<br />
	 			<input type="hidden" name="module_{{ $modul->id }}" value="1" />
	 		@else
	 			<input type="checkbox" name="cek_{{ $modul->id }}" /> {{{ $modul->module_name }}}<br />
	 			<input type="hidden" name="module_{{ $modul->id }}" value="0" />
	 		@endif
	 	@endforeach

	 </span>
  	</p>
  	              
    <p class="stdformbutton">
    	<button class="submit radius2">Update</button>
    </p>

</form>
@stop
