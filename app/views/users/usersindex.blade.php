@extends('layouts.master');

@section('content')
<h1>List Users</h1><br>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
 @endif
<div><input type="button" class="stdbtn btn_lime" onClick="window.location.href='users/new'" value="New Users"></div><br><hr><br>
<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
<thead>
  <tr>
     <th width="5%" class="head1">No.</th>
     <th width="10%" class="head1">Username</th>
     <th width="10%" class="head1">Nama Lengkap</th>
     <th width="10%" class="head1">Email</th>
     <th width="14%" class="head1">Level</th>
     <th width="14%" class="head1">Action</th>
  </tr>
</thead>
<tbody>
<?php $no= 1; ?>	
@foreach ($users as $user)
	<tr>
		<td>{{{ $no }}}</td>
		<td>{{ $user->username }}</td>
    <td>{{{ $nama[$no-1] }}}</td>
    <td>{{{ $email[$no-1] }}}</td>
		<td>{{ $user->LEVEL }}</td>
		<td><a href="{{ URL::to('users/edit/'.$user->ID) }}">Edit</a> | <a href="{{ URL::to('users/delete/'.$user->ID) }}" onCLick="return confirm('apakah anda ingin menghapus user {{$user->username }}')">Delete</a></td>
	</tr>
<?php $no++; ?>	
@endforeach
</tbody>
</table>	
@stop