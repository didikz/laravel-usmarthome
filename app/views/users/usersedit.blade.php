@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Edit Users</h2></div><br>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
@endif
<form class="stdform" method="post" action="../update/{{ $user->ID }}" />
<input type="hidden" name="userid" value="{{ $user->ID }}" />
<input type="hidden" name="masterid" value="{{ $master['id'] }}" />
	<p>
       <label>Username</label>
       <span class="field"><input type="text" name="username" class="smallinput" value="{{ $user->username}}" readonly/></span>
           <small class="desc">Username tidak bisa diganti</small>
    </p>
    <p>
       <label>Password</label>
       <span class="field"><input type="password" name="password" class="smallinput" /></span>
           <small class="desc">Kosongkan password bila tidak diganti</small>
    </p>
    <p>
       <label>Nama User</label>
       <span class="field"><input type="text" name="nama" class="smallinput" value="{{{ $master['nama'] }}}" required/> *)</span>
    </p>
    <p>
       <label>Email</label>
       <span class="field"><input type="text" name="email" class="smallinput" value="{{{ $master['email'] }}}" required/> *)</span>
    </p>
    <p>
       <label>Telepon</label>
       <span class="field"><input type="text" name="telepon" class="smallinput" value="{{{ $master['telepon'] }}}" required/> *)</span>
    </p>
    <p>
       <label>Loker</label>
       <span class="field">
        <select name="loker">
        @foreach($propinsi as $loker)
          <option value="{{{ $loker }}}" @if($master['loker']==$loker) selected @endif>{{{ $loker }}}</option>
        @endforeach
        </select>  *)
       </span>
    </p>
    <p>
       <label>Level</label>
       <span class="field">
       	<select name="level">
       	@if ($user->LEVEL == 'mitra')
       		<option value="mitra" selected>Mitra</option>
       		<!-- <option value="admin">admin</option>
       		<option value="ESA DSC">ESA DSC</option>
       		<option value="CO AM">CO AM</option>
       		<option value="AM">AM</option>
       		<option value="setter">Setter</option> -->
       	@elseif($user->LEVEL == 'setter')
       		<!-- <option value="mitra">Mitra</option>
       		<option value="admin">admin</option>
       		<option value="ESA DSC">ESA DSC</option>
       		<option value="CO AM">CO AM</option>
       		<option value="AM">AM</option> -->
       		<option value="setter" selected>Setter</option>
       	@elseif($user->LEVEL == 'ESA DSC')
       		<!-- <option value="mitra">Mitra</option>
       		<option value="admin">admin</option> -->
       		<option value="ESA DSC" selected>ESA DSC</option>
       		<option value="CO AM">CO AM</option>
       		<!-- <option value="AM">AM</option>
       		<option value="setter">Setter</option> -->
       	@elseif($user->LEVEL == 'AM')
	       	<!-- <option value="mitra">Mitra</option>
       		<option value="admin">admin</option>
       		<option value="ESA DSC">ESA DSC</option>
       		<option value="CO AM">CO AM</option> -->
       		<option value="AM" selected>AM</option>
       		<!-- <option value="setter">Setter</option>	 -->
       	@elseif($user->LEVEL == 'CO AM')
<!-- 	       	<option value="mitra">Mitra</option>
       		<option value="admin">admin</option> -->
       		<option value="ESA DSC">ESA DSC</option>
       		<option value="CO AM" selected>CO AM</option>
       		<!-- <option value="AM">AM</option>
       		<option value="setter">Setter</option> -->
       	@elseif($user->LEVEL == 'admin')
	       	<!-- <option value="mitra">Mitra</option> -->
       		<option value="admin" selected>admin</option>
       		<!-- <option value="ESA DSC">ESA DSC</option>
       		<option value="CO AM">CO AM</option>
       		<option value="AM">AM</option>
       		<option value="setter">Setter</option>		 -->
       	@endif
       	</select>
       </span>
          <!--  <small class="desc">Order ID</small> -->
    </p>
    <p class="stdformbutton">
      	<button class="submit radius2">Submit</button>
        <input type="reset" class="reset radius2" value="Reset" />
    </p>	
</form>
@stop
