@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>New Users</h2></div><br>
@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif
<form class="stdform" method="post" action="save" />
	<p>
     <label>Username</label>
     <span class="field"><input type="text" name="username" class="smallinput" required/> *)</span>
         <small class="desc">Username dipergunakan untuk login dan sifatnya unique</small>
  </p>
  <p>
     <label>Password</label>
     <span class="field"><input type="password" name="password" class="smallinput" required/> *)</span>
  </p>
  <p>
     <label>Nama Lengkap</label>
     <span class="field"><input type="text" name="nama" class="smallinput" required/> *)</span>
  </p>
  <p>
     <label>Email</label>
     <span class="field"><input type="text" name="email" class="smallinput" required/> *)</span>
  </p>
  <p>
     <label>Telepon</label>
     <span class="field"><input type="text" name="telepon" class="smallinput" required/> *)</span>
  </p>
  <p>
     <label>Loker</label>
     <span class="field">
      <select name="loker">
      @foreach($propinsi as $loker)
        <option value="{{{ $loker }}}">{{{ $loker }}}</option>
      @endforeach
      </select>  *)
     </span>
  </p>
  <p>
     <label>Level</label>
     <span class="field">
     	<select name="level">
     		<option value="ESA DSC">ESA DSC</option>
           <option value="CO AM">Koordinator AM</option>
     	</select> *)
     </span>
  </p>
  <p class="stdformbutton">
    	<button class="submit radius2">Submit</button>
      <input type="reset" class="reset radius2" value="Reset" />
  </p>	
</form>

@stop
