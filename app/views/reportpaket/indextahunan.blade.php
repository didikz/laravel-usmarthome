@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>{{{ $data['title'] }}}</h2></div>

<div class="overviewhead">
<form class="stdform" method="get" action="{{ URL::to('reportpaket/filtertahunan')}}">
  <select name="year">
    <option value="0">- Pilih tahun -</option>
    @foreach ($data['dynYear'] as $dyn)
      <option value="{{{ $dyn }}}">{{{ $dyn }}}</option>
    @endforeach
  </select>
  <input type="submit" value="Filter" id="submit"> 
</form>
</div><br>

<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
  <thead>
      <tr>
         <th class="head1">Paket</th> 
         <th class="head1">Jan</th>
         <th class="head1">Feb</th>
         <th class="head1">Mar</th>
         <th class="head1">Apr</th>
         <th class="head1">Mei</th>
         <th class="head1">Jun</th>
         <th class="head1">Jul</th>
         <th class="head1">Agt</th>
         <th class="head1">Sep</th>
         <th class="head1">Okt</th>
         <th class="head1">Nov</th>
         <th class="head1">Des</th>
         <th class="head1">Total</th>
      </tr>
  </thead>
  <tbody>
  <?php $totalPackage=0; $i= 0;?>	
  @for($packages=1; $packages <= $data['jumlahPaket']; $packages++)
  	<tr>
	  	<td>{{{ $paket[$packages] }}}</td>
	  	@foreach ($count[$packages] as $item)
	  	
        <?php $totalPackage += $item; ?>
  	  	<td>
  	  		<center><a href="{{{ URL::to('reportpaket/tahunan/show/'.$packages.'/'.$data['year'].'-'.$month[$i]) }}}">{{{ $item }}}</a></center>
  	  	</td>
	  	<?php $i++; ?>
      @endforeach
      <?php $i=0; ?>
	  	<td> <center><a href="{{{ URL::to('reportpaket/tahunan/show/'.$packages.'/'.$data['year'])}}}">{{{ $totalPackage }}}</a> </center> </td>
	  	<?php $totalPackage=0; ?>
	</tr>
  @endfor 
  </tbody>
 </table> <br>

 <form method="get" action="{{{ URL::to(Request::path()) }}}">
  <input type="hidden" name="year" value="{{{ $year }}}" />
  <input type="submit" name="download" value="Download as xls" /> 
</form>

@stop
