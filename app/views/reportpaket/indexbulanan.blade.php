@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>{{{ $data['title'] }}}</h2></div>
<div class="overviewhead">
  <form class="stdform" method="get" action="{{ URL::to('reportpaket/filterbulanan')}}">
    <select name="month">
      <option value="0">- Pilih bulan -</option>
      <option value="01">Januari</option>
      <option value="02">Februari</option>
      <option value="03">Maret</option>
      <option value="04">April</option>
      <option value="05">Mei</option>
      <option value="06">Juni</option>
      <option value="07">Juli</option>
      <option value="08">Agustus</option>
      <option value="09">September</option>
      <option value="10">Oktober</option>
      <option value="11">November</option>
      <option value="12">Desember</option>
    </select>
    <select name="year">
      <option value="0">- Pilih tahun -</option>
      @foreach ($data['dynYear'] as $dyn)
        <option value="{{{ $dyn }}}">{{{ $dyn }}}</option>
      @endforeach
    </select>
    <input type="submit" value="Filter" > 
  </form>
</div><br>
<table cellpadding='0' cellspacing='0' border='0' width="80%" class='stdtable' style='font-size:10px; padding:0;'>
    <thead>
    <tr>
    	<th class='head1'>Paket</th>
    	@for($hari=1; $hari<=$data['days']; $hari++)
    	<th class='head1'>{{{ $hari }}}</th>
    	@endfor
    	<th class='head1'>Total</th>
    </tr>
    <tbody>
	<?php $totalPackage=0; $i= 0; ?>
	@for($packages=1; $packages <= $data['jumlahPaket']; $packages++)
    	<tr>
    		<td>{{{ $paket[$packages] }}}</td>
    		@foreach($count[$packages] as $item)
    		<?php $totalPackage += $item; $i++; ?>
	    		<td><a href="{{{ URL::to('reportpaket/bulanan/show/'.$packages.'/'.$data['year'].'-'.$data['month'].'-'.$i) }}}">{{{ $item }}}</a> </td>
    		@endforeach
    		<td><a href="{{{ URL::to('reportpaket/bulanan/show/'.$packages.'/'.$data['year'].'-'.$data['month']) }}}">{{{ $totalPackage }}}</a> </td>
    		<?php $totalPackage=0; $i=0;?>
    	</tr>
    @endfor 
    </tbody>
</table><br>

<form method="get" action="{{{ URL::to(Request::path()) }}}">
  <input type="hidden" name="month" value="{{{ $data['month'] }}}" />
  <input type="hidden" name="year" value="{{{ $data['year'] }}}" />
  <input type="submit" name="download" value="Download as xls" /> 
</form>
    	
@stop
