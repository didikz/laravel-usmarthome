@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Daftar Rekap Order</h2></div>
<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
  <thead>
    <tr>
       <th width="1%" class="head0 nosort"></th>
       <th width="1%" class="head0 nosort">No.</th>
       <th width="5%" class="head1">Tipe Order</th>
       <th width="10%" class="head1">Paket</th> 
       <th width="5%" class="head1">No. Speedy</th>
       <th width="10%" class="head1">Nama Pelanggan</th>
       <th width="10%" class="head1">Tanggal Order</th>
       
       <th width="8%" class="head1 nosort">Action</th>
    </tr>
  </thead>
  <tbody>
  <?php $no=1; ?>	
  @foreach($fab as $data)
  	<tr class="gradeX">
  		<td></td>
  		<td>{{{ $no }}}</td>
  		<td>{{{ $data->TransType }}}</td>
  		<td>{{{ $paket[$data->PackageID] }}}</td>
  		<td>{{{ $data->AccountID }}}</td>
  		<td>{{{ $data->CustomerName }}}</td>
      
      <td>{{{ $data->lastUpdt }}}</td>
  		<td><a href="{{ URL::to('reportpaket/detail/'.$data->id) }}">Detail</a></td>
  	</tr>
  <?php $no++; ?>		
  @endforeach	
  </tbody>
</table>
@stop
