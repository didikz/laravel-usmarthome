@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>{{{ $title }}}</h2></div>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div><!-- notification announcement -->
  </div>
@endif

<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
	<thead>
		<tr>
			<th width="1%" class="head0 nosort"></th>
			<th width="1%" class="head1 nosort">No.</th>
			<th width="2%" class="head1">Tipe Order</th>
			<th width="2%" class="head1">No. Speedy</th>
			<th width="5%" class="head1">Nama Pelanggan</th>
			<th width="5%" class="head1">Tanggal Order</th>
			<th width="5%" class="head1">Status Order</th>
			<th width="5%" class="head1">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; ?>
	@foreach($allData as $order)
		<tr>
			<td></td>
			<td>{{{ $no }}}</td>
			<td>{{{ $order->OrderType }}}</td>
			<td>{{{ $order->cust_id }}}</td>
			<td>{{{ $order->cust_name }}}</td>
			<td>{{{ $order->tanggal_order }}}</td>
			<td>{{{ $order->status }}}</td>
			<td><a href="{{{ URL::to('orderdevices/detail/'.$order->id_status) }}}">Detail</a></td>
		</tr>
	<?php $no++; ?>	
	@endforeach
	</tbody>
</table>
<br>
<input type="button" class="stdbtn" value="Kembali" onclick="window.history.back()">
@stop
