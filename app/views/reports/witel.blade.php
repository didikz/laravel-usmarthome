@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Report Order by Witel</h2></div>

@if(Session::has('message'))
  <div id="updates" class="subcontent">
      <div class="notibar announcement">
             <p>{{ Session::get('message') }}</p>
      </div>
  </div>
@endif

<form class="stdform" method="get" action="{{{ URL::to('report/witel/filter') }}}">
	<p>
       <label>Witel:</label>
       <span class="field">
       		<select name="witel">
       			<option value="0">-- Pilih witel --</option>
       			@foreach($witels as $witel)
       			<option value="{{{ $witel }}}">{{{ $witel }}}</option>
       			@endforeach
       		</select> *)
       </span>
    </p>
    <p>
    	<label>Periode:</label>
    	<span class="field">
    		<select name="month">
		      <option value="0">- Pilih bulan -</option>
		      <option value="01">Januari</option>
		      <option value="02">Februari</option>
		      <option value="03">Maret</option>
		      <option value="04">April</option>
		      <option value="05">Mei</option>
		      <option value="06">Juni</option>
		      <option value="07">Juli</option>
		      <option value="08">Agustus</option>
		      <option value="09">September</option>
		      <option value="10">Oktober</option>
		      <option value="11">November</option>
		      <option value="12">Desember</option>
		    </select>
		    <select name="year">
			    <option value="0">-- Pilih tahun --</option>
			    @foreach ($dynYear as $dyn)
			      <option value="{{{ $dyn }}}">{{{ $dyn }}}</option>
			    @endforeach
			 </select> *)
    	</span>
    </p>
    <p class="stdformbutton">
      	<button class="submit radius2">Submit</button>
        <input type="reset" class="reset radius2" value="Reset" />
    </p>
</form>
@stop
