@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Rekap Order</h2></div>
<div class="overviewhead">
  <form method="get" action="{{ URL::to('report/filterbyrange') }}">
    Dari: 
    <select name="dayFrom">
      @for ($from=1; $from <= 31; $from++)
      <option value="{{{ $from }}}">{{{ $from }}}</option>
      @endfor
    </select>
    <select name="monthFrom">
      <option value="01">Januari</option>
      <option value="02">Februari</option>
      <option value="03">Maret</option>
      <option value="04">April</option>
      <option value="05">Mei</option>
      <option value="06">Juni</option>
      <option value="07">Juli</option>
      <option value="08">Agustus</option>
      <option value="09">September</option>
      <option value="10">Oktober</option>
      <option value="11">November</option>
      <option value="12">Desember</option>
    </select>
    <select name="yearFrom">
      @foreach ($dynYear as $dyn)
        <option value="{{{ $dyn }}}">{{{ $dyn }}}</option>
      @endforeach
    </select>
    Sampai:
    <select name="dayTo">
      @for ($to=1; $to <= 31; $to++)
      <option value="{{{ $to }}}">{{{ $to }}}</option>
      @endfor
    </select>
    <select name="monthTo">
      <option value="01">Januari</option>
      <option value="02">Februari</option>
      <option value="03">Maret</option>
      <option value="04">April</option>
      <option value="05">Mei</option>
      <option value="06">Juni</option>
      <option value="07">Juli</option>
      <option value="08">Agustus</option>
      <option value="09">September</option>
      <option value="10">Oktober</option>
      <option value="11">November</option>
      <option value="12">Desember</option>
    </select>
    <select name="yearTo">
      @foreach ($dynYear as $dyn)
        <option value="{{{ $dyn }}}">{{{ $dyn }}}</option>
      @endforeach
    </select>
    <input type="submit" value="Filter">
  </form>
</div>  
<br>

<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
	<thead>
	  <tr>
	     <th class="head1">Order</th> 
	     <th class="head1">Jumlah Order</th>
	  </tr>
	</thead>
	<tbody>
	<tr>
		<td>ACTIVATION</td>
		<td>{{{ $activationCount }}}</td>
	</tr>
	<tr>
		<td>MODIFICATION</td>
		<td>{{{ $modificationCount }}}</td>
	</tr>
	<tr>
		<td>SUSPENSION</td>
		<td>{{{ $suspensionCount }}}</td>
	</tr>
	<tr>
		<td>RESUMPTION</td>
		<td>{{{ $resumptionCount }}}</td>
	</tr>
	<tr>
		<td>TERMINATION</td>
		<td>{{{ $terminationCount }}}</td>
	</tr>
	<tfoot>
	<tr>
		<th class="head1">Total</th>
		<th class="head1">{{{ $total }}}</th>
	</tr>
	</tfoot>	
	</tbody>
</table>
@stop
