@extends('layouts.master')

@section('content')
<?php 
$totalActivation=0; $totalModification=0; $totalSuspension=0; $totalResumption=0; $totalTermination=0; $totalMonthly=0; 
$i   = 0;
$ii  = 0;
$iii = 0;
$iv  = 0;
$v   = 0;
$vi  = 0;
?>
<div class="contenttitle2"><h2>Rekap Order Tahun {{{ $year }}}</h2></div>
<div class="overviewhead">
<form class="stdform" method="get" action="{{ URL::to('report/filtertahunan')}}">
  <select name="year">
    <option value="0">- Pilih tahun -</option>
    @foreach ($dynYear as $dyn)
      <option value="{{{ $dyn }}}">{{{ $dyn }}}</option>
    @endforeach
  </select>
  <input type="submit" value="Filter" id="submit"> 
</form>
</div>
<br>
<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
      <thead>
          <tr>
             <th class="head1">Order</th> 
             <th class="head1">Jan</th>
             <th class="head1">Feb</th>
             <th class="head1">Mar</th>
             <th class="head1">Apr</th>
             <th class="head1">Mei</th>
             <th class="head1">Jun</th>
             <th class="head1">Jul</th>
             <th class="head1">Agt</th>
             <th class="head1">Sep</th>
             <th class="head1">Okt</th>
             <th class="head1">Nov</th>
             <th class="head1">Des</th>
             <th class="head1">Total</th>
          </tr>
      </thead>
      <tbody>
      <tr>
      	<td><strong>ACTIVATION</strong></td>
      	@foreach($activation as $act)
	      	<?php $totalActivation += $act; ?>
      		<td><a href="{{ URL::to('report/tahunan/activation/'.$month[$i].'-'.date('Y')) }}">{{{ $act }}}</a> </td>
      		<?php $i++; ?>
      	@endforeach
      	<?php ?>
      	<td><a href="{{ URL::to('report/tahunan/activation/total-'.date('Y')) }}">{{{ $totalActivation }}}</a></td>
      </tr>
      <tr>
      	<td><strong>MODIFICATION</strong></td>
      	@foreach($modification as $mc)
	      	<?php $totalModification += $mc ?>
      		<td><a href="{{ URL::to('report/tahunan/modification/'.$month[$ii].'-'.date('Y')) }}">{{{ $mc }}}</a> </td>
      		<?php $ii++; ?>
      	@endforeach
      	<td><a href="{{ URL::to('report/tahunan/modification/total-'.date('Y')) }}">{{{ $totalModification }}}</a></td>
      </tr>
      <tr>
      	<td><strong>SUSPENSION</strong></td>
      	@foreach($suspension as $susp)
	      	<?php $totalSuspension += $susp ?>
      		<td><a href="{{ URL::to('report/tahunan/suspension/'.$month[$iii].'-'.date('Y')) }}">{{{ $susp }}}</a> </td>
      		<?php $iii++; ?>
      	@endforeach
      	<td><a href="{{ URL::to('report/tahunan/suspension/total-'.date('Y')) }}">{{{ $totalSuspension }}}</a></td>
      </tr>
      <tr>
      	<td><strong>RESUMPTION</strong></td>
      	@foreach($resumption as $res)
	      	<?php $totalResumption += $res ?>
      		<td><a href="{{ URL::to('report/tahunan/resumption/'.$month[$iv].'-'.date('Y')) }}">{{{ $res }}}</a> </td>
      		<?php $iv++; ?>
      	@endforeach
      	<td><a href="{{ URL::to('report/tahunan/resumption/total-'.date('Y')) }}">{{{ $totalResumption }}}</a></td>
      </tr>
      <tr>
      	<td><strong>TERMINATION</strong></td>
      	@foreach($termination as $term)
	      	<?php $totalTermination += $term ?>
      		<td><a href="{{ URL::to('report/tahunan/termination/'.$month[$v].'-'.date('Y')) }}">{{{ $term }}}</a> </td>
      		<?php $v++; ?>
      	@endforeach
      	<td><a href="{{ URL::to('report/tahunan/termination/total-'.date('Y')) }}">{{{ $totalTermination }}}</a></td>
      </tr>
      <tfoot>
      <tr>
      	<th class='head1'><strong>TOTAL</strong></th>
      	@foreach($monthly as $monthCount)
	      	<?php $totalMonthly += $monthCount; ?>
      		<th class='head1'><a href="{{ URL::to('report/tahunan/monthly/'.$month[$vi].'-'.date('Y')) }}">{{{ $monthCount }}}</a></th>
      		<?php $vi++; ?>	
      	@endforeach
      	<th class='head1'><a href="{{ URL::to('report/tahunan/'.date('Y')) }}">{{{ $totalMonthly }}}</a></th>
      </tr>
      </tfoot>	
      </tbody>
</table><br>
<form method="get" action="{{{ URL::to(Request::path()) }}}">
  <input type="hidden" name="year" value="{{{ $year }}}" />
  <input type="submit" name="download" value="Download as xls" /> 
</form>

@stop
