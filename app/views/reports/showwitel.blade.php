@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>{{{ $data['title'] }}}</h2></div>

<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
	<thead>
		<tr>
			<th class="head1">No.</th>
			<th class="head1">Order</th>
			<th class="head1">Jumlah</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>Activation</td>
			<td><a href="{{{ URL::to('report/witel/activation/'.urlencode($data['witel']).'/'.$data['month'].'/'.$data['year']) }}}">{{{ $data['activation'] }}}</a></td>
		</tr>
		<tr>
			<td>2</td>
			<td>Modification</td>
			<td><a href="{{{ URL::to('report/witel/modification/'.urlencode($data['witel']).'/'.$data['month'].'/'.$data['year']) }}}">{{{ $data['modification'] }}}</a> </td>
		</tr>
	<tfoot>
		<tr>
			<th colspan="2" class="head1"><center>Total</center></th>
			<th class="head1">{{{ $data['activation'] + $data['modification']}}} </th>
		</tr>
	</tfoot>	
	</tbody>
</table>
<br>
<input type="button" class="stdbtn" value="Kembali" onclick="window.history.back()">
@stop
