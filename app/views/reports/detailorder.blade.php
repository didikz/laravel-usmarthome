@extends('layouts.master')

@section('content')
<div class="contenttitle2"><h2>Detail Data Order</h2></div>
<form class="stdform">
	<label>Order ID:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['TransId'] }}}" readonly /></span>

   <label>Transaksi</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['TransType'] }}}" readonly /></span>

   <label>No Speedy:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['AccountID'] }}}" readonly /></span>

   <label>Paket:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $paket }}}" readonly /></span>

   <label>Customer ID:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['Cust_id'] }}}" readonly /></span>

   <label>Promo ID:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['PromoID'] }}}" readonly /></span>

   <label>Nama Pelanggan:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['CustomerName'] }}}" readonly /></span>

   <label>Alamat:</label>
   <span class="field"><input type="text" class="largeinput" value="{{{ $fab['Address'] }}}" readonly /></span>

   <label>Developer ID:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['DeveloperID'] }}}" readonly /></span>

   <label>Perusahaan Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['DeveloperCompany'] }}}" readonly /></span>

   <label>Nama Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['DeveloperName'] }}}" readonly /></span>

   <label>Telepon Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['DeveloperTelp'] }}}" readonly /></span>

   <label>Email Developer:</label>
   <span class="field"><input type="text" class="smallinput" value="{{{ $fab['DeveloperEmail'] }}}" readonly /></span>
</form>
@stop
