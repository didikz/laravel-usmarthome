@extends('layouts.master')

@section('content')
<?php 
$totalActivation=0; $totalModification=0; $totalSuspension=0; $totalResumption=0; $totalTermination=0; $totalDaily=0; 
$i   = 0;
$ii  = 0;
$iii = 0;
$iv  = 0;
$v   = 0;
$vi  = 0;
?>
<div class="contenttitle2"><h2>Rekap Order Bulan {{{ $monthName }}} {{{ $year }}}</h2></div>
<div class="overviewhead">
  <form class="stdform" method="get" action="{{ URL::to('report/filterbulanan')}}">
    <select name="month">
      <option value="0">- Pilih bulan -</option>
      <option value="01">Januari</option>
      <option value="02">Februari</option>
      <option value="03">Maret</option>
      <option value="04">April</option>
      <option value="05">Mei</option>
      <option value="06">Juni</option>
      <option value="07">Juli</option>
      <option value="08">Agustus</option>
      <option value="09">September</option>
      <option value="10">Oktober</option>
      <option value="11">November</option>
      <option value="12">Desember</option>
    </select>
    <select name="year">
      <option value="0">- Pilih tahun -</option>
      @foreach ($dynYear as $dyn)
        <option value="{{{ $dyn }}}">{{{ $dyn }}}</option>
      @endforeach
    </select>
    <input type="submit" value="Filter" > 
  </form>
</div><br>
<div class="overviewhead">
  <form method="get" action="{{ URL::to('report/filterbyrange') }}">
    Dari: 
    <select name="dayFrom">
      @for ($from=1; $from <= 31; $from++)
      <option value="{{{ $from }}}">{{{ $from }}}</option>
      @endfor
    </select>
    <select name="monthFrom">
      <option value="01">Januari</option>
      <option value="02">Februari</option>
      <option value="03">Maret</option>
      <option value="04">April</option>
      <option value="05">Mei</option>
      <option value="06">Juni</option>
      <option value="07">Juli</option>
      <option value="08">Agustus</option>
      <option value="09">September</option>
      <option value="10">Oktober</option>
      <option value="11">November</option>
      <option value="12">Desember</option>
    </select>
    <select name="yearFrom">
      @foreach ($dynYear as $dyn)
        <option value="{{{ $dyn }}}">{{{ $dyn }}}</option>
      @endforeach
    </select>
    Sampai:
    <select name="dayTo">
      @for ($to=1; $to <= 31; $to++)
      <option value="{{{ $to }}}">{{{ $to }}}</option>
      @endfor
    </select>
    <select name="monthTo">
      <option value="01">Januari</option>
      <option value="02">Februari</option>
      <option value="03">Maret</option>
      <option value="04">April</option>
      <option value="05">Mei</option>
      <option value="06">Juni</option>
      <option value="07">Juli</option>
      <option value="08">Agustus</option>
      <option value="09">September</option>
      <option value="10">Oktober</option>
      <option value="11">November</option>
      <option value="12">Desember</option>
    </select>
    <select name="yearTo">
      @foreach ($dynYear as $dyn)
        <option value="{{{ $dyn }}}">{{{ $dyn }}}</option>
      @endforeach
    </select>
    <input type="submit" value="Filter">
  </form>
</div>  

<br>
<table cellpadding='0' cellspacing='0' border='0' width="80%" class='stdtable' style='font-size:10px; padding:0;'>
    <thead>
    <tr>
    	<th class='head1'>Order</th>
    	@for($hari=1; $hari<=$days; $hari++)
    	<th class='head1'>{{{ $hari }}}</th>
    	@endfor
    	<th class='head1'>Total</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    	<td>ACTIVATION</td>
    	@foreach($activation as $act)
	      	<?php $totalActivation += $act; ?>
      		<td><a href="{{ URL::to('report/bulanan/activation/'.$year.'-'.$month.'-'.($i+1)) }}">{{{ $act }}}</a> </td>
      		<?php $i++; ?>
      	@endforeach
      	<?php ?>
      	<td><a href="{{ URL::to('report/bulanan/activation/total/'.$year.'-'.$month) }}">{{{ $totalActivation }}}</a></td>
    </tr>
    <tr>
    	<td>MODIFICATION</td>
    	@foreach($modification as $mc)
	      	<?php $totalModification += $mc ?>
      		<td><a href="{{ URL::to('report/bulanan/modification/'.$year.'-'.$month.'-'.($ii+1)) }}">{{{ $mc }}}</a> </td>
      		<?php $ii++; ?>
      	@endforeach
      	<td><a href="{{ URL::to('report/bulanan/modification/total/'.$year.'-'.$month) }}">{{{ $totalModification }}}</a></td>
    </tr>
    <tr>
    	<td>SUSPENSION</td>
    	@foreach($suspension as $susp)
	      	<?php $totalSuspension += $susp ?>
      		<td><a href="{{ URL::to('report/bulanan/suspension/'.$year.'-'.$month.'-'.($iii+1)) }}">{{{ $susp }}}</a> </td>
      		<?php $iii++; ?>
      	@endforeach
      	<td><a href="{{ URL::to('report/bulanan/suspension/total/'.$year.'-'.$month) }}">{{{ $totalSuspension }}}</a></td>
    </tr>
    <tr>
    	<td>RESUMPTION</td>
    	@foreach($resumption as $res)
	      	<?php $totalResumption += $res ?>
      		<td><a href="{{ URL::to('report/bulanan/resumption/'.$year.'-'.$month.'-'.($iv+1)) }}">{{{ $res }}}</a> </td>
      		<?php $iv++; ?>
      	@endforeach
      	<td><a href="{{ URL::to('report/bulanan/resumption/total/'.$year.'-'.$month) }}">{{{ $totalResumption }}}</a></td>
    </tr>
    <tr>
    	<td>TERMINATION</td>
    	@foreach($termination as $term)
	      	<?php $totalTermination += $term ?>
      		<td><a href="{{ URL::to('report/bulanan/termination/'.$year.'-'.$month.'-'.($v+1)) }}">{{{ $term }}}</a> </td>
      		<?php $v++; ?>
      	@endforeach
      	<td><a href="{{ URL::to('report/bulanan/termination/total/'.$year.'-'.$month) }}">{{{ $totalTermination }}}</a></td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
    	<th class='head1'>TOTAL</th>
    	@foreach($daily as $dailyCount)
	      	<?php $totalDaily += $dailyCount; ?>
      		<th class='head1'><a href="{{ URL::to('report/bulanan/daily/'.$year.'-'.date('m').'-'.($vi+1)) }}">{{{ $dailyCount }}}</a></th>
      		<?php $vi++; ?>	
      	@endforeach
      	<th class='head1'><a href="{{ URL::to('report/bulanan/'.$year.'-'.$month) }}">{{{ $totalDaily }}}</a></th>
    </tr>
    </tfoot>	
    
</table>
<br>
<form method="get" action="{{{ URL::to(Request::path()) }}}">
  <input type="hidden" name="month" value="{{{ $month }}}" />
  <input type="hidden" name="year" value="{{{ $year }}}" />
  <input type="submit" name="download" value="Download as xls" /> 
</form>
@stop
