<?php

class BookingDevices extends Eloquent {

	protected $table = 'booking_device';
	protected $guarded = array();

	public $timestamps = false;
	public static $rules = array();
}
