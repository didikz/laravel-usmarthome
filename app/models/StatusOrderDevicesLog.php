<?php

class StatusOrderDevicesLog extends Eloquent {
	protected $table = 'status_order_device_log';
	protected $primaryKey = 'id_status_log';
	public $timestamps = false;
	protected $guarded = array();

	public static $rules = array();
}
