<?php

class RequestStatusLog extends Eloquent {

	protected $table = 'request_status_log';
	
	public $timestamps = false;

	protected $guarded = array();

	public static $rules = array();
}
