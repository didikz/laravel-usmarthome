<?php

class OrderDevice extends Eloquent {
	
	protected $table      = 'order_device';
	protected $primaryKey = 'invoice';
	public $timestamps    = false;
	
	protected $guarded    = array();
	
	public static $rules  = array();

	public function devices()
	{
		return $this->hasMany('OrderedDevices', 'invoice')->where('edited_status','=','0');
	}
}
