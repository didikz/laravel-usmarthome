<?php

class MasterUsers extends Eloquent {
	protected $table     = 'master_users';
	
	protected $guarded   = array();
	
	public $timestamps   = false;
	public static $rules = array();
}
