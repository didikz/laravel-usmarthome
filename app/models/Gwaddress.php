<?php

class Gwaddress extends Eloquent {
	protected $table      = 'iot_gateways';
	protected $primaryKey = 'gwid';
	public $timestamps    = false;
	
	protected $guarded    = array();
	
	public static $rules  = array();
}
