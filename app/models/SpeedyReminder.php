<?php

class SpeedyReminder extends Eloquent {

	protected $table     = 'speedy_reminder';
	protected $guarded   = array();
	
	public $timestamps   = false;
	public static $rules = array();
}
