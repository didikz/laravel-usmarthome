<?php

class MasterAM extends Eloquent {

	protected $table      = 'master_am';
	protected $primaryKey = 'id_am';
	
	public $timestamps    = false;
	protected $guarded    = array();
	
	public static $rules  = array();

	public function scopeOrderWitelPeriod($query, $witel, $month, $year, $ordertype)
	{
		if($month=='0' && $year=='0') {

			$query->join('order_device', function($join)
									{
										$join->on('order_device.id_am','=', 'master_am.id_am');
									})->where('witel','=',$witel)
									  ->where('OrderType','=',$ordertype);
		
		} elseif($month=='0' && $year!=='0') {
		
			$query->join('order_device', function($join)
									{
										$join->on('order_device.id_am','=', 'master_am.id_am');
									})->where('witel','=',$witel)
									  ->where('OrderType','=',$ordertype)
									  ->where(DB::raw('YEAR(tanggal_order)'),'=', $year);
		
		} else {
		
			$query->join('order_device', function($join)
									{
										$join->on('order_device.id_am','=', 'master_am.id_am');
									})->where('witel','=',$witel)
									  ->where('OrderType','=',$ordertype)
									  ->where(DB::raw('MONTH(tanggal_order)'),'=', $month)
									  ->where(DB::raw('YEAR(tanggal_order)'),'=', $year);
		
		}
		
	}

}
