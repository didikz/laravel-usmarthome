<?php

class SetterActivity extends Eloquent {

	protected $table = 'setter_activity';
	protected $primaryKey = 'id_setter_act';
	protected $guarded = array();

	public $timestamps = false;
	public static $rules = array();
}
