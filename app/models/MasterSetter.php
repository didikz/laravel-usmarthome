<?php

class MasterSetter extends Eloquent {

	protected $table      = 'master_setter';
	protected $primaryKey = 'id_setter';
	
	public $timestamps    = false;
	protected $guarded    = array();
	
	public static $rules  = array();
}
