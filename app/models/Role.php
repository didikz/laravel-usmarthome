<?php

class Role extends Eloquent {
	protected $guarded = array();
	protected $table = 'role';

	public static $rules = array();

	public function scopePrivileges($id)
	{
		return $this->join('privileges', function($join)
					  {
					  	$join->on('id', '=', 'id_role');
					  })->where('id','=',$id);
	}
}
