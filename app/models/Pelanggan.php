<?php

class Pelanggan extends Eloquent {

	protected $table = 'iot_users';
	protected $primaryKey = 'userid';
	
	public $timestamps = false;
	
	protected $guarded = array();
	
	public static $rules = array();
}
