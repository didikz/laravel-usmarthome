<?php

class Privileges extends Eloquent {
	protected $guarded = array();
	protected $table = 'privileges';

	public static $rules = array();

	public function role()
	{
		return $this->belongsTo('Role');
	}
}
