<?php

class MasterDevices extends Eloquent {

	protected $table      = 'master_device';
	protected $primaryKey = 'id_devices';
	public $timestamps    = false;
	
	protected $guarded    = array();
	
	public static $rules  = array();

	public function scopeStatus($query)
	{
		return $query->where('status', '=', 'unhide')
					 ->where('stock', '!=', '0')
					 ->where('id_vendor', '=', '1') // limited by vendor 1 until multivendor feature fixed
					 ->where('active', '=', '1');
	}

	public function devices()
	{
		return $query->hasMany('OrderedDevices', 'id_device');
	}
}
