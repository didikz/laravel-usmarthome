<?php

class OrderedDevices extends Eloquent {

	protected $table = 'ordered_device';
	protected $guarded = array();

	public static $rules = array();

	public function order()
	{
		return $this->belongsTo('OrderDevice');
	}

	public function master()
	{
		return $this->belongsTo('MasterDevices');
	}
}
