<?php

class Fab extends Eloquent {

	protected $table = 'FAB';

	protected $guarded = array();

	public static $rules = array();

	public $timestamps = false;

	public function scopeActivationfab($query)
	{
		return $query->where('Transtype', '=', 'ACTIVATION')
					->orderBy('lastUpdt', 'desc');
	}

	public function scopeModificationfab($query)
	{
		return $query->where('Transtype', '=', 'MODIFICATION')
					->orderBy('lastUpdt', 'desc');
	}

	public function requestStatus()
	{
		return $query->hasMany('RequestStatus', 'id_fab');
	}

		
}
