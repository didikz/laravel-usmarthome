<?php

class Fablog extends Eloquent {

	protected $table = 'FABlog';
	protected $primaryKey = 'id';
	protected $guarded = array();

	public $timestamps = false;
	public static $rules = array();

	public function scopeActivationfab($query)
	{
		return $query->where('Transtype', '=', 'ACTIVATION')
					->where('result', 'LIKE', 'Activation Success%')
					->orderBy('lastUpdt', 'desc');
					// ->get();
	}

	public function scopeModificationfab($query)
	{
		return $query->where('Transtype', '=', 'MODIFICATION')
					->where('result', '=', 'Modification Success')
					->orderBy('lastUpdt', 'desc');
					// ->get();
	}

	public function scopeSearchForOrderDevice($query, $AccountID, $TransType)
	{
		return $query->where('AccountID','=', $AccountID)
					 ->where('TransType','=', $TransType)
					 ->where('status_code','=', '1');
	}

	public function scopeYearlyOrder($query, $type, $month, $year)
	{
		if ($type=='ACTIVATION') {
			$result = 'Activation Success%';
		} elseif($type=='MODIFICATION') {
			$result = 'Modification Success';
		} elseif($type=='SUSPENSION') {
			$result = 'SUSPENSION Berhasil';
		} elseif($type=='RESUMPTION') {
			$result = 'RESUMPTION Berhasil';
		} elseif($type=='TERMINATION') {
			$result = 'TERMINATION Berhasil';
		}

		return $query->where('FABlog.TransType', '=', $type)
					->where('result', 'LIKE', $result)
					->where(DB::raw('MONTH(lastUpdt)'),'=', $month)
					->where(DB::raw('YEAR(lastUpdt)'),'=', $year);
	}
	
	public function scopeYearlyOrderByMonth($query, $month, $year)
	{
		return $query->where(DB::raw('MONTH(lastUpdt)'),'=', $month)
					->where(DB::raw('YEAR(lastUpdt)'),'=', $year)
					->where(function($query)
					  {
					  	$query->where('result','LIKE','Activation Success%')
					  		  ->orWhere('result', '=', 'TERMINATION Berhasil')
							  ->orWhere('result', '=', 'RESUMPTION Berhasil')
							  ->orWhere('result', '=', 'SUSPENSION Berhasil')
							  ->orWhere('result', '=', 'Modification Success');
					  });
					
	}

	public function scopeYearlyOrderTotalByType($query, $type, $year)
	{
		if ($type=='ACTIVATION') {
			$result = 'Activation Success';
		} elseif($type=='MODIFICATION') {
			$result = 'Modification Success';
		} elseif($type=='SUSPENSION') {
			$result = 'SUSPENSION Berhasil';
		} elseif($type=='RESUMPTION') {
			$result = 'RESUMPTION Berhasil';
		} elseif($type=='TERMINATION') {
			$result = 'TERMINATION Berhasil';
		}

		return $query->where('FABlog.TransType', '=', $type)
					->Where('result', 'LIKE', $result.'%')
					->where(DB::raw('YEAR(lastUpdt)'),'=', $year);
					
	}

	public function scopeYearlyTotalAll($query, $year)
	{
		return $query->where(DB::raw('YEAR(lastUpdt)'),'=', $year)
					->where(function($query) {
						$query->where('result','=','Activation Success')
							  ->orWhere('result','=','Activation Success and has been terminated')
							  ->orWhere('result','=','Modification Success')
							  ->orWhere('result', '=', 'TERMINATION Berhasil')
							  ->orWhere('result', '=', 'RESUMPTION Berhasil')
							  ->orWhere('result', '=', 'SUSPENSION Berhasil');
					});
					
	}

	public function scopeMonthlyOrder($query, $type, $day, $month, $year)
	{
		if ($type=='ACTIVATION') {
			$result = 'Activation Success';
		} elseif($type=='MODIFICATION') {
			$result = 'Modification Success';
		} elseif($type=='SUSPENSION') {
			$result = 'SUSPENSION Berhasil';
		} elseif($type=='RESUMPTION') {
			$result = 'RESUMPTION Berhasil';
		} elseif($type=='TERMINATION') {
			$result = 'TERMINATION Berhasil';
		}

		return $query->where('FABlog.TransType','=',$type)
					->where('result','LIKE', $result.'%')
					->where(DB::raw('DAY(lastUpdt)'),'=', $day)
					->where(DB::raw('MONTH(lastUpdt)'),'=', $month)
					->where(DB::raw('YEAR(lastUpdt)'),'=', $year);
	}



	public function scopeMonthlyOrderByDay($query, $day, $month, $year)
	{
		return $query->where(DB::raw('YEAR(lastUpdt)'),'=', $year)
					 ->where(DB::raw('MONTH(lastUpdt)'),'=', $month)
					 ->where(DB::raw('DAY(lastUpdt)'),'=', $day)
					 ->where(function($query) {
						$query->where('result','=','Activation Success')
							  ->orWhere('result','=','Modification Success')
							  ->orWhere('result', '=', 'TERMINATION Berhasil')
							  ->orWhere('result', '=', 'RESUMPTION Berhasil')
							  ->orWhere('result', '=', 'SUSPENSION Berhasil');
					 });
					
	}

	public function scopeMonthlyOrderTotalByType($query, $type, $month, $year)
	{
		if ($type=='ACTIVATION') {
			$result = 'Activation Success';
		} elseif($type=='MODIFICATION') {
			$result = 'Modification Success';
		} elseif($type=='SUSPENSION') {
			$result = 'SUSPENSION Berhasil';
		} elseif($type=='RESUMPTION') {
			$result = 'RESUMPTION Berhasil';
		} elseif($type=='TERMINATION') {
			$result = 'TERMINATION Berhasil';
		}

		return $query->where('FABlog.TransType', '=', $type)
					->Where('result', 'LIKE', $result.'%')
					->where(DB::raw('YEAR(lastUpdt)'),'=', $year)
					->where(DB::raw('MONTH(lastUpdt)'),'=', $month);
					
	}

	public function scopeSearchbyrange($query, $type, $dateFrom, $dateTo)
	{
		if ($type=='ACTIVATION') {
			$result = 'Activation Success';
		} elseif($type=='MODIFICATION') {
			$result = 'Modification Success';
		} elseif($type=='SUSPENSION') {
			$result = 'SUSPENSION Berhasil';
		} elseif($type=='RESUMPTION') {
			$result = 'RESUMPTION Berhasil';
		} elseif($type=='TERMINATION') {
			$result = 'TERMINATION Berhasil';
		}

		return $query->whereBetween('lastUpdt', array($dateFrom, $dateTo))
					->where('TransType','=', $type)
					->where('result','LIKE',$result.'%');
	}

	public function scopeReportPaketYearly($query, $PackageID, $month, $year)
	{
		if ($PackageID >= '9') {
			//do something
		}

		return $query->where(function($query)
							{
								$query->where('TransType','=', 'ACTIVATION')
									  ->orWhere('TransType','=','MODIFICATION');
							})
					 
					 ->where(function($query)
							{
								$query->where('result','LIKE', 'Activation Success%')
									  ->orWhere('result','=','Modification Success');
							})

					 ->where(DB::raw('YEAR(lastUpdt)'),'=', $year)
					 ->where(DB::raw('MONTH(lastUpdt)'),'=', $month)
					 ->where('PackageID','=',$PackageID);
	}

	public function scopeReportPaketYearlyAll($query, $PackageID, $year)
	{
		if ($PackageID >= '9') {
			//do something
		}

		return $query->where(function($query)
							{
								$query->where('TransType','=', 'ACTIVATION')
									  ->orWhere('TransType','=','MODIFICATION');
							})
					 
					 ->where(function($query)
							{
								$query->where('result','LIKE', 'Activation Success%')
									  ->orWhere('result','=','Modification Success');
							})

					 ->where(DB::raw('YEAR(lastUpdt)'),'=', $year)
					 ->where('PackageID','=',$PackageID);
	}

	public function scopeReportPaketMonthly($query, $PackageID, $day, $month, $year)
	{

		if ($PackageID >= '9') {
			//do something
		}

		return $query->where(function($query)
							{
								$query->where('TransType','=', 'ACTIVATION')
									  ->orWhere('TransType','=','MODIFICATION');
							})
					 
					 ->where(function($query)
							{
								$query->where('result','LIKE', 'Activation Success%')
									  ->orWhere('result','=','Modification Success');
							})

					 ->where(DB::raw('YEAR(lastUpdt)'),'=', $year)
					 ->where(DB::raw('MONTH(lastUpdt)'),'=', $month)
					 ->where(DB::raw('DAY(lastUpdt)'),'=', $day)
					 ->where('PackageID','=',$PackageID);

	}

}
