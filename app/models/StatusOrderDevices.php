<?php

class StatusOrderDevices extends Eloquent {

	protected $table      = 'status_order_device';
	protected $primaryKey = 'id_status';
	public $timestamps    = false;
	protected $guarded    = array();
	
	public static $rules  = array();
}
