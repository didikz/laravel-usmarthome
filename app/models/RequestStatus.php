<?php

class RequestStatus extends Eloquent {

	protected $table = 'request_status';

	protected $primaryKey = 'id_req_status';

	protected $guarded = array();

	public static $rules = array();

	public $timestamps = false;

	public function fab()
	{
		return $this->belongsTo('Fab');
	}
}
