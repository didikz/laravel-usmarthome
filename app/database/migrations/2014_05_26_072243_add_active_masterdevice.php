<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddActiveMasterdevice extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('master_device', function(Blueprint $table) 
		{
			$table->enum('active', array('0','1'))->default('1');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('master_device', function(Blueprint $table) 
		{
			$table->dropColumn('active');
		});
	}

}