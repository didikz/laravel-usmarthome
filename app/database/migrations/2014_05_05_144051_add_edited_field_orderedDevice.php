<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddEditedFieldOrderedDevice extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ordered_device', function(Blueprint $table) {
			$table->enum('edited_status', array('0','1'))->default('0');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ordered_device', function(Blueprint $table) {
			$table->dropColumn('edited_status');
		});
	}

}