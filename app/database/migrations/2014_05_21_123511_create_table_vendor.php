<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVendor extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('master_vendor', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('vendor_name');
			$table->string('vendor_location')->nullable();
			$table->string('vendor_address');
			$table->string('vendor_phone');
			$table->string('vendor_email');
			$table->enum('active', array('1','0'))->default('1');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('master_vendor');
	}

}
