<?php

use Illuminate\Database\Migrations\Migration;

class InsertModuleData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('module')->insert(array('module_name'=>'management users','module_route'=>'users'));
		DB::table('module')->insert(array('module_name'=>'users','module_route'=>'users'));
		DB::table('module')->insert(array('module_name'=>'privilege','module_route'=>'users/privilege'));
		DB::table('module')->insert(array('module_name'=>'management order','module_route'=>'ordermanagement'));
		DB::table('module')->insert(array('module_name'=>'order aktivasi','module_route'=>'ordermanagement/aktivasi'));
		DB::table('module')->insert(array('module_name'=>'order modifikasi','module_route'=>'ordermanagement/modifikasi'));
		DB::table('module')->insert(array('module_name'=>'order device','module_route'=>'orderdevices'));
		DB::table('module')->insert(array('module_name'=>'management device','module_route'=>'managementdevices'));
		DB::table('module')->insert(array('module_name'=>'management AM','module_route'=>'am'));
		DB::table('module')->insert(array('module_name'=>'data pelanggan','module_route'=>'pelanggan'));
		DB::table('module')->insert(array('module_name'=>'aktif','module_route'=>'pelanggan/aktif'));
		DB::table('module')->insert(array('module_name'=>'nonaktif','module_route'=>'pelanggan/nonaktif'));
		DB::table('module')->insert(array('module_name'=>'data perangkat','module_route'=>'perangkat'));
		DB::table('module')->insert(array('module_name'=>'history setter','module_route'=>'setter/history'));
		DB::table('module')->insert(array('module_name'=>'rekap order','module_route'=>'report'));
		DB::table('module')->insert(array('module_name'=>'tahunan','module_route'=>'report/tahunan'));
		DB::table('module')->insert(array('module_name'=>'bulanan','module_route'=>'report/bulanan'));
		DB::table('module')->insert(array('module_name'=>'witel','module_route'=>'report/witel'));
		DB::table('module')->insert(array('module_name'=>'rekap paket','module_route'=>'rekappaket'));
		DB::table('module')->insert(array('module_name'=>'tahunan','module_route'=>'reportpaket/tahunan'));
		DB::table('module')->insert(array('module_name'=>'bulanan','module_route'=>'reportpaket/bulanan'));
		DB::table('module')->insert(array('module_name'=>'inquiry order','module_route'=>'inquiry'));
		DB::table('module')->insert(array('module_name'=>'Management Setter','module_route'=>'setter'));
		DB::table('module')->insert(array('module_name'=>'booking order','module_route'=>'bookingorder'));
		DB::table('module')->insert(array('module_name'=>'mapping device','module_route'=>'mapping'));
		DB::table('module')->insert(array('module_name'=>'binding device','module_route'=>'binding'));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('module')->truncate();
	}

}