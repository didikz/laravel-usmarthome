<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddOrdereddevicePrice extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ordered_device', function(Blueprint $table) 
		{
			$table->integer('ordered_price');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ordered_device', function(Blueprint $table) 
		{
			$table->dropColumn('ordered_price');
		});
	}

}