<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BookingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_order', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_am');
			$table->string('AccountID', 20)->nullable();
			$table->string('customer_name', 100);
			$table->boolean('status')->default(0);
			$table->timestamps();
			$table->index('id_am');
		});

		// Schema::create('booking_device', function(Blueprint $table)
		// {
		// 	$table->increments('id');
		// 	$table->integer('id_order');
		// 	$table->integer('id_device');
		// 	$table->integer('qty');
		// 	$table->index('id_order');
		// 	$table->index('id_device');
		// });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_order');
		// Schema::drop('booking_device');
	}

}