<?php

use Illuminate\Database\Migrations\Migration;

class InsertRoleData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('role')->insert(array('role_name'=>'admin'));
		DB::table('role')->insert(array('role_name'=>'mitra'));
		DB::table('role')->insert(array('role_name'=>'setter'));
		DB::table('role')->insert(array('role_name'=>'CO AM'));
		DB::table('role')->insert(array('role_name'=>'AM'));
		DB::table('role')->insert(array('role_name'=>'ESA DSC'));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('role')->truncate();
	}

}