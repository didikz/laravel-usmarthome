<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivilegesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('privileges', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('id_role')->unsigned();
			$table->integer('id_module')->unsigned();
			$table->timestamps();
			$table->index('id_role');
			$table->index('id_module');
		});

		// Schema::table('privileges', function(Blueprint $table)
		// {
		// 	$table->foreign('id_role')->references('id')->on('role')->onDelete('cascade');
		// 	$table->foreign('id_module')->references('id')->on('module')->onDelete('cascade');
		// });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('privileges');
	}

}
