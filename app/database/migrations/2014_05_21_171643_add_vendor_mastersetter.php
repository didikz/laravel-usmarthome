<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddVendorMastersetter extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('master_setter', function(Blueprint $table) 
		{
			$table->integer('id_vendor');
			$table->index('id_vendor');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('master_setter', function(Blueprint $table) 
		{
			$table->dropColumn('id_vendor');
		});
	}

}