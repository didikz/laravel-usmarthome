<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddOrdertypeOrderdevice extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_device', function(Blueprint $table) {
			$table->enum('OrderType', array('activation','modification','alacarte'))->default('activation');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_device', function(Blueprint $table) {
			$table->dropColumn('OrderType');
		});
	}

}
