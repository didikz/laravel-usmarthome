<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BookingDevice extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_device', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_order');
			$table->integer('id_device');
			$table->integer('qty');
			$table->index('id_order');
			$table->index('id_device');
			// $table->foreign('id_order')->references('id')->on('booking_order')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_device');
	}

}