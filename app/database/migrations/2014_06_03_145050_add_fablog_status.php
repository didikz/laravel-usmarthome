<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFablogStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		/**
		 *  status code
		 *  0 = failed result order
		 *  1 = success result order
		 *  2 = order has been terminated
		 */
		Schema::table('FABlog', function(Blueprint $table) 
		{
			$table->enum('status_code', array('0','1','2'))->default('1');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('FABlog', function(Blueprint $table) 
		{
			$table->dropColumn('status_code');
		});
	}

}