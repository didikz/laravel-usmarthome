<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddTranstypeBookingOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('booking_order', function(Blueprint $table) {
			$table->enum('OrderType', array('activation','modification','alacarte'))->default('activation');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('booking_order', function(Blueprint $table) {
			$table->dropColumn('OrderType');
		});
	}

}