<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddOrderdeviceCustomertype extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_device', function(Blueprint $table) 
		{
			$table->enum('cust_type', array('dinas','pelanggan'))->default('pelanggan');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_device', function(Blueprint $table) 
		{
			$table->dropColumn('cust_type');
		});
	}

}