<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderInquiryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inquiry_order', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_device');
			$table->integer('qty');
			$table->integer('restock');
			$table->integer('id_am');
			$table->string('keterangan')->nullable();
			 // 0= belum diproses, 1=sudah diproses, 2=cancel sebelum restock, 3= close inquiry dan sudah diproses
			$table->enum('inquiry_status', array('0','1','2','3'))->default('0');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inquiry_order');
	}

}