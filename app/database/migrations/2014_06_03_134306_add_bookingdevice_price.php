<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddBookingdevicePrice extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('booking_device', function(Blueprint $table) 
		{
			$table->integer('harga_booking');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('booking_device', function(Blueprint $table) 
		{
			$table->dropColumn('harga_booking');
		});
	}

}