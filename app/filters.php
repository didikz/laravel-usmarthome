<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('/')->with('message','you must login first');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

/**
 *  call module by privileges and display to master layout
 */
View::composer('layouts.master', function($view)
{
    
    $menu = Privileges::join('module', function($join)
    							{
    							
                                	$join->on('id_module', '=','module.id');
    							
                                })->where('id_role', '=', Session::get('roleid'))
    							  ->get();
    
    $arrParent = array();
    $arrChild  = array();
    $arrUsual  = array();
    $init      = 0;
    $init2     = 0;
    
    foreach ($menu as $item) {

    	if ($item->module_parent == '0') { //parent
    		
    		$arrParent[$init]['parentName'] = $item->module_name;
    		$arrParent[$init]['parentRoute'] = $item->module_route;

    		$submenu = Privileges::join('module', function($join)
    							{
    							
                                	$join->on('id_module', '=','module.id');
    							
                                })->where('module_parent', '=', $item->id)
                                  ->where('id_role', '=', Session::get('roleid'))
    							  ->get();	
    		
    		$init3 = 0;
    		
    		foreach ($submenu as $item2) {
    		
    			
    			$arrParent[$init]['child'][$init3]['childName'] = $item2->module_name;
    			$arrParent[$init]['child'][$init3]['childRoute'] = $item2->module_route; 
    			$init3++;
    		
            }

    		$init++;
    		
    	} elseif($item->module_parent == '') {
    		
            if ($item->module_name== 'management device' && Session::get('roleid')=='5') {
                
                $arrUsual[$init2]['parentName'] = 'Info Device';
            
            } else {
            
                $arrUsual[$init2]['parentName'] = $item->module_name;
            
            }
    		
            $arrUsual[$init2]['parentRoute'] = $item->module_route;
    		$init2++;
    		
    	}
    	
    }
    
    $view->with('nestedMenu', $arrParent)->with('usualMenu', $arrUsual);

});