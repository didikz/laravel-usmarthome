<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@showLogin');

Route::post('login', 'HomeController@postlogin');

Route::group(array('before'=>'auth'), function()
{

	Route::get('dashboard', 'DashboardController@index');

	Route::get('signout', function()
	{
		
		Auth::logout();
		Session::flush();
		
		return Redirect::to('/');
	});

});


// ---------------------------- Start of Order Management Modul Route -------------------------------------------//

Route::group(array('before'=>'auth', 'prefix'=>'ordermanagement'), function()
{

	Route::get('{order}', 'OrderManagementController@index');
	Route::get('{type}/detail/{fabid}', 'OrderManagementController@show');
	Route::get('{type}/status/{idReqStatus}', 'OrderManagementController@createStatus');
	Route::post('{type}/status/store','OrderManagementController@storeStatus');
	Route::get('{type}/searchspeedy','OrderManagementController@searchNonAlacarte');
	Route::get('modifikasi/alacarte','OrderManagementController@searchAlacarte');

});

// ---------------------------- End of Order Management Modul Route -------------------------------------------//

// ---------------------------- Start of Booking Modul Route -------------------------------------------//

Route::group(array('before'=>'auth', 'prefix'=>'bookingorder'), function()
{

	Route::get('', 'BookingController@index');
	Route::get('new', 'BookingController@create');
	Route::post('confirm', 'BookingController@confirm');
	Route::post('save', 'BookingController@store');
	Route::get('edit/{id}', 'BookingController@edit');
	Route::post('update/{id}', 'BookingController@update');
	Route::get('delete/{id}', 'BookingController@destroy');
	Route::get('detail/{id}', 'BookingController@show');

});


// ---------------------------- End of booking Modul Route -------------------------------------------//

// ---------------------------- Start of User Modul Route -------------------------------------------//

Route::group(array('before'=>'auth','prefix'=>'users'), function()
{

	Route::get('', 'UserManagementController@index');
	Route::get('new', 'UserManagementController@create');
	Route::post('save', 'UserManagementController@store');
	Route::get('edit/{id}', 'UserManagementController@edit');
	Route::post('update/{id}', 'UserManagementController@update');
	Route::get('delete/{id}', 'UserManagementController@destroy');
	Route::group(array('prefix'=>'privilege'), function()
	{
		Route::get('', 'UserManagementController@privilege');
		Route::get('set/{id}', 'UserManagementController@setPrivilege');
		Route::post('update/{id}', 'UserManagementController@updatePrivilege');
	});
	

});

// ---------------------------- End of User Modul Route -------------------------------------------//

// ---------------------------- Start of vendor management module Route ----------------------------//

Route::group(array('before'=>'auth', 'prefix'=>'managementmitra'), function()
{
	Route::get('', 'VendorManagementController@index');
	Route::get('new', 'VendorManagementController@create');
	Route::get('edit/{id}', 'VendorManagementController@edit');
	Route::get('delete/{id}', 'VendorManagementController@destroy');
	Route::get('detail/{id}', 'VendorManagementController@show');
	Route::get('users/{id}', 'VendorManagementController@userCreate');
	Route::post('save', 'VendorManagementController@store');
	Route::post('update/{id}', 'VendorManagementController@update');
	Route::post('users/save/', 'VendorManagementController@userStore');
});

// ---------------------------- Start of vendor management module Route ----------------------------//

// ---------------------------- Start of AM Management Modul Route -------------------------------------------//

Route::group(array('before'=>'auth', 'prefix'=>'am'), function()
{

	// index and show data all AM
	Route::get('', 'AmManagementController@index');
	// create new AM
	Route::get('new', 'AmManagementController@create');
	// action insert data AM
	Route::post('save', 'AmManagementController@store');
	// action delete data AM
	Route::get('delete/{id}', 'AmManagementController@destroy');
	// action edit data AM
	Route::get('edit/{id}', 'AmManagementController@edit');
	// action update data AM
	Route::post('update/{id}', 'AmManagementController@update');
	// show detail data AM
	Route::get('detail/{id}', 'AmManagementController@show');

});

// ---------------------------- End of AM Management Modul Route -------------------------------------------//

// ---------------------------- Start of Setter Management Modul Route -------------------------------------------//

Route::group(array('before'=>'auth', 'prefix'=>'setter'), function()
{
	// index and show data all Setter
	Route::get('', 'SetterManagementController@index');
	// show data form new data Setter
	Route::get('new', 'SetterManagementController@create');
	// store new data Setter
	Route::post('save', 'SetterManagementController@store');
	// show data form edit data Setter
	Route::get('edit/{id}', 'SetterManagementController@edit');
	// show data form edit data Setter
	Route::post('update/{id}', 'SetterManagementController@update');
	// delete data Setter
	Route::get('delete/{id}', 'SetterManagementController@destroy');
	// show detail data Setter
	Route::get('detail/{id}', 'SetterManagementController@show');
	// show history setter
	Route::get('history', 'SetterManagementController@history');
	// show detail history setter
	Route::get('history/{id}', 'SetterManagementController@showHistory');

});

// ---------------------------- End of Setter Management Modul Route -------------------------------------------//

// ---------------------------- Start of Devices Management Modul Route -------------------------------------------//

Route::group(array('before'=>'auth', 'prefix'=>'managementdevices'), function()
{
	// index and show data all master devices
	Route::get('', 'DevicesManagementController@index');
	// show form new master devices
	Route::get('new', 'DevicesManagementController@create');
	// store data new master devices
	Route::post('save', 'DevicesManagementController@store');
	// show form edit master devices
	Route::get('edit/{id}', 'DevicesManagementController@edit');
	// store update data new master devices
	Route::post('update/{id}', 'DevicesManagementController@update');
	// show detail master devices
	Route::get('detail/{id}', 'DevicesManagementController@show');
	// show detail master devices
	Route::get('delete/{id}', 'DevicesManagementController@destroy');
	
});

// ---------------------------- End of Devices Management Modul Route -------------------------------------------//

// ---------------------------- Start of Inquiry order modul route ---------------------------------------------- //

// Group of inquiry device
Route::group(array('before'=>'auth','prefix'=>'inquiry'), function()
{
	Route::get('', 'InquiryDeviceController@index');
	// show new inquiry device form
	Route::get('new/{id}', 'InquiryDeviceController@create');
	// show new inquiry device form
	Route::get('new', 'InquiryDeviceController@createNoParam');
	// store data inquiry device
	Route::post('save', 'InquiryDeviceController@store');
	// show detail inquiry device
	Route::get('detailall/{id}', 'InquiryDeviceController@showDetailAll');
	// show detail inquiry device form
	Route::get('detail/{id}', 'InquiryDeviceController@showDetail');
	// show restock form
	Route::get('restock/{id}', 'InquiryDeviceController@restock');
	// store data inquiry device
	Route::post('update/{id}', 'InquiryDeviceController@update');
	// show restock all form
	Route::get('restockall/{id}', 'InquiryDeviceController@restockAll');
	// store data restock all
	Route::post('restockall/update/{id}', 'InquiryDeviceController@updateRestockAll');
});

// ----------------------------- End of inquiry order modul route ---------------------------------------------//

// ---------------------------- Start of order Devices Modul Route -------------------------------------------//

Route::group(array('before'=>'auth', 'prefix'=>'orderdevices'), function()
{
	// index and show data all order devices
	Route::get('', 'OrderDevicesController@index');
	// show new order device form
	Route::get('new/{accountid}/{type}', 'OrderDevicesController@create');
	// show new order device form alacarte
	Route::get('alacarte/new/{accountid}', 'OrderDevicesController@createAlacarte');
	// confirm process order device form
	Route::post('confirm', 'OrderDevicesController@confirm');
	// confirm process order device form
	Route::post('alacarteconfirm', 'OrderDevicesController@alacarteConfirm');
	// save data order device form
	Route::post('save', 'OrderDevicesController@store');
	// save data order device form
	Route::post('alacartesave', 'OrderDevicesController@storeAlacarte');
	// show new order device form
	Route::get('detail/{id}', 'OrderDevicesController@show');
	// show change status order device form
	Route::get('response/{id}', 'OrderDevicesController@response');
	// store change status order device form
	Route::post('response/save', 'OrderDevicesController@updateResponse');
	// show edit device form
	Route::get('edit/{id}', 'OrderDevicesController@edit');
	// update order device
	Route::post('update/{id}', 'OrderDevicesController@update');
	// show log ordered device
	Route::get('detail/log/{id}', 'OrderDevicesController@showLog');
	// show struk order device
	Route::get('detail/struk/{invoice}', 'OrderDevicesController@showStruk');

});

// ---------------------------- End of order Devices Modul Route -------------------------------------------//

// ---------------------------- Start of Data Pelanggan Modul Route -------------------------------------------//

Route::group(array('before'=>'auth', 'prefix'=>'pelanggan'), function()
{
	// index and show data all pelanggan
	Route::get('{any}', 'PelangganController@index');
	// show detail data all pelanggan
	Route::get('detail/{accountid}', 'PelangganController@show');
});

// ---------------------------- End of Data Pelanggan Modul Route -------------------------------------------//

// ---------------------------- Start of Data perangkat Modul Route -------------------------------------------//

Route::group(array('before'=>'auth', 'prefix'=>'perangkat'), function()
{

	Route::get('','PerangkatController@index');
	Route::get('detail/{invoice}','PerangkatController@show');

});

// ---------------------------- End of Data perangkat Modul Route -------------------------------------------//

// ---------------------------- Start of Binding perangkat Modul Route -------------------------------------------//

Route::group(array('before'=>'auth', 'prefix'=>'binding'), function()
{
	
	Route::get('','BindingController@index');
	Route::get('detail/{speedy}/{rmid}','BindingController@show');

});

// ---------------------------- End of Binding perangat Modul Route -------------------------------------------//

// ---------------------------- Start of mapping device Modul Route -------------------------------------------//

Route::group(array('before'=>'auth', 'prefix'=>'mapping'), function()
{

	// show index list order for mapping preparation 
	Route::get('', 'MappingController@index');
	// show mapping form
	Route::get('new/{fabid}/{reqid}', 'MappingController@create');
	//store data mapping form
	Route::post('save', 'MappingController@store');

});

// ---------------------------- End of mapping device Modul Route -------------------------------------------//

// ---------------------------- start report modul Route ---------------------------------------------------//

Route::group(array('before'=>'auth','prefix'=>'report'), function()
{
	Route::get('tahunan', 'ReportController@indexTahunan');
	Route::get('bulanan', 'ReportController@indexBulanan');
	Route::get('detail/{id}', 'ReportController@detailOrderAll');
	Route::get('filtertahunan', 'ReportController@indexTahunan');
	Route::get('filterbulanan', 'ReportController@indexBulanan');
	Route::get('filterbyrange', 'ReportController@filterByRange');
	Route::group(array('prefix'=>'tahunan'), function()
	{
		Route::get('{type}/{date}', 'ReportController@detailTahunan');
		Route::get('{year}', 'ReportController@totalTahunan');
	});
	Route::group(array('prefix'=>'bulanan'), function()
	{
		Route::get('{type}/{date}', 'ReportController@detailBulanan');
		Route::get('{type}/total/{date}', 'ReportController@detailTotalBulanan');
		Route::get('{periode}', 'ReportController@totalBulanan');
	});
	Route::group(array('prefix'=>'witel'), function()
	{
		Route::get('', 'ReportController@indexWitel');
		Route::get('filter', 'ReportController@filterWitel');
		Route::get('{type}/{witel}/{month}/{year}', 'ReportController@detailFilterWitel');
	});
});

Route::group(array('before'=>'auth','prefix'=>'reportpaket'), function()
{
	Route::get('tahunan', 'ReportPaketController@indexTahunan');
	Route::get('bulanan', 'ReportPaketController@indexBulanan');
	Route::get('filtertahunan', 'ReportPaketController@indexTahunan');
	Route::get('filterbulanan', 'ReportPaketController@indexBulanan');
	Route::get('detail/{id}', 'ReportPaketController@detailPaketTahunan');

	Route::group(array('prefix'=>'tahunan'), function()
	{
		Route::get('show/{package}/{period}', 'ReportPaketController@showTahunan');
		// Route::('showtotal/{year}', 'Repo');
	});

	Route::group(array('prefix'=>'bulanan'), function()
	{
		Route::get('show/{package}/{period}', 'ReportPaketController@showBulanan');
		// Route::('showtotal/{year}', 'Repo');
	});
});
// ---------------------------- start report modul Route ---------------------------------------------------//

// ----------------------------- start modul fulfillment API ----------------------------------------------- //
Route::group(array('prefix'=>'fab'), function()
{
	// Route::get('activation', 'FabAPIController@activation');
	// Route::get('modification', 'FabAPIController@modification');
	// Route::get('suspension', 'FabAPIController@suspension');
	// Route::get('resumption', 'FabAPIController@resumption');
	// Route::get('termination', 'FabAPIController@termination');
	Route::get('cancel', 'FabAPIController@cancellation');
});
// ----------------------------- End modul fulfillment API ----------------------------------------------- //

Route::get('tes', function()
{
	$idvendor = '1';
	$mitra    = UserVendor::join('tblusers', function($join)
						{
						
							$join->on('tblusers.ID','=','id_user');
						
						})->join('master_users', function($join)
						{
						
							$join->on('user_vendor.id_user','=','master_users.id_user');
						
						})->where('user_vendor.id_vendor','=',$idvendor)
						  ->get();

	$emailAddress = array('maroooona@gmail.com','didiktrisusanto@yahoo.com');
	
	foreach ($mitra as $user) {
	
		array_push($emailAddress, $user->email);
	
	}

	$data = array('email' => $emailAddress);
	// send notif order device email to mitra and esa
	$send = Mail::send('emails.tesmail', $data, function($message) use ($data)
		   {

		   		$message->to($data['email'], 'Mitra & ESA DSC')->subject('Open Order Device layanan Home Automation');
		  
		   });

	return $data['email'];
});

Route::get('tesexcel', function()
{
	$excel = Excel::create('myexcel', function($excel) {

		$excel->sheet('new sheet', function($sheet) {
		
			// Set background color for a specific cell
			$sheet->getStyle('A1')->applyFromArray(array(
			    'fill' => array(
			        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
			        'color' => array('rgb' => 'FF0000')
			    )
			));

			$data['title'] = 'coba title';
			$data['row'] = 'coba row';
			$sheet->loadView('dashboard.repex', array('data'=>$data));
		
		});
	
	})->download('xls');

	return 'done';
});