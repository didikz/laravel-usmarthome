<?php

class InquiryDeviceController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		if (Session::get('role')=='AM') {
		
			$inquiry = InquiryOrder::join('master_device', function($join)
									{
		
										$join->on('id_devices','=','id_device');
		
									})->where('inquiry_order.id_am','=', Session::get('idam'))
									  ->select('nama_device','qty','inquiry_order.id_am','inquiry_status','created_at','id_device','id','restock')
									  ->orderBy('updated_at','desc')
									  ->get();

			  return View::make('inquiryorder.index', array('inquiry'=>$inquiry));
		
		} else {
		
			$inquiry = InquiryOrder::join('master_device', function($join)
									{
									
										$join->on('id_devices','=','id_device');
									
									})->join('master_am', function($join)
									{
									
										$join->on('inquiry_order.id_am','=','master_am.id_am');
									
									})
									  ->orderBy('updated_at','desc')
									  ->get();

			$countInquiry = array();
			$master       = MasterDevices::select('id_devices','nama_device', 'stock')->get();
			
			foreach ($master as $item) {
		  		
		  		$count = InquiryOrder::where('id_device','=', $item->id_devices)
		  							 ->select(DB::raw('SUM(qty) as kuantiti'),DB::raw('SUM(restock) as addstock'))
		  							 ->first();
			
				if($count['kuantiti']==NULL || $count['kuantiti']=='0') {
			
					$qty = '0';
			
				} else {
			
					$qty = $count['kuantiti'] - $count['addstock'];
			
				}
			
				array_push($countInquiry, $qty);
			}						  
			
			return View::make('inquiryorder.indexmitra', array('inquiry'=>$inquiry, 'count'=>$countInquiry, 'master'=>$master));
		}
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function create($id)
	{
		
		$device = MasterDevices::select('id_devices','nama_device')
								->where('id_devices','=', $id)
								->first();
       
        return View::make('inquiryorder.create', array('device'=>$device));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function createNoParam()
	{
		
		$device = MasterDevices::select('id_devices','nama_device')
							   ->where('status','=','unhide')
							   ->where('active','=','1')
							   ->where('id_vendor','=','1')
							   ->get();
        
        return View::make('inquiryorder.createnoparam', array('device'=>$device));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$iddevice   = Input::get('iddevice');
		$qty        = Input::get('qty');
		$keterangan = Input::get('keterangan');

		$store = InquiryOrder::create(array(

											'id_device' => $iddevice,
											'qty' => $qty,
											'keterangan' => $keterangan,
											'id_am' => Session::get('idam')
										));

		return Redirect::to('inquiry')->with('message', 'Inquiry order berhasil dilakukan');
	}

	/**
	 * show detail specified resource
	 * @param  int $id
	 * @return response
	 */
	public function showDetail($id)
	{
		$inquiry = InquiryOrder::join('master_device', function($join) {
								
									$join->on('id_devices', '=', 'id_device');
								
								})->where('id_device','=', $id)
								  ->first();

        return View::make('inquiryorder.show', array('inquiry'=>$inquiry));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showDetailAll($id)
	{

		$inquiry = InquiryOrder::join('master_am', function($join)
								{
								
									$join->on('inquiry_order.id_am','=','master_am.id_am');
								
								})->join('master_device', function($join)
								{
								
									$join->on('id_devices','=','id_device');
								
								})->select('nama_device','qty','inquiry_order.id_am','inquiry_status','created_at','id_device','nama_lengkap','id','restock')
								  ->where('id_device','=',$id)
								  ->get();

        return View::make('inquiryorder.showdetaildevice', array('inquiry'=>$inquiry));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id of inquiry order
	 * @return Response
	 */
	public function restock($id)
	{
		$inquiry = InquiryOrder::join('master_device', function($join)
								{
									$join->on('id_device','=','id_devices');
								
								})->join('master_am', function($join)
								{
									$join->on('inquiry_order.id_am','=','master_am.id_am');
								
								})->where('id','=',$id)
								  ->first();

        return View::make('inquiryorder.restock', array('inquiry'=>$inquiry));
	}

	/**
	 * show the form for restock process
	 * @param  int $id from id_device
	 * @return response view
	 */
	public function restockAll($id)
	{
		$inquiry = InquiryOrder::join('master_device', function($join)
								{
								
									$join->on('id_devices','=','id_device');
								
								})->select('nama_device',DB::raw('SUM(qty) as kuantiti'),DB::raw('SUM(restock) as addstock'))
								  ->where('inquiry_status','=','0')
								  ->orWhere('inquiry_status','=','1')
								  ->first();
		
		$data['id'] = $id;
		$data['qty'] = $inquiry['kuantiti'];
		$data['devicename'] = $inquiry['nama_device'];
		
		return View::make('inquiryorder.restockall', array('data'=>$data));
	}

	/**
	 * store restock all data
	 * @param  int $id id device
	 * @return response
	 */
	public function updateRestockAll($id)
	{
		$restock = Input::get('restock');
		$qty = Input::get('qty');

		if ($qty !== $restock) {
			
			return Redirect::to('inquiry/restockall/'.$id)->with('message','Jumlah restock harus sama dengan jumlah inquiry');
		
		} else {

			$inquiry = InquiryOrder::where('id_device','=', $id)
								   ->where('inquiry_status','=','0')
								   ->orWhere('inquiry_status','=','1')
								   ->get();
			
			foreach ($inquiry as $item) {
		   		InquiryOrder::where('id','=',$item->id)->update(array('restock'=>$item->qty, 'inquiry_status'=>'3'));
		    }

		    // update current device stock
			$master        = MasterDevices::find($id);
			$master->stock = $master->stock + $restock;
			$master->save();		   

			return Redirect::to('inquiry')->with('message', 'Proses restock untuk inquiry order berhasil. Stock perangkat '.$master->nama_device.' telah ditambah sebanyak '.$restock);

		}
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$restock        = Input::get('restockqty');
		$setstatus      = 'null';

		// cek restock dg qty inquiry
		$inquiry        = InquiryOrder::find($id);
		$inquiryQty     = $inquiry->qty;
		$inquiryRestock = $inquiry->restock;
		
		if($restock > $inquiryQty) {
		
			return Redirect::to('inquiry/restock/'.$id)->with('message', 'Jumlah restock tidak boleh lebih besar dari jumlah inquiry.');
		
		} elseif($restock == $inquiryQty) { // otomatis closed inquiry

			$updateInquiry = InquiryOrder::where('id','=',$id)->update(array('restock'=>$restock,'inquiry_status'=>'3'));
			
			// update current device stock
			$master        = MasterDevices::find($inquiry->id_device);
			$master->stock = $master->stock + $restock;
			$master->save();

			if ($updateInquiry) {

				$message = 'Proses restock untuk inquiry order berhasil. Stock perangkat '.$master->nama_device.' telah ditambah sebanyak '.$restock;
		
			} else {
		
				$message = 'Gagal menyimpan data';
		
			}

			return Redirect::to('inquiry')->with('message', $message);

		} else {
			
			if($inquiry->restock==0) {
			
				if ($setstatus!=='null') {

					$dataArr = array('restock'=>$restock,'inquiry_status'=>$setstatus);
				
				} else {
					
					$dataArr = array('restock'=>$restock,'inquiry_status'=>'1');
		
				}

				$updateInquiry = InquiryOrder::where('id','=',$id)->update($dataArr);
			
			} else {
				
				$cekRestock = $inquiry->qty - $inquiry->restock; // cek selisih antara inquiry dengan restock lama
					
				if($restock > $cekRestock) {
				
					return Redirect::to('inquiry/restock/'.$id)->with('message', 'Jumlah restock yang dimasukkan telah melebihi jumlah inquiry. Kurangi restock');
					
				} elseif($restock == $cekRestock ) { // jumlah inquiry telah sama dengan restock, lalu close order
					
					$updateRestock = $inquiry->restock + $restock;
					$dataArr = array('restock'=>$updateRestock,'inquiry_status'=>'3');
				
				} else {

					$updateRestock = $inquiry->restock + $restock;
				
					if($updateRestock > $inquiryQty) {

						return Redirect::to('inquiry/restock/'.$id)->with('message', 'Jumlah restock yang dimasukkan telah melebihi jumlah inquiry. Kurangi restock');
					
					} else {
						
						$dataArr = array('restock'=>$updateRestock,'inquiry_status'=>'1');
						
					}
				}

				$updateInquiry = InquiryOrder::where('id','=',$id)->update($dataArr);

			}

			// update current device stock
			$master        = MasterDevices::find($inquiry->id_device);
			$master->stock = $master->stock + $restock;
			$master->save();

			if ($updateInquiry) {

				$message = 'Proses restock untuk inquiry order berhasil. Stock perangkat '.$master->nama_device.' telah ditambah sebanyak '.$restock;
		
			} else {
		
				$message = 'Gagal menyimpan data';
		
			}

			return Redirect::to('inquiry')->with('message', $message);
		
		} // end of restock input validation

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
