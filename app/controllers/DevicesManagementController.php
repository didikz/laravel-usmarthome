<?php

class DevicesManagementController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Session::get('role')=='AM' || Session::get('idrole')=='5') {

			$data =  MasterDevices::where('status','=', 'unhide')
								  ->where('id_vendor','=', '1')
								  ->where('active','=', '1')
								  ->get();	
		
		} elseif(Session::get('role')=='mitra' || Session::get('idrole')=='2') {

			$data = MasterDevices::where('id_vendor','=', Session::get('idvendor'))
								 ->where('active','=','1')
								 ->get();

		} else {
		
			$data = MasterDevices::where('active','=','1')->get();
		
		}

        return View::make('devicesmanagement.index', array('data' => $data));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

        return View::make('devicesmanagement.create');
	
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$nama                 = Input::get('nama_devices');
		$spesifikasi          = Input::get('spesifikasi');
		$harga                = Input::get('harga');
		$stock                = Input::get('stock');
		$status               = Input::get('status_device');
		
		$devices              = new MasterDevices;
		$devices->nama_device = $nama;
		$devices->spesifikasi = $spesifikasi;
		$devices->harga       = $harga;
		$devices->stock       = $stock;
		$devices->status      = $status;
		$devices->last_update = date('Y-m-d H:i:s');
		$devices->updated_by  = Session::get('username');
		$devices->id_vendor   = Session::get('idvendor');


		if (Input::hasFile('image')) {

			$destination    = 'deviceimages/';
			
			Input::file('image')->move($destination, Input::file('image')->getClientOriginalName());
			
			$devices->image = Input::file('image')->getClientOriginalName();
		
		} 
		else {

			$devices->image = '';
		
		}

		$storeDevice = $devices->save();
		
		if ($storeDevice) {

			return Redirect::to('managementdevices')->with('message', 'Device '.$nama.' berhasil disimpan');
		
		} else {

			return Redirect::to('managementdevices')->with('message', 'Device '.$nama.' gagal disimpan');
		
		}
		

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		$data = MasterDevices::find($id);

        return View::make('devicesmanagement.show', array('data' => $data));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = MasterDevices::find($id);

        return View::make('devicesmanagement.edit', array('data' => $data));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$nama        = Input::get('nama_devices');
		$spesifikasi = Input::get('spesifikasi');
		$harga       = Input::get('harga');
		$stock       = Input::get('stock');
		$status      = Input::get('status_device');
		$imageOld    = Input::get('image_old');

		$devices              = MasterDevices::find($id);
		$devices->nama_device = $nama;
		$devices->spesifikasi = $spesifikasi;
		$devices->harga       = $harga;
		$devices->stock       = $stock;
		$devices->status      = $status;
		$devices->last_update = date('Y-m-d H:i:s');
		$devices->updated_by  = Session::get('username');


		if (Input::hasFile('image')) {

			$destination    = 'deviceimages/';
			
			if (!empty($imageOld)) {

				// remove file
				File::delete($destination.'/'.$imageOld);	
			
			}
			
			Input::file('image')->move($destination, Input::file('image')->getClientOriginalName());
			$devices->image = Input::file('image')->getClientOriginalName();
		
		}

		$updateDevice = $devices->save();
		
		if ($updateDevice) {

			return Redirect::to('managementdevices')->with('message', 'Device '.$nama.' berhasil dirubah');
		
		} else {

			return Redirect::to('managementdevices')->with('message', 'Device '.$nama.' gagal dirubah');
		
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		// do not delete data, just set active = 0
		$device = MasterDevices::where('id_devices','=',$id)->update(array('active'=>'0'));
		
		if($device) {

			$message = 'Device berhasil dihapus';
		
		} else {
		
			$message = 'Device gagal dihapus';
		
		}

		return Redirect::to('managementdevices')->with('message', $message);
	}

}
