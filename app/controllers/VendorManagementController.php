<?php

class VendorManagementController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$vendor = MasterVendor::where('active','=','1')->get();

        return View::make('vendormanagements.index', array('vendor'=>$vendor));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$witel = array(
				'Bangka Belitung (Pangkal Pinang)',
				'Banten Barat (Serang)',
				'Banten Timur (Tangerang)',
				'Bengkulu',
				'Jabar Barat (Bogor)',
				'Jabar Barat Utara (Bekasi)',
				'Jabar Selatan (Sukabumi)',
				'Jabar Tengah (Bandung)',
				'Jabar Timsel (Tasikmalaya)',
				'Jabar Timur (Cirebon)',
				'Jabar Utara (Karawang)',
				'Jakarta Barat',
				'Jakarta Pusat',
				'Jakarta Selatan',
				'Jakarta Timur',
				'Jakarta Utara',
				'Jambi',
				'Lampung',
				'NAD (Aceh)',
				'Riau Daratan (Pekanbaru)',
				'Riau Kepulauan (Batam)',
				'Sumatera Barat (Padang)',
				'Sumatera Selatan (Palembang)',
				'Sumut Barat (Medan)',
				'Sumut Timur (Pematang Siantar)',
				'Bali Selatan (Denpasar)',
				'Bali Utara (Singaraja)',
				'DI Yogyakarta',
				'Jateng Barat Selatan (Purwokerto)',
				'Jateng Barat Utara (Pekalongan)',
				'Jateng Selatan (Magelang)',
				'Jateng Tengah (Salatiga)',
				'Jateng Timur Selatan (Solo)',
				'Jateng Timur Utara (Kudus)',
				'Jateng Utara (Semarang)',
				'Jatim Barat (Madiun)',
				'Jatim Selatan (Malang)',
				'Jatim Selatan Timur (Pasuruan)',
				'Jatim Suramadu (Surabaya)',
				'Jatim Tengah (Kediri)',
				'Jatim Tengah Timur (Sidoarjo)',
				'Jatim Timur (Jember)',
				'Jatim Utara (Gresik)',
				'Kalbar (Pontianak)',
				'Kalsel (Banjarmasin)',
				'Kalteng (Palangkaraya)',
				'Kaltimsel (Balikpapan)',
				'Kaltimteng (Samarinda)',
				'Kaltimut (Tarakan)',
				'Maluku (Ambon)',
				'Maluku Utara (Ternate)',
				'NTB (Mataram)',
				'NTT (Kupang)',
				'Papua (Jayapura)',
				'Papua Barat (Sorong)',
				'Sulsel (Makassar)',
				'Sulsel Barat (Pare-pare)',
				'Sulteng (Palu)',
				'Sultra (Kendari)',
				'Sulut (Menado)',
				'Sulut Selatan (Gorontalo)'
			);
        return View::make('vendormanagements.create', array('witel'=>$witel));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$vendor_name     = Input::get('vendor_name');
		$vendor_location = Input::get('vendor_location');
		$vendor_address  = Input::get('vendor_address');
		$vendor_phone    = Input::get('vendor_phone');
		$vendor_email    = Input::get('vendor_email');

		$store = MasterVendor::create(array(
									'vendor_name'     => $vendor_name,
									'vendor_location' => $vendor_location,
									'vendor_address'  => $vendor_address,
									'vendor_phone'    => $vendor_phone,
									'vendor_email'    => $vendor_email
								));
		if ($store) {

			return Redirect::to('managementmitra')->with('message','Mitra '.$vendor_name.' berhasil ditambahkan');
		
		} else {
		
			return Redirect::to('managementmitra/new')->with('message','Error. Mitra '.$vendor_name.' gagal ditambahkan ke database');
		
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$vendor = MasterVendor::find($id);

        return View::make('vendormanagements.show', array('vendor'=>$vendor));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		$witel = array(
				'Bangka Belitung (Pangkal Pinang)',
				'Banten Barat (Serang)',
				'Banten Timur (Tangerang)',
				'Bengkulu',
				'Jabar Barat (Bogor)',
				'Jabar Barat Utara (Bekasi)',
				'Jabar Selatan (Sukabumi)',
				'Jabar Tengah (Bandung)',
				'Jabar Timsel (Tasikmalaya)',
				'Jabar Timur (Cirebon)',
				'Jabar Utara (Karawang)',
				'Jakarta Barat',
				'Jakarta Pusat',
				'Jakarta Selatan',
				'Jakarta Timur',
				'Jakarta Utara',
				'Jambi',
				'Lampung',
				'NAD (Aceh)',
				'Riau Daratan (Pekanbaru)',
				'Riau Kepulauan (Batam)',
				'Sumatera Barat (Padang)',
				'Sumatera Selatan (Palembang)',
				'Sumut Barat (Medan)',
				'Sumut Timur (Pematang Siantar)',
				'Bali Selatan (Denpasar)',
				'Bali Utara (Singaraja)',
				'DI Yogyakarta',
				'Jateng Barat Selatan (Purwokerto)',
				'Jateng Barat Utara (Pekalongan)',
				'Jateng Selatan (Magelang)',
				'Jateng Tengah (Salatiga)',
				'Jateng Timur Selatan (Solo)',
				'Jateng Timur Utara (Kudus)',
				'Jateng Utara (Semarang)',
				'Jatim Barat (Madiun)',
				'Jatim Selatan (Malang)',
				'Jatim Selatan Timur (Pasuruan)',
				'Jatim Suramadu (Surabaya)',
				'Jatim Tengah (Kediri)',
				'Jatim Tengah Timur (Sidoarjo)',
				'Jatim Timur (Jember)',
				'Jatim Utara (Gresik)',
				'Kalbar (Pontianak)',
				'Kalsel (Banjarmasin)',
				'Kalteng (Palangkaraya)',
				'Kaltimsel (Balikpapan)',
				'Kaltimteng (Samarinda)',
				'Kaltimut (Tarakan)',
				'Maluku (Ambon)',
				'Maluku Utara (Ternate)',
				'NTB (Mataram)',
				'NTT (Kupang)',
				'Papua (Jayapura)',
				'Papua Barat (Sorong)',
				'Sulsel (Makassar)',
				'Sulsel Barat (Pare-pare)',
				'Sulteng (Palu)',
				'Sultra (Kendari)',
				'Sulut (Menado)',
				'Sulut Selatan (Gorontalo)'
			);
		
		$vendor = MasterVendor::find($id);

        return View::make('vendormanagements.edit', array('witel'=>$witel, 'vendor'=>$vendor));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$vendor_name     = Input::get('vendor_name');
		$vendor_location = Input::get('vendor_location');
		$vendor_address  = Input::get('vendor_address');
		$vendor_phone    = Input::get('vendor_phone');
		$vendor_email    = Input::get('vendor_email');

		$store = MasterVendor::where('id','=',$id)
							 ->update(array(
									'vendor_name'     => $vendor_name,
									'vendor_location' => $vendor_location,
									'vendor_address'  => $vendor_address,
									'vendor_phone'    => $vendor_phone,
									'vendor_email'    => $vendor_email
								));
		if ($store) {

			return Redirect::to('managementmitra')->with('message','Mitra '.$vendor_name.' berhasil diupdate');
		
		} else {
		
			return Redirect::to('managementmitra/edit'.$id)->with('message','Error. Mitra '.$vendor_name.' gagal diupdate ke database');
		
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$update = MasterVendor::where('id','=', $id)->update(array('active'=>'0'));

		if ($update) {

			return Redirect::to('managementmitra')->with('message','Mitra berhasil dihapus');
		
		} else {
		
			return Redirect::to('managementmitra')->with('message','Error. Mitra gagal dihapus');
		
		}
	}

	public function userCreate($id)
	{
		$witel = array(
				'Bangka Belitung (Pangkal Pinang)',
				'Banten Barat (Serang)',
				'Banten Timur (Tangerang)',
				'Bengkulu',
				'Jabar Barat (Bogor)',
				'Jabar Barat Utara (Bekasi)',
				'Jabar Selatan (Sukabumi)',
				'Jabar Tengah (Bandung)',
				'Jabar Timsel (Tasikmalaya)',
				'Jabar Timur (Cirebon)',
				'Jabar Utara (Karawang)',
				'Jakarta Barat',
				'Jakarta Pusat',
				'Jakarta Selatan',
				'Jakarta Timur',
				'Jakarta Utara',
				'Jambi',
				'Lampung',
				'NAD (Aceh)',
				'Riau Daratan (Pekanbaru)',
				'Riau Kepulauan (Batam)',
				'Sumatera Barat (Padang)',
				'Sumatera Selatan (Palembang)',
				'Sumut Barat (Medan)',
				'Sumut Timur (Pematang Siantar)',
				'Bali Selatan (Denpasar)',
				'Bali Utara (Singaraja)',
				'DI Yogyakarta',
				'Jateng Barat Selatan (Purwokerto)',
				'Jateng Barat Utara (Pekalongan)',
				'Jateng Selatan (Magelang)',
				'Jateng Tengah (Salatiga)',
				'Jateng Timur Selatan (Solo)',
				'Jateng Timur Utara (Kudus)',
				'Jateng Utara (Semarang)',
				'Jatim Barat (Madiun)',
				'Jatim Selatan (Malang)',
				'Jatim Selatan Timur (Pasuruan)',
				'Jatim Suramadu (Surabaya)',
				'Jatim Tengah (Kediri)',
				'Jatim Tengah Timur (Sidoarjo)',
				'Jatim Timur (Jember)',
				'Jatim Utara (Gresik)',
				'Kalbar (Pontianak)',
				'Kalsel (Banjarmasin)',
				'Kalteng (Palangkaraya)',
				'Kaltimsel (Balikpapan)',
				'Kaltimteng (Samarinda)',
				'Kaltimut (Tarakan)',
				'Maluku (Ambon)',
				'Maluku Utara (Ternate)',
				'NTB (Mataram)',
				'NTT (Kupang)',
				'Papua (Jayapura)',
				'Papua Barat (Sorong)',
				'Sulsel (Makassar)',
				'Sulsel Barat (Pare-pare)',
				'Sulteng (Palu)',
				'Sultra (Kendari)',
				'Sulut (Menado)',
				'Sulut Selatan (Gorontalo)'
			);

		$vendor = MasterVendor::find($id);

		return View::make('vendormanagements.usersCreate', array('vendor'=>$vendor, 'witel'=>$witel));
	}

	public function userStore()
	{
		$username = Input::get('username');
		$password = Hash::make(Input::get('password'));
		$nama     = Input::get('nama');
		$email    = Input::get('email');
		$telepon  = Input::get('telepon');
		$loker    = Input::get('loker');
		$id_vendor = Input::get('id_vendor');
		$level    = 'mitra';

		$cekUsername = User::where('username', '=', $username)->first();
		$role = Role::where('role_name','=', $level)->first();

		if (empty($cekUsername->username)) 
		{
			$user           = new User;
			$user->username = $username;
			$user->password = $password;
			$user->LEVEL    = $level;
			$user->role     = $role['id'];
			$store          = $user->save();

			if ($store) {
				
				$master                = new MasterUsers;
				$master->id_user       = $user->ID;
				$master->nama          = $nama;
				$master->email         = $email;
				$master->telepon       = $telepon;
				$master->loker         = $loker;
				$master->updated_by    = Session::get('username');
				$save                  = $master->save();
				
				$uservendor            = new UserVendor;
				$uservendor->id_user   = $user->ID;
				$uservendor->id_vendor = $id_vendor;
				$save2                 = $uservendor->save();
				
				if($save && $save2) {

					return Redirect::to('managementmitra')->with('message', 'akun '.$username.' berhasil ditambahkan');
				
				} else {
				
					User::destroy($user->ID);
				
					return Redirect::to('managementmitra/users/'.$id_vendor)->with('message', 'terjadi kesalahan sistem, akun gagal ditambahkan');
				
				}
				
			
			} else {
				
				return Redirect::to('managementmitra/users/'.$id_vendor)->with('message', 'terjadi kesalahan sistem, akun gagal ditambahkan');
			
			}

		} else {
			
			return Redirect::to('users/new')->with('message', 'username '.$username.' sudah ada. Silahkan gunakan username yang lain.');
		
		}
	}

}
