<?php

class BindingController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		// get data user and gateway id
		$cust = Pelanggan::join('FAB', function($join)
							{

								$join->on('iot_users.AccountID', '=', 'FAB.AccountID');
							
							})->select(DB::raw('DISTINCT(FAB.AccountID), FAB.CustomerName, iot_users.username, iot_users.gwid, iot_users.updatedBy'))
							  ->get();

		$mac   = array();
		$count = array();
		$rmid  = array();

		foreach ($cust as $gw)
		{
			// get mac address
			$gwaddress      = Gwaddress::find($gw->gwid);
			
			// get attached devices count
			$getDeviceCount = Rmdevices::join('iot_gwrooms', function($join)
										{

											$join->on('iot_rmdevices.rmid', '=', 'iot_gwrooms.rmid');
										
										})->select(DB::raw('COUNT(iot_rmdevices.rmid) as rmid_count, iot_rmdevices.rmid'))
										  ->where('iot_gwrooms.gwid', '=', $gw->gwid)->first();

		    array_push($mac, $gwaddress->gwaddress);
			array_push($count, $getDeviceCount->rmid_count);		
			array_push($rmid, $getDeviceCount->rmid);				  
		}

        return View::make('bindings.index', array(
        										'users'=>$cust,
        										'gws'=>$mac, 
        										'count'=>$count,
        										'rmids'=>$rmid
        									));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('bindings.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($speedy, $rmid)
	{
		$data = Pelanggan::join('FAB', function($join){
									$join->on('iot_users.AccountID', '=', 'FAB.AccountID');
								})
								->where('iot_users.AccountID', '=', $speedy)->first();

		$devices = Rmdevices::where('rmid', '=', $rmid)->get();						

        return View::make('bindings.show')->with('data', $data)->with('devices', $devices);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('perangkats.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
