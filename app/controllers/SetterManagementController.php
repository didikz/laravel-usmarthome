<?php

class SetterManagementController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = MasterSetter::join('tblusers', function($join){
						
								$join->on('id_users', '=', 'ID');
						
							})->where(function($query){
						
								$query->where('LEVEL', '=', 'setter')
							  		  ->orWhere('role','=','3');
						
							})->where('id_vendor','=',Session::get('idvendor'))
							  ->get();

        return View::make('settermanagements.index', array('data' => $data));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

        return View::make('settermanagements.create');
	
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$name     = Input::get('settername');
		$email    = Input::get('email');
		$address  = Input::get('setteraddress');
		$phone    = Input::get('setterphone');
		$username = Input::get('setterusername');
		$password = Input::get('setterpassword');

		Input::flash();

		$checkUsername = User::where('username', '=', $username)->first();
		if (empty($checkUsername->username))
		{
			/**
			 * Store data setter, insert to tblusers first
			 * get inserted id and store to master setter
			 */

			$user           = new User;
			$user->username = $username;
			$user->password = Hash::make($password);
			$user->LEVEL    = 'setter';
			$user->role     = '3';
			$storeUser      = $user->save();

			if ($storeUser) {

				$idUser                 = $user->ID;
				
				$setter                 = new MasterSetter;
				$setter->id_users       = $idUser;
				$setter->email          = $email;
				$setter->setter_name    = $name;
				$setter->setter_phone   = $phone;
				$setter->setter_address = $address;
				$setter->last_update    = date('Y-m-d H:i:s');
				$setter->id_vendor      = Session::get('idvendor');
				$storeSetter            = $setter->save();

				if ($storeSetter) {

					return Redirect::to('setter')->with('message', 'Setter '.$name.' berhasil ditambahkan');
				
				} else {

					User::destroy($idUser);
					
					return Redirect::to('setter/new')->with('message', 'Setter '.$name.' gagal ditambahkan')->withInput();
				
				}

			}

		}
		else {

			return Redirect::to('setter/new')
					->with('message', 'username '.$username.' sudah ada. Silahkan gunakan username yang lain.')
					->withInput();
		
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data = MasterSetter::join('tblusers', function($join)
							{
							
								$join->on('id_users', '=', 'ID');
							
							})->where('id_setter', '=', $id)
							  ->first();

        return View::make('settermanagements.show', array('data' => $data));
	
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = MasterSetter::join('tblusers', function($join)
							{
							
								$join->on('id_users', '=', 'ID');
							
							})->where('id_setter', '=', $id)
							  ->first();

        return View::make('settermanagements.edit', array('data' => $data));
	
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$name     = Input::get('settername');
		$email    = Input::get('email');
		$address  = Input::get('setteraddress');
		$phone    = Input::get('setterphone');
		$password = Input::get('setterpassword');

		Input::flash();
		$setter                 = MasterSetter::find($id);
		$setter->email          = $email;
		$setter->setter_name    = $name;
		$setter->setter_phone   = $phone;
		$setter->setter_address = $address;
		$setter->last_update    = date('Y-m-d H:i:s');
		$updateSetter           = $setter->save();

		if (!empty($password)){

			$user           = User::find($setter->id_users);
			$user->password = Hash::make($password);
			$updateAccount  = $user->save();	
		
		}

		if ($updateSetter || $updateAccount) {

			return Redirect::to('setter')->with('message', 'Setter '.$name.' berhasil diperbarui.');
		
		} else {
		
			return Redirect::to('setter/edit/'.$id)->with('message', 'Setter '.$name.' gagal diperbarui.');
		
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$setter = MasterSetter::find($id);
		$nama   = $setter->setter_name;
		$idUser = $setter->id_users;

		MasterSetter::destroy($id);
		User::destroy($idUser);

		return Redirect::to('setter')->with('message', 'Setter '.$nama.' sudah dihapus');
	
	}

	/**
	 * Display a listing of the resource history.
	 *
	 * @return Response
	 */
	public function history()
	{
		$count = array();

		$master = MasterSetter::where('id_vendor','=', Session::get('idvendor'))->get();
		
		foreach ($master as $setter) {
	
			$activity = SetterActivity::select('id_setter_act')->where('id_setter', '=', $setter->id_setter)->count();
			array_push($count, $activity);
	
		}

		return View::make('settermanagements.indexhistory', array('data' => $master, 'count' => $count));		
	
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showHistory($id)
	{
		$master = MasterSetter::where('id_setter','=', $id)->select('setter_name','setter_address','setter_phone')->first();
		
		$data   = DB::table('setter_activity')
					->join('iot_users', 'userid', '=', 'id_user')
					->join('iot_gateways', 'iot_gateways.gwid', '=', 'setter_activity.gwid')
					->where('id_setter', '=', $id)
					->orderBy('last_update', 'DESC')
					->get();

        return View::make('settermanagements.showhistory', array('data' => $data, 'master' => $master));
	
	}

}
