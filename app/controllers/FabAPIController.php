<?php

class FabAPIController extends BaseController {

	/**
	 * return json as response call.
	 *
	 * @return Response
	 */
	public function cancellation()
	{
		$transid      = Input::get('transid');
		$transtype    = Input::get('transtype');
		$accountid    = Input::get('accountid');

		$status = Fab::select('Status','TransType')
					 ->where('AccountID','=',$accountid)
					 ->first();
		
			if($status['Status']=='Instalasi' || ($status['Status']=='Aktif' && $status['TransType']=='MODIFICATION')) {
				
				$requestStatusOrder = DB::table('request_status')
										->where('accountid','=',$accountid)
										->where('TransType','=',$status['TransType'])
										->first();
				
				if ($requestStatusOrder->status_order=='Open') { // jika belum melakukan order device

					// set cancel pada order management
					$statusRequest = RequestStatus::where('accountid','=',$accountid)
												  ->where('TransType','=',$status['TransType'])
												  ->first();

					$statusLog                    = new RequestStatusLog;
					$statusLog->id_request_status = $statusRequest->id_req_status;
					$statusLog->status_order      = $statusRequest->status_order;
					$statusLog->id_fab            = $statusRequest->id_fab;
					$statusLog->last_update       = date('Y-m-d H:i:s');
					$statusLog->start_update      = $statusRequest->last_update;
					$statusLog->keterangan        = $statusRequest->keterangan;
					$statusLog->updated_by        = $statusRequest->updated_by;
					$statusLog->accountid         = $statusRequest->accountid;
					$statusLog->TransType         = $statusRequest->TransType;
					$saveLog                      = $statusLog->save();

					$statusRequest->status_order  = 'Canceled';
					$statusRequest->keterangan    = '';
					$updateSR                     = $statusRequest->save();

					$response['status'] = 'T';
					$response['message'] = 'Layanan berhasil di cancel';

				} elseif($requestStatusOrder->status_order=='Proses Order Device') { // jika sedang dalam masa order device
					
					// proses cancellation order device
					$orderDevice = OrderDevice::join('status_order_device', function($join) {
													$join->on('order_device.invoice','=','status_order_device.invoice');
												})->where('cust_id','=',$accountid)
												  ->select('status_order_device.status','order_device.invoice','cust_id')
												  ->first();
					
					// set cancel pada order device dan kembalikan semua stock device yang dipesan
					$statusOrder = StatusOrderDevices::where('invoice','=',$orderDevice['invoice'])
													// ->update(array('status'=>'Canceled','last_update'=>date('Y-m-d H:i:s')));
													 ->first();
					// insert to log
					$statusOrderLog               = new StatusOrderDevicesLog;
					$statusOrderLog->id_status    = $statusOrder['id_status'];
					$statusOrderLog->status       = $statusOrder['status'];
					$statusOrderLog->start_update = $statusOrder['last_update'];
					$statusOrderLog->last_update  = date('Y-m-d H:i:s');
					$statusOrderLog->keterangan   = $statusOrder['keterangan'];
					$statusOrderLog->updated_by   = $statusOrder['updated_by'];
					$statusOrderLog->invoice      = $statusOrder['invoice'];
					$updateLog                    = $statusOrderLog->save();		
					
					$statusOrder->status  		  = 'Canceled';
					$statusOrder->last_update	  = date('Y-m-d H:i:s');
					$updateStatusOrder            = $statusOrder->save();

					$orderedDevice = OrderedDevices::where('invoice','=', $orderDevice['invoice'])
													->where('edited_status','=','0')
													->get();

					foreach ($orderedDevice as $device) {
						$master = MasterDevices::find($device->id_device);
						$stock = $master->stock + $device->qty;
						$master->stock = $stock;
						$master->save();
					}

					// set cancel pada order management
					$statusRequest = RequestStatus::where('accountid','=',$accountid)
												  ->where('TransType','=',$status['TransType'])
												  ->first();

					$statusLog                    = new RequestStatusLog;
					$statusLog->id_request_status = $statusRequest->id_req_status;
					$statusLog->status_order      = $statusRequest->status_order;
					$statusLog->id_fab            = $statusRequest->id_fab;
					$statusLog->last_update       = date('Y-m-d H:i:s');
					$statusLog->start_update      = $statusRequest->last_update;
					$statusLog->keterangan        = $statusRequest->keterangan;
					$statusLog->updated_by        = $statusRequest->updated_by;
					$statusLog->accountid         = $statusRequest->accountid;
					$statusLog->TransType         = $statusRequest->TransType;
					$saveLog                      = $statusLog->save();

					$statusRequest->status_order  = 'Canceled';
					$statusRequest->keterangan    = '';
					$updateSR                     = $statusRequest->save();


					if ($statusOrder && $updateSR && $saveLog) {
						$response['status'] = 'T';
						$response['message'] = 'Layanan berhasil di cancel, status '.$orderDevice['status'].' menjadi cancelled';
					} else {
						$response['status'] = 'F';
						$response['message'] = 'gagal update db';
					}								
				
				} else { // jika order layanan sudah close
					
					// do termination process
					$updateFABlog = Fablog::where('AccountID','=',$accountid)->where('result','=','Activation Success')
											->update(array('result'=>'Activation Success and has been terminated'));

					// copy FAB to history and delete //
					$getDataFAB = Fab::where('AccountID','=',$accountid)->first();
					$insertHistory = DB::table('FABhistory')->insert(array(
																'Status' => $getDataFAB['Status'],
																'TransType' => $getDataFAB['TransType'],
																'TransId' => $getDataFAB['TransId'],
																'PackageID' => $getDataFAB['PackageID'],
																'PromoID' => $getDataFAB['PromoID'],
																'AccountID' => $getDataFAB['AccountID'],
																'Cust_id' => $getDataFAB['Cust_id'],
																'CustomerName' => $getDataFAB['CustomerName'],
																'Address' => $getDataFAB['Address'],
																'DeveloperID' => $getDataFAB['DeveloperID'],
																'DeveloperCompany' => $getDataFAB['DeveloperCompany'],
																'DeveloperName' => $getDataFAB['DeveloperName'],
																'DeveloperTelp' => $getDataFAB['DeveloperTelp'],
																'TipeDeveloper' => $getDataFAB['TipeDeveloper'],
																'DeveloperEmail' => $getDataFAB['DeveloperEmail'],
																'alacarte' => $getDataFAB['alacarte'],
																'recording' => $getDataFAB['recording'],
																'lastUpdt' => $getDataFAB['lastUpdt']
															));

					$insertFABlog = Fablog::create(array(
																'Status' => 'Terminated',
																'TransType' => 'TERMINATION',
																'TransId' => $transid,
																'PackageID' => $getDataFAB['PackageID'],
																'PromoID' => $getDataFAB['PromoID'],
																'AccountID' => $getDataFAB['AccountID'],
																'Cust_id' => $getDataFAB['Cust_id'],
																'CustomerName' => $getDataFAB['CustomerName'],
																'Address' => $getDataFAB['Address'],
																'DeveloperID' => $getDataFAB['DeveloperID'],
																'DeveloperCompany' => $getDataFAB['DeveloperCompany'],
																'DeveloperName' => $getDataFAB['DeveloperName'],
																'DeveloperTelp' => $getDataFAB['DeveloperTelp'],
																'TipeDeveloper' => $getDataFAB['TipeDeveloper'],
																'DeveloperEmail' => $getDataFAB['DeveloperEmail'],
																'alacarte' => $getDataFAB['alacarte'],
																'recording' => $getDataFAB['recording'],
																'lastUpdt' => $getDataFAB['lastUpdt'],
																'result' => 'TERMINATION Berhasil'
															));	

					$getPelanggan = Pelanggan::where('AccountID','=',$accountid)->first();
					$insertPelangganHistory = DB::table('iot_users_history')->insert(array(
																					'username' => $getPelanggan['username'],
																					'password' => $getPelanggan['password'],
																					'gwid' => $getPelanggan['gwid'],
																					'enabled' => '0',
																					'lastupd' => $getPelanggan['lastupd'],
																					'active' => '0',
																					'id_pakage' => $getPelanggan['id_pakage'],
																					'phone' => $getPelanggan['phone'],
																					'AccountID' => $getPelanggan['accountid'],
																					'alamat' => $getPelanggan['alamat'],
																					'facebook' => $getPelanggan['facebook'],
																					'twitter' => $getPelanggan['twitter'],
																					'updatedBy' => $getPelanggan['updatedBy']
																				));		

					$deleteFAB = Fab::where('AccountID','=',$accountid)->delete();
					$deletePelanggan = Pelanggan::where('AccountID','=',$accountid)->delete();

					$response['status'] = 'T';
					$response['message'] = 'Layanan berhasil di cancel';
				}
				
			} else {
				$response['status'] = 'F';
				$response['message'] = 'Layanan sudah '.$status['Status'].', proses cancel gagal';
			}
		
        $data = json_encode($response);
        
        return $response;
	}

}
