<?php

class PelangganController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$segment = Request::segment(2);
		$mac = array();
		
		if ($segment=='aktif') {

			$data['type'] = 'Aktif';
			$cust = Pelanggan::join('FAB', function($join)
								{
								
									$join->on('iot_users.AccountID', '=', 'FAB.AccountID');
								
								})->select(DB::raw('DISTINCT(FAB.AccountID), FAB.CustomerName, iot_users.username, iot_users.alamat, iot_users.gwid, iot_users.updatedBy'))
								  ->where('iot_users.active', '=', '1')
								  ->get();
		
		} else {
			
			$data['type'] = 'Tidak Aktif';
			$cust = Pelanggan::join('FAB', function($join) 
								{
								
									$join->on('iot_users.AccountID', '=', 'FAB.AccountID');
								
								})->select(DB::raw('DISTINCT(FAB.AccountID), FAB.CustomerName, iot_users.username, iot_users.alamat, iot_users.gwid, iot_users.updatedBy'))
								  ->where('iot_users.active', '=', '0')
								  ->get();

		}

		foreach ($cust as $gw) {

			$gwaddress = Gwaddress::find($gw->gwid);
			array_push($mac, $gwaddress->gwaddress);
		
		}
        
        return View::make('customers.index', array(

												'data'      => $data, 
												'customers' => $cust, 
												'mac'       => $mac

        									));
        
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($accountid)
	{
		$data = Pelanggan::join('FAB', function($join)
								{
								
									$join->on('iot_users.AccountID', '=', 'FAB.AccountID');
								
								})->where('iot_users.AccountID', '=', $accountid)
								  ->first();

		$history = Pelanggan::join('FABlog', function($join)
								{
								
									$join->on('iot_users.AccountID', '=', 'FABlog.AccountID');
								
								})
								->where('iot_users.AccountID', '=', $accountid)
								->where('FABlog.result','LIKE', '%'.'Success')
								->orWhere('FABlog.result','LIKE', '%'.'Berhasil')
								->get();						

        return View::make('customers.show', array('data' => $data, 'history' => $history));
	}

}
