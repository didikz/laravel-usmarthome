<?php

class PerangkatController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$order  = OrderDevice::all();
		$counts = array();
		
		foreach ($order as $device) {
		
			$ordered = OrderedDevices::where('invoice', '=', $device->invoice)
									 ->where('edited_status','=','0')
									 ->select(DB::raw('SUM(qty) as jumlah'))
									 ->first();
	
			array_push($counts, $ordered['jumlah']);
		
		}
        
        return View::make('perangkats.index', array('orders' => $order, 'count' => $counts));
	
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($invoice)
	{

		$order   = OrderDevice::find($invoice);
		
		$ordered = OrderedDevices::join('master_device', function($join)
									{
									
										$join->on('id_devices', '=', 'id_device');
									
									})->where('invoice', '=', $invoice)
									  ->where('edited_status','=','0')
									  ->get();
		
        return View::make('perangkats.show', array('order'=> $order, 'ordered' => $ordered));
	
	}

}
