<?php

class OrderDevicesController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$qty           = array();
		$assignInvoice = array();

		if (Session::get('role')=='mitra') {
			
			$data = OrderDevice::join('status_order_device', function($join)
								{
			
									$join->on('order_device.invoice', '=', 'status_order_device.invoice');
			
								})->orderBy('status_order_device.last_update', 'DESC')
								  ->get();

		} elseif (Session::get('role')=='setter') {

		    
		    $assigned = AssignSetter::select('invoice')
		    						->where('id_setter','=',Session::get('idsetter'))
		    						->where('status','=','0')
		    						->get();

		    $data = OrderDevice::join('status_order_device', function($join)
		    					{
								
									$join->on('order_device.invoice', '=', 'status_order_device.invoice');
								
								})->orderBy('status_order_device.last_update', 'DESC')
								  ->get();
			
			foreach ($assigned as $datanya) {

				array_push($assignInvoice, $datanya->invoice);
				
			}
		
		} elseif (Session::get('role')=='admin' || Session::get('role')=='ESA DSC' || Session::get('role')=='CO AM') {
			
			$data = OrderDevice::join('status_order_device', function($join)
								{
								
									$join->on('order_device.invoice', '=', 'status_order_device.invoice');
								
								})->orderBy('status_order_device.last_update', 'DESC')
								  ->get();
		} else {
			
			$idam = MasterAM::where('id_user','=', Auth::user()->ID)->select('id_am')->first();

			$data = OrderDevice::join('status_order_device', function($join)
								{
								
									$join->on('order_device.invoice', '=', 'status_order_device.invoice');
								
								})->where('order_device.id_am', '=', $idam['id_am'])
								  ->orderBy('status_order_device.last_update', 'DESC')
								  ->get();

		}

		foreach ($data as $device) {

			$dev = OrderedDevices::select(DB::raw('SUM(qty) as jumlah'))
								 ->where('invoice', '=', $device->invoice)
								 ->where('edited_status','=','0')
								 ->first();
		
			array_push($qty, $dev->jumlah);
		
		}
		
        return View::make('orderdevices.index', array(

													'data'     => $data,
													'qty'      => $qty,
													'assigned' => $assignInvoice
        	
        										));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($accountid, $type)
	{

		// get master device data
		$data  = MasterDevices::status()->get();
		$delta = 0;
		$kuota = '';

		if ($type=='aktivasi') {
			
			//get data fab
			$fab = Fablog::SearchForOrderDevice($accountid, 'ACTIVATION')->first();
			
			if ($fab['PackageID']=='1' || $fab['PackageID']=='6' || $fab['PackageID']=='7' || $fab['PackageID']=='8') {
			
			 	// device untuk PSB
				$kuota = '3';
			
			} elseif ($fab['PackageID']=='2' || $fab['PackageID']=='9' || $fab['PackageID']=='10' || $fab['PackageID']=='11') { //Ala carte Basic - 1
			
				$kuota = '13';
			
			} elseif($fab['PackageID']=='3' || $fab['PackageID']=='12' || $fab['PackageID']=='13' || $fab['PackageID']=='14') { // Ala carte Bronze - 2
			
				$kuota = '23';
			
			} elseif($fab['PackageID']=='4' || $fab['PackageID']=='15' || $fab['PackageID']=='16' || $fab['PackageID']=='17') { // Ala carte Silver - 3
			
				$kuota = '33';
			
			} elseif($fab['PackageID']=='5' || $fab['PackageID']=='18' || $fab['PackageID']=='19' || $fab['PackageID']=='20') { // Ala carte Gold - 4
			
				$kuota = '43';
			
			}

		} elseif ($type=='modifikasi') {
			
			//get data fab
			$fab = Fablog::SearchForOrderDevice($accountid, 'MODIFICATION')->first();
			
			if ($fab['PackageID']=='1' || $fab['PackageID']=='6' || $fab['PackageID']=='7' || $fab['PackageID']=='8') {
			
			// device untuk PSB
				$kuota = '3';
			
			} elseif ($fab['PackageID']=='2' || $fab['PackageID']=='9' || $fab['PackageID']=='10' || $fab['PackageID']=='11') { //Ala carte Basic - 1
			
				$kuota = '13';
			
			} elseif($fab['PackageID']=='3' || $fab['PackageID']=='12' || $fab['PackageID']=='13' || $fab['PackageID']=='14') { // Ala carte Bronze - 2
			
				$kuota = '23';
			
			} elseif($fab['PackageID']=='4' || $fab['PackageID']=='15' || $fab['PackageID']=='16' || $fab['PackageID']=='17') { // Ala carte Silver - 3
			
				$kuota = '33';
			
			} elseif($fab['PackageID']=='5' || $fab['PackageID']=='18' || $fab['PackageID']=='19' || $fab['PackageID']=='20') { // Ala carte Gold - 4
			
				$kuota = '43';
			
			}

		}
		
		//cek booking order and booking device
		$qty         = array();
		$harga       = array();
		$bookedOrder = BookingOrder::where('AccountID','=',$accountid)
									->where('status','=','1')
									->select('id')
									->first();

		foreach ($data as $device) {

			$temp = BookingDevices::where('id_order','=',$bookedOrder['id'])
									->where('id_device','=', $device->id_devices)
									->first();
			
			array_push($qty, $temp['qty']);
			
			if($temp['harga_booking']!==NULL) {

				$hargaDevice = $temp['harga_booking'];
			
			} else {
			
				$hargaDevice = $device->harga;
			
			}

			array_push($harga, $hargaDevice);
			
		}

        return View::make('orderdevices.create', array(
													
													'data'      => $data,
													'kuota'     => $kuota,
													'fab'       => $fab,
													'qty'       => $qty,
													'idbooking' => $bookedOrder['id'],
													'type'		=> $type,
													'delta'		=> $delta,
													'harga'		=> $harga
        											
        										));
	}

	public function createAlacarte($accountid)
	{
		// get master device data
		$data  = MasterDevices::status()->get();
		$delta = 0;
		$kuota = '';

		$fab    = Fab::where('AccountID','=',$accountid)->first();
		
		$fablog = Fablog::SearchForOrderDevice($accountid, 'ACTIVATION')->first();
		
		$totalDevice = OrderedDevices::join('order_device', function($join)
						{
						
							$join->on('order_device.invoice','=','ordered_device.invoice');
						
						})->select(DB::raw('SUM(qty) as jumlah'))
						  ->where('order_device.cust_id','=',$accountid)
						  ->first();

		$customer = OrderDevice::where('cust_id','=',$accountid)->first();				  

		if ($fab['PackageID']=='1' || $fab['PackageID']=='6' || $fab['PackageID']=='7' || $fab['PackageID']=='8') {
		
			// device untuk PSB
			$kuota = '3';
		
		} elseif ($fab['PackageID']=='2' || $fab['PackageID']=='9' || $fab['PackageID']=='10' || $fab['PackageID']=='11') { //Ala carte Basic - 1
		
			$kuota = '13';
		
		} elseif($fab['PackageID']=='3' || $fab['PackageID']=='12' || $fab['PackageID']=='13' || $fab['PackageID']=='14') { // Ala carte Bronze - 2
		
			$kuota = '23';
		
		} elseif($fab['PackageID']=='4' || $fab['PackageID']=='15' || $fab['PackageID']=='16' || $fab['PackageID']=='17') { // Ala carte Silver - 3
		
			$kuota = '33';
		
		} elseif($fab['PackageID']=='5' || $fab['PackageID']=='18' || $fab['PackageID']=='19' || $fab['PackageID']=='20') { // Ala carte Gold - 4
		
			$kuota = '43';
		
		}				  

		$delta = $kuota - $totalDevice['jumlah'];

		return View::make('orderdevices.alacarte', array(
													
													'data'      => $data,
													'kuota'     => $kuota,
													'fab'       => $fab,
													'delta'		=> $delta,
													'customer'  => $customer
        											
        											));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$transid      = Input::get('transid');
		$idfab        = Input::get('idfab');
		$dev_company  = Input::get('developer_company');
		$dev_name     = Input::get('developer_name');
		$dev_telp     = Input::get('developer_telp');
		$dev_email    = Input::get('developer_email');
		$speedy       = Input::get('speedy');
		$cust_type    = Input::get('cust_type');
		$cust_name    = Input::get('cust_name');
		$cust_phone   = Input::get('cust_phone');
		$cust_address = Input::get('alamat');
		$keterangan   = Input::get('keterangan');
		$date         = date('Y-m-d H:i:s');
		$count        = Input::get('device_count');
		$idbooking    = Input::get('idbooking');
		$order        = Input::get('order');
		$quantity     = 0;
		$paketOrder   = array('null','PSB','Ala carte Basic - 1','Ala carte Bronze - 2','Ala carte Silver - 3','Ala carte Gold - 4');

		//get id am
		$idam      = Session::get('idam');
		$orderType = '';

		if ($order=='aktivasi') {
		
			$orderType = 'activation';
		
		} elseif($order == 'modifikasi') {
		
			$orderType = 'modification';
		
		} else {
		
			$orderType = 'alacarte';
		
		}

		Input::flash();

		$order                   = new OrderDevice;
		$order->transid          = $transid;
		$order->dev_company      = $dev_company;
		$order->dev_name         = $dev_name;
		$order->dev_telp         = $dev_telp;
		$order->dev_email        = $dev_email;
		$order->cust_id          = $speedy;
		$order->cust_name        = $cust_name;
		$order->cust_phone       = $cust_phone;
		$order->alamat           = $cust_address;
		$order->tanggal_order    = $date;
		$order->keterangan_order = $keterangan;
		$order->updated_by       = Session::get('username');
		$order->id_am            = $idam;
		$order->orderType 		 = $orderType;
		$order->cust_type		 = $cust_type;
		$storeOrder              = $order->save();

		$getInvoice              = $order->invoice;

		if ($storeOrder) {

			$statusOrder                  = new StatusOrderDevices;
			$statusOrder->invoice         = $getInvoice;
			$statusOrder->status          = 'Open';
			$statusOrder->last_update     = $date;
			$statusOrder->updated_by      = Session::get('username');
			$storeStatus                  = $statusOrder->save();
			
			$statusRequest                = RequestStatus::where('id_fab','=',$idfab)
														->where('accountid', '=', $speedy)
														->first();
			
			$statusLog                    = new RequestStatusLog;
			$statusLog->id_request_status = $statusRequest->id_req_status;
			$statusLog->status_order      = $statusRequest->status_order;
			$statusLog->id_fab            = $statusRequest->id_fab;
			$statusLog->last_update       = $date;
			$statusLog->start_update      = $statusRequest->last_update;
			$statusLog->keterangan        = $statusRequest->keterangan;
			$statusLog->updated_by        = Session::get('username');
			$statusLog->accountid         = $statusRequest->accountid;
			$statusLog->TransType         = strtoupper($orderType);
			$saveLog                      = $statusLog->save();

			$statusRequest->status_order  = 'Proses Order Device';
			$updateSR                     = $statusRequest->save();

			for ($i=1; $i<=$count; $i++)
			{ 
				
				$idDevice    = Input::get('iddevice_'.$i);
				$qtyDevice   = Input::get('jumlah_'.$i);
				$hargaDevice = Input::get('harga_'.$i);

				// update current device stock
				$master        = MasterDevices::find($idDevice);
				$master->stock = $master->stock - $qtyDevice;
				$master->save();
				
				$devices                = new OrderedDevices;
				$devices->invoice       = $getInvoice;
				$devices->id_device     = $idDevice;
				$devices->qty           = $qtyDevice;
				$devices->ordered_price = $hargaDevice;
				$devices->save();

				$quantity += $qtyDevice;

				// data init untuk struk pemesanan perangkat
				$struk['nama_perangkat'][$i] = $master->nama_device;
				$struk['qty'][$i]            = $qtyDevice;
				$struk['ordered_price'][$i]  = $hargaDevice;
			}

			// update current device stock based on booking device
			$booked = BookingDevices::where('id_order','=', $idbooking)->get();
			
			foreach ($booked as $item) {
			
				$deviceid      = $item->id_device;
				$bookedQty     = $item->qty;
				$master        = MasterDevices::find($deviceid);
				$master->stock = $master->stock + $bookedQty;
				$master->save();
			
			}

			if ($storeStatus) {

				//send email notification to mitra
				$idpackage  = Fab::where('TransId','=', $transid)->first();
				
				$paket      = $paketOrder[$idpackage['PackageID']];

				$idvendor = '1';
				$mitra    = UserVendor::join('tblusers', function($join)
									{
									
										$join->on('tblusers.ID','=','id_user');
									
									})->join('master_users', function($join)
									{
									
										$join->on('user_vendor.id_user','=','master_users.id_user');
									
									})->where('user_vendor.id_vendor','=',$idvendor)
									  ->get();

				$emailAddress = array('enterprise@telkom.support','consumer@telkom.support');
				
				foreach ($mitra as $user) {
				
					array_push($emailAddress, $user->email);
				
				}

				$data = array(
						
						'email'       => $emailAddress,
						'invoice'     => $getInvoice,
						'alamat'	  => $cust_address,
						'speedy'      => $speedy,
						'paket'		  => $paket,
						'pelanggan'	  => $cust_name,
						'deviceCount' => $quantity

					);

				// data init bukti pemesanan
				$struk['speedy']       = $speedy;
				$struk['cust_name']    = $cust_name;
				$struk['cust_address'] = $cust_address;
				$struk['invoice']      = $getInvoice;
				$struk['count']        = $count;

				Mail::send('dashboard.orderdevicemail', $data, function($message) use ($data)
					   {

					   		$message->to($data['email'], 'Mitra dan ESA DSC')->subject('Open Order Device layanan USmarthome');
					   
					   });

				Mail::send('dashboard.struk', $struk, function($message) use ($dev_email)
					   {

					   		$message->to($dev_email, 'Pelanggan Usmarthome')->subject('Bukti Pemesanan Perangkat USmarthome');
					   
					   });

				return Redirect::to('orderdevices')->with('message', 'order device berhasil diproses');
				
			}

		} else {

			return Redirect::to('orderdevices')->with('message', 'Terjadi kesalahan, order device gagal diproses');
		
		}

	}

	public function storeAlacarte()
	{

		$transid      = Input::get('transid');
		$idfab        = Input::get('idfab');
		$dev_company  = Input::get('developer_company');
		$dev_name     = Input::get('developer_name');
		$dev_telp     = Input::get('developer_telp');
		$dev_email    = Input::get('developer_email');
		$speedy       = Input::get('speedy');
		$cust_name    = Input::get('cust_name');
		$cust_phone   = Input::get('cust_phone');
		$cust_address = Input::get('alamat');
		$keterangan   = Input::get('keterangan');
		$date         = date('Y-m-d H:i:s');
		$count        = Input::get('device_count');
		$idbooking    = Input::get('idbooking');
		$order 		  = Input::get('order');
		$quantity     = 0;

		//get id am
		$idam = Session::get('idam');

		Input::flash();

		$order                   = new OrderDevice;
		$order->transid          = '0';
		$order->dev_company      = $dev_company;
		$order->dev_name         = $dev_name;
		$order->dev_telp         = $dev_telp;
		$order->dev_email        = $dev_email;
		$order->cust_id          = $speedy;
		$order->cust_name        = $cust_name;
		$order->cust_phone       = $cust_phone;
		$order->alamat           = $cust_address;
		$order->tanggal_order    = $date;
		$order->keterangan_order = $keterangan;
		$order->updated_by       = Session::get('username');
		$order->id_am            = $idam;
		$order->orderType 		 = 'alacarte';
		$storeOrder              = $order->save();

		$getInvoice				 = $order->invoice;

		if ($storeOrder) {

			$invoice                      = $order->invoice;
			$statusOrder                  = new StatusOrderDevices;
			$statusOrder->invoice         = $invoice;
			$statusOrder->status          = 'Open';
			$statusOrder->last_update     = $date;
			$statusOrder->updated_by      = Session::get('username');
			$storeStatus                  = $statusOrder->save();

			for ($i=1; $i<=$count; $i++)
			{ 

				$idDevice = Input::get('iddevice_'.$i);
				$qtyDevice = Input::get('jumlah_'.$i);

				// update current device stock
				$master        = MasterDevices::find($idDevice);
				$master->stock = $master->stock - $qtyDevice;
				$master->save();
				
				// store ordered devices
				$devices                = new OrderedDevices;
				$devices->invoice       = $invoice;
				$devices->id_device     = $idDevice;
				$devices->qty           = $qtyDevice;
				$devices->ordered_price = $master->harga;
				$devices->save();

				$quantity += $qtyDevice;

				// data init untuk struk pemesanan perangkat
				$struk['nama_perangkat'][$i] = $master->nama_device;
				$struk['qty'][$i]            = $qtyDevice;
				$struk['ordered_price'][$i]  = $master->harga;

			}

			if ($storeStatus) {

				//send email notification to mitra
				$idvendor = '1';
				$mitra = UserVendor::join('tblusers', function($join)
									{
									
										$join->on('tblusers.ID','=','id_user');
									
									})->join('master_users', function($join)
									{
									
										$join->on('user_vendor.id_user','=','master_users.id_user');
									
									})->where('user_vendor.id_vendor','=',$idvendor)
									  ->get();

				$emailAddress = array('enterprise@telkom.support','consumer@telkom.support');
				// $emailAddress = array('d2k_jombloz@yahoo.com','didiktrisusanto@yahoo.com');
				foreach ($mitra as $user) {
				
					array_push($emailAddress, $user->email);
				
				}

				$data = array(
						
						'email'       => $emailAddress,
						'invoice'     => $order->invoice,
						'alamat'	  => $cust_address,
						'speedy'      => $speedy,
						'paket'		  => 'order alacarte',
						'pelanggan'	  => $cust_name,
						'deviceCount' => $quantity

					);

				// data init bukti pemesanan
				$struk['speedy']       = $speedy;
				$struk['cust_name']    = $cust_name;
				$struk['cust_address'] = $cust_address;
				$struk['invoice']      = $getInvoice;
				$struk['count']        = $count;
				
				// send notif order device email to mitra and esa
				$send = Mail::send('dashboard.orderdevicemail', $data, function($message) use ($data)
					   {

					   		$message->to($data['email'], 'Mitra & ESA DSC')->subject('Open Order Device layanan Home Automation');
					  
					   });

				// send struk to customer
				Mail::send('dashboard.struk', $struk, function($message) use ($dev_email)
					   {

					   		$message->to($dev_email, 'Pelanggan Usmarthome')->subject('Bukti Pemesanan Perangkat USmarthome');
					   
					   });

				return Redirect::to('orderdevices')->with('message', 'order device berhasil diproses');
				
			}

		} else {

			return Redirect::to('orderdevices')->with('message', 'Terjadi kesalahan, order device gagal diproses');
		
		}
	}

	/**
	 * Display the specified resource.
	 * 
	 * Data: All data order device, all data status order device and log, all data ordered devices, device name from master device
	 * 
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		$status     = StatusOrderDevices::find($id);

		$log        = StatusOrderDevicesLog::where('status_order_device_log.id_status', '=', $status->id_status)
											->orderBy('start_update')
											->get();

		$order      = OrderDevice::find($status->invoice);
		
		$devices    = OrderDevice::find($status->invoice)->devices;

		$nameDevice = array();
		$qty        = array();
		$unitPrice  = array();
		$orderTotal = 0;
		
		foreach ($devices as $device) {
			
			$master = MasterDevices::find($device->id_device);
			
			array_push($nameDevice, $master->nama_device);
			array_push($qty, $device->qty);

			$temp = $device->ordered_price * $device->qty;
			array_push($unitPrice, $temp);
			
			$orderTotal += $temp;
			
		}

		$requestDate    = new DateTime($status->last_update);
		$nowDate        = new DateTime(date("Y-m-d H:i:s"));
		$selisih        = $requestDate->diff($nowDate);
		$displaySelisih = $selisih->d.' hari, '.$selisih->h.' jam, '.$selisih->i.' menit';

        return View::make('orderdevices.show')
        			->with('status', $status)
        			->with('logs', $log)
        			->with('order', $order)
        			->with('name', $nameDevice)
        			->with('qty', $qty)
        			->with('price', $unitPrice)
        			->with('total',$orderTotal)
        			->with('selisih', $displaySelisih);
	}

	/**
	 * show confirmation page
	 *
	 * @return Response
	 */
	public function confirm()
	{
		
		// get all input
		$transid      = Input::get('transid');
		$idfab        = Input::get('idfab');
		$dev_company  = Input::get('developer_company');
		$dev_name     = Input::get('developer_name');
		$dev_telp     = Input::get('developer_telp');
		$dev_email    = Input::get('developer_email');
		$speedy       = Input::get('custid');
		$cust_type    = Input::get('cust_type');
		$cust_name    = Input::get('cust_name');
		$cust_phone   = Input::get('cust_phone');
		$cust_address = Input::get('alamat');
		$order        = Input::get('type');
		$keterangan   = Input::get('keterangan');
		$kuota        = Input::get('kuota');
		$idbooking    = Input::get('idbooking');
		$keterangan   = Input::get('keterangan');

		Input::flash();

		$count         = MasterDevices::status()->count();
		
		$iddevices     = array();
		$namadevices   = array();
		$jumlahdevices = array();
		$prices        = array();
		$orderCount    = 0;
		$qty           = 0;
		$total         = 0;

		for ($i=1; $i <= $count; $i++) { 

			$idDevice    = Input::get('id_device_'.$i);
			$qtyDevice   = Input::get('jumlah_'.$i);
			$namaDevice  = Input::get('namedevice_'.$i);
			$hargaDevice = Input::get('price_'.$i);

			if ($qtyDevice!=='' || $qtyDevice > 0) {
				
				$masterDevice = MasterDevices::where('id_devices','=', $idDevice)
											->select('harga','stock','nama_device')
											->first();
				
				if ($qtyDevice > $masterDevice['stock']) {
			
					return Redirect::to('orderdevices/new/'.$speedy.'/'.$order)
									->with('message', 'Jumlah order perangkat '.$masterDevice['nama_device'].' yang anda masukkan lebih besar dari stock perangkat yang tersedia. Kurangi jumlah order perangkat')
									->withInput();
					break;
			
				}
			
				array_push($prices, $hargaDevice);
				array_push($iddevices, $idDevice);
				array_push($namadevices, $namaDevice);
				array_push($jumlahdevices, $qtyDevice);
				
				$total += $hargaDevice;
	
				$qty += $qtyDevice;
	
				$orderCount++;
			
			} 
		
		}

		// cek apakah device sudah sesuai dengan paket yang didaftarkan
		if ($qty > $kuota) {
		
			return Redirect::to('orderdevices/new/'.$speedy.'/'.$order)
							->with('message', 'Jumlah order perangkat yang anda masukkan lebih besar dari kuota maksimal paket yang didaftarkan. Kurangi jumlah order perangkat')
							->withInput();
		
		}

		$data = array(
			'transid'      => $transid,
			'idfab'        => $idfab,
			'dev_company'  => $dev_company,
			'dev_name'     => $dev_name,
			'dev_telp'     => $dev_telp,
			'dev_email'    => $dev_email,
			'speedy'       => $speedy,
			'cust_type'	   => $cust_type,
			'cust_name'    => $cust_name,
			'cust_phone'   => $cust_phone,
			'cust_address' => $cust_address,
			'order'        => $order,
			'keterangan'   => $keterangan,
			'count'        => $count,
			'ordercount'   => $orderCount,
			'idbooking'    => $idbooking,
			'keterangan'   => $keterangan
		);

		return View::make('orderdevices.confirm')
					->with('data', $data)
					->with('id', $iddevices)
					->with('name', $namadevices)
					->with('total', $total)
					->with('price', $prices)
					->with('qty', $jumlahdevices);
	}

	public function alacarteConfirm()
	{

		$transid       = Input::get('transid');
		$idfab         = Input::get('idfab');
		$dev_company   = Input::get('developer_company');
		$dev_name      = Input::get('developer_name');
		$dev_telp      = Input::get('developer_telp');
		$dev_email     = Input::get('developer_email');
		$speedy        = Input::get('custid');
		$cust_type     = Input::get('cust_type');
		$cust_name     = Input::get('cust_name');
		$cust_phone    = Input::get('cust_phone');
		$cust_address  = Input::get('alamat');
		$order         = Input::get('type');
		$keterangan    = Input::get('keterangan');
		$kuota         = Input::get('kuota');
		$idbooking     = Input::get('idbooking');
		
		$count         = MasterDevices::status()->count();
		
		$iddevices     = array();
		$namadevices   = array();
		$jumlahdevices = array();
		$prices        = array();
		$orderCount    = 0;
		$qty           = 0;
		$total         = 0;

		for ($i=1; $i <= $count; $i++) { 

			$idDevice    = Input::get('id_device_'.$i);
			$qtyDevice   = Input::get('jumlah_'.$i);
			$namaDevice  = Input::get('namedevice_'.$i);
			// $hargaDevice = Input::get('price_'.$i);

			if ($qtyDevice !=='' || $qtyDevice > 0) {
				
				$master = MasterDevices::where('id_devices','=', $idDevice)
										->select('harga','stock','nama_device')
										->first();
				
				if ($qtyDevice > $master['stock']) {
				
					return Redirect::to('orderdevices/alacarte/new/'.$speedy)
									->with('message', 'Jumlah order perangkat '.$master['nama_device'].' yang anda masukkan lebih besar dari stock paket yang tersedia. Kurangi jumlah order perangkat')
									->withInput();

					break;
				
				}
				
				array_push($prices, $master['harga']);
				array_push($iddevices, $idDevice);
				array_push($namadevices, $namaDevice);
				array_push($jumlahdevices, $qtyDevice);
				
				$total += $master['harga'];
	
				$qty += $qtyDevice;
	
				$orderCount++;
			
			} 
		}

		// cek apakah device sudah sesuai dengan paket yang didaftarkan
		if ($qty > $kuota) {

			return Redirect::to('orderdevices/alacarte/new/'.$speedy)->with('message', 'Jumlah order perangkat yang anda masukkan lebih besar dari kuota maksimal paket yang didaftarkan. Kurangi jumlah order perangkat')->withInput();
		
		}

		$data = array(
			'transid'      => $transid,
			'idfab'        => $idfab,
			'dev_company'  => $dev_company,
			'dev_name'     => $dev_name,
			'dev_telp'     => $dev_telp,
			'dev_email'    => $dev_email,
			'speedy'       => $speedy,
			'cust_type'    => $cust_type,
			'cust_name'    => $cust_name,
			'cust_phone'   => $cust_phone,
			'cust_address' => $cust_address,
			'order'        => $order,
			'keterangan'   => $keterangan,
			'count'        => $count,
			'ordercount'   => $orderCount,
			'idbooking'    => $idbooking
		);

		return View::make('orderdevices.confirm')
					->with('data', $data)
					->with('id', $iddevices)
					->with('name', $namadevices)
					->with('total', $total)
					->with('price', $prices)
					->with('qty', $jumlahdevices);
	}

	/**
	 * Show the form for change status the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function response($id)
	{

		$status     = StatusOrderDevices::find($id);
		
		$order      = OrderDevice::find($status->invoice);
		
		$devices    = OrderDevice::find($status->invoice)->devices;

		if ($status->status=='Confirmation') {
			
			$idvendor = '1'; // temporary id vendor, change dynamicaly later for multivendor purpose
			$setter = MasterSetter::select('id_setter', 'setter_name')
									->where('id_vendor','=','1')
									->get();
	
		} else {
	
			$setter = '0';
	
		}

		$nameDevice = array();
		$qty        = array();

		
		foreach ($devices as $device) {
			
			$name = MasterDevices::find($device->id_device);
			array_push($nameDevice, $name->nama_device);
			array_push($qty, $device->qty);
			
		}

        return View::make('orderdevices.response')
        			->with('order', $order)
        			->with('status', $status)
        			->with('device', $nameDevice)
        			->with('setters', $setter)
        			->with('qty', $qty);
	
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateResponse()
	{
		$idstatus       = Input::get('idstatus');
		$invoice        = Input::get('invoice');
		$status         = Input::get('status_order_device');
		$currentstatus  = Input::get('currentstatus');
		$currentupdater = Input::get('currentupdater');
		$startDate      = Input::get('date');
		$dateNow        = date('Y-m-d H:i:s');
		
		$order          = OrderDevice::find($invoice);
		
		if (Input::get('keterangan_order_device')!=='') {
		
			$keterangan = Input::get('keterangan_order_device');
		
		} else {
		
			$keterangan = NULL;
		
		}

		if (Input::get('currentketerangan')!=='') {
		
			$cur_ket = Input::get('currentketerangan');
		
		} else {
		
			$cur_ket =NULL;
		
		}

		if ($currentstatus!=='Closed' && $status=='Closed') {

			return Redirect::to('orderdevices/response/'.$idstatus)->with('message', 'Device belum di install, order gagal ditutup')->withInput();
		
		} elseif($currentstatus == $status) {
		
			return Redirect::to('orderdevices/response/'.$idstatus)->with('message', 'Update data gagal, status '.$status.' sedang proses atau sudah selesai.')->withInput();
		
		} else {

			if($status=='Close') {

				if($order['OrderType']=='activation' || $order['OrderType']=='modification') {

						// call API for SaaS Notification
						$fab = Fab::where('AccountID', '=', $order['cust_id'])
									->where('TransId', '=', $order['transid'])
									->select('TransId','TransType')
									->first();
						
						$type = '';
						
						if ($fab['TransType']=='ACTIVATION') {
						
							$fabResult = 'Activation Success';
						
						} elseif($fab['TransType']=='MODIFICATION') {
						
							$fabResult = 'Modification Success';
						
						}			

						$requestLog = 'OrderID='.$fab['TransId'].', type='.$fab['TransType'].', cust_id='.$order['cust_id'];
									
						$url = 'http://118.98.97.139/sha/soap/omahsclient.php?transid='.$fab['TransId'].'&type='.$fab['TransType'].'&custid='.$order['cust_id'];
						// $url = 'http://localhost/sha/soap/get.php?transid='.$fab['TransId'].'&type='.$fab['TransType'].'&custid='.$order['cust_id'];
						$ch  = curl_init();
						curl_setopt($ch, CURLOPT_URL,$url);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_TIMEOUT, 0);
						$data = curl_exec($ch);
						curl_close($ch);
						$decode = json_decode($data);

						if($decode->OutStatus=='T') {

							// update Status FAB to aktif			
							$fab       = Fab::where('AccountID', '=', $order['cust_id'])
											->update(array('Status'=>'Aktif'));
							//update iot_users
							$pelanggan = Pelanggan::where('AccountID', '=', $order['cust_id'])
												->update(array('active'=>'1'));
							
							// close order layanan
							$fablog    = Fablog::where('AccountID', '=', $order['cust_id'])
												->where('result', '=', $fabResult)
												->select('id')
												->first();

							$idfab     = $fablog['id'];

							// set close current order request
							$request = RequestStatus::where('id_fab', '=', $idfab)
													->update(array(
															'status_order' =>'Closed',
															'keterangan'   => '',
															'updated_by'   => Session::get('username')
															)
															
														);

							$request2 = RequestStatus::where('id_fab', '=', $idfab)->first();						

							// insert to status log
							$statusLog                    = new RequestStatusLog;
							$statusLog->id_request_status = $request2['id_req_status'];
							$statusLog->status_order      = $request2['status_order'];
							$statusLog->id_fab            = $idfab;
							$statusLog->start_update      = $request2['last_update'];
							$statusLog->keterangan        = $request2['keterangan'];
							$statusLog->updated_by        = Session::get('username');
							$saveLog                      = $statusLog->save();

							
							// get AM email and send notification
							$email = MasterAM::find($order['id_am']);
							$attr  = array(
									'email'     => $email->email,
									'name'      => $email->nama_lengkap,
									'speedy'    => $order['cust_id'],
									'cust_name' => $order['cust_name'],
									'ordertype' => $order['OrderType'] 
								);
						    
						    Mail::send('dashboard.closeordermail', $attr, function($message) use ($attr)
						    {
						   	
						   		$message->to($attr['email'], $attr['name'])->subject('Closing order layanan Home Automation');
						    
						    });

						    // create response log to saaslog
						    $responseLog = 'status='.$decode->OutStatus.', message='.$decode->OutMessage;
						    DB::table('saaslog')->insert(array('request'=>$requestLog, 'response'=>$responseLog, 'created_at'=>date('Y-m-d H:i:s')));

						} else {

							$responseLog = 'status='.$decode->OutStatus.', message='.$decode->OutMessage;
							DB::table('saaslog')->insert(array('request'=>$requestLog, 'response'=>$responseLog, 'created_at'=>date('Y-m-d H:i:s')));

							return Redirect::to('orderdevices/response/'.$idstatus)
										->with('message', 'Gagal close order, error '.$decode->OutMessage.' saat mengirim notifikasi');
						
						}

				} else { // close order untuk alacarte

					// get AM email and send notification
					$email = MasterAM::find($order['id_am']);
					$attr  = array(
							'email'     => $email->email,
							'name'      => $email->nama_lengkap,
							'speedy'    => $order['cust_id'],
							'cust_name' => $order['cust_name'],
							'ordertype' => $order['OrderType'] 
						);

					Mail::send('dashboard.closeordermail', $attr, function($message) use ($attr)
				    {
				   
				   		$message->to($attr['email'], $attr['name'])->subject('Closing order layanan Home Automation');
				   
				    });

				}
				
			// end of close control
			} elseif($status=='Preparation') {
				
				// insert to assign setter for assign customer visit
				$assign            = new AssignSetter;
				$assign->id_setter = Input::get('id_setter');
				$assign->invoice   = $invoice;
				$assign->status    = '0';
				$assign->save();

				// send email notification to setter
				$master = MasterSetter::select('email','setter_name')
										->where('id_setter', '=', Input::get('id_setter'))
										->first();
				$attr  = array(
						'email'     => $master['email'],
						'name'      => $master['setter_name'],
						'invoice'   => $invoice,
						'speedy'    => $order['cust_id'],
						'cust_name' => $order['cust_name'],
						'address'   => $order['alamat'] ,
						'phone'     => $order['cust_phone']
					);
			    
			    Mail::send('dashboard.setteremail', $attr, function($message) use ($attr)
			    {
			   
			   		$message->to($attr['email'], $attr['name'])->subject('Persiapan instalasi layanan Home Automation');
			   
			    });

			// end of preparation control    
			} elseif($status=="Installation Done") {
				
				AssignSetter::where('invoice','=',$invoice)->update(array('status'=>'1'));
				
				// send email to ESA about instalation done
				$attr = array(
							'invoice' => $invoice,
							'speedy'  => $order['cust_id'],
							'name'    => $order['cust_name'],
							'phone'   => $order['cust_phone']
						);
				
				Mail::send('dashboard.installationdonemail', $attr, function($message) use ($attr)
			    {
			   
			   		$message->to(array('enterprise@telkom.support','consumer@telkom.support'), 'ESA DSC')->subject('Installation done');
			   
			    });
			
			// end of Installation done control
			}

			// update status order device
			$statusOrder              = StatusOrderDevices::find($idstatus);
			$statusOrder->status      = $status;
			$statusOrder->last_update = $dateNow;
			$statusOrder->keterangan  = $keterangan;
			$statusOrder->updated_by  = Session::get('username');
			$update                   = $statusOrder->save();

			// insert to log
			$statusOrderLog               = new StatusOrderDevicesLog;
			$statusOrderLog->id_status    = $idstatus;
			$statusOrderLog->status       = $currentstatus;
			$statusOrderLog->start_update = $startDate;
			$statusOrderLog->last_update  = $dateNow;
			$statusOrderLog->keterangan   = $cur_ket;
			$statusOrderLog->updated_by   = $currentupdater;
			$statusOrderLog->invoice      = $invoice;
			$updateLog                    = $statusOrderLog->save();

			if ($update && $updateLog) {

				return Redirect::to('orderdevices')->with('message', 'Status berhasil dirubah menjadi '.$status);
			
			}	

		}	// end of response control
	
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$invoice       = $id;
		$kuota         = '';
		$ordered       = array();
		$masterDevices = MasterDevices::status()->get();
		$order         = OrderDevice::find($invoice);
		$fab           = Fab::where('TransId','=', $order['transid'])->first();
		
		if ($fab['PackageID']=='1' || $fab['PackageID']=='6' || $fab['PackageID']=='7' || $fab['PackageID']=='8') {
		
			$kuota = '3';
		
		} elseif ($fab['PackageID']=='2' || $fab['PackageID']=='9' || $fab['PackageID']=='10' || $fab['PackageID']=='11') { //Ala carte Basic - 1
		
			$kuota = '13';
		
		} elseif($fab['PackageID']=='3' || $fab['PackageID']=='12' || $fab['PackageID']=='13' || $fab['PackageID']=='14') { // Ala carte Bronze - 2
		
			$kuota = '23';
		
		} elseif($fab['PackageID']=='4' || $fab['PackageID']=='15' || $fab['PackageID']=='16' || $fab['PackageID']=='17') { // Ala carte Silver - 3
		
			$kuota = '33';
		
		} elseif($fab['PackageID']=='5' || $fab['PackageID']=='18' || $fab['PackageID']=='19' || $fab['PackageID']=='20') { // Ala carte Gold - 4
		
			$kuota = '43';
		
		}
		
		foreach ($masterDevices as $master) {
		
			$orderedDevice = OrderedDevices::where('invoice','=',$invoice)->where('id_device','=',$master->id_devices)->first();
			array_push($ordered, $orderedDevice['qty']);
		
		}

        return View::make('orderdevices.edit', array(
													'devices'       => $masterDevices,
													'order'         => $order,
													'orderedDevice' => $ordered,
													'invoice'       => $invoice,
													'kuota'         => $kuota
        											));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$invoice      = $id;
		$transtype    = Input::get('transtype');
		$dev_company  = Input::get('developer_company');
		$dev_name     = Input::get('developer_name');
		$dev_telp     = Input::get('developer_telp');
		$dev_email    = Input::get('developer_email');
		$speedy       = Input::get('speedy');
		$cust_type    = Input::get('cust_type');
		$cust_name    = Input::get('cust_name');
		$cust_phone   = Input::get('cust_phone');
		$cust_address = Input::get('alamat');
		$keterangan   = Input::get('keterangan');
		$date         = date('Y-m-d H:i:s');

		//cek kuota device dengan inputan order
		$orderCount = 0;
		$count      = MasterDevices::status()->count();

		for ($j=0; $j < $count; $j++) { 
			
			$price = MasterDevices::where('id_devices','=', Input::get('id_device_'.$j))->select('stock')->first();
				
			if (Input::get('jumlah_'.$j) > $price['stock']) {
		
				return Redirect::to('orderdevices/edit/'.$id)->with('message', 'Jumlah order perangkat '.$price['nama_device'].' yang anda masukkan lebih besar dari stock paket yang tersedia. Kurangi jumlah order perangkat')->withInput();
				break;
		
			}

			if (Input::get('jumlah_'.$j)!=='' || Input::get('jumlah_'.$j) > 0) {
		
				$orderCount += Input::get('jumlah_'.$j);
		
			}

			if(Input::get('jumlah_'.$j)=='' || Input::get('jumlah_'.$j)==0) {
		
				$temp = 0;
				$orderCount += $temp;
		
			}
		
		} //end of loop order device validation

		if ($orderCount > Input::get('kuota')) {

			return Redirect::to('orderdevices/edit/'.$id)->with('message','Jumlah device yang anda pesan melebihi jumlah kuota, periksa kembali pesanan anda');

		}

		if ($orderCount == 0) {

			return Redirect::to('orderdevices/edit/'.$id)->with('message','Jumlah device yang dipesan tidak boleh kosong semua');

		}

		// update customer data order device
		$customer = OrderDevice::where('invoice','=',$invoice)
								->update(array(
											'dev_company' => $dev_company,
											'dev_name'    => $dev_name,
											'dev_telp'    => $dev_telp,
											'dev_email'   => $dev_email,
											'cust_phone'  => $cust_phone,
											'cust_type'   => $cust_type
									));

		// loop for order device
		for ($i=1; $i <= $count; $i++) { 

			$jumlahOrder = Input::get('jumlah_'.$i);
			$idDevice    = Input::get('id_device_'.$i);
			$oldOrder    = Input::get('oldOrder_'.$i);

			$price = MasterDevices::where('id_devices','=', $idDevice)
									->select('stock')
									->first();

			$ordered = OrderedDevices::where('invoice','=',$invoice)
									 ->where('id_device','=',$idDevice)
									 ->where('edited_status','=','0')
									 ->first();
			$masterStock = 0;

			// pengurangan atau penambahan jumlah order, dan pengurangan bukan menjadi 0
			if($ordered['qty'] !== $jumlahOrder && $ordered['qty']!==NULL && $jumlahOrder !== $oldOrder && $oldOrder>0 && $jumlahOrder > 0) {
				
				$oldqty      = $ordered['qty'];
				$newQty      = $jumlahOrder;
				$delta       = $oldqty - $newQty;
				$masterStock = $price['stock'] + $delta;

				$update = OrderedDevices::where('invoice','=',$invoice)
										 ->where('id_device','=',$idDevice)
										 ->update(array('edited_status'=>'1'));
				
				$devices            = new OrderedDevices;
				$devices->invoice   = $invoice;
				$devices->id_device = $idDevice;
				$devices->qty       = $jumlahOrder;
				$devices->save();					

				$setMasterStock = MasterDevices::where('id_devices','=', $idDevice)
											   ->update(array('stock'=> $masterStock));	 

			} elseif($ordered['qty']==NULL && $jumlahOrder!=='') { //penambahan order dari 0 menjadi > 0
				
				$masterStock = $price['stock'] - $jumlahOrder;
				
				$devices            = new OrderedDevices;
				$devices->invoice   = $invoice;
				$devices->id_device = $idDevice;
				$devices->qty       = $jumlahOrder;
				$devices->save();

				$setMasterStock = MasterDevices::where('id_devices','=', $idDevice)
											   ->update(array('stock'=> $masterStock));
			
			} elseif ($oldOrder > 0 && ($jumlahOrder == '' || $jumlahOrder==0)) { // pengurangan order dari !=0 menjadi 0 
				
				$masterStock = $price['stock'] + $oldOrder;
				
				$update = OrderedDevices::where('invoice','=',$invoice)
										 ->where('id_device','=',$idDevice)
										 ->update(array('edited_status'=>'1'));
				
				$setMasterStock = MasterDevices::where('id_devices','=', $idDevice)
											   ->update(array('stock'=> $masterStock));
			}
				
		} // end of device count loop

		return Redirect::to('orderdevices')->with('message', 'data order device berhasil dirubah');
		
	}

	/**
	 * Display the specified resource.
	 * 
	 * Data: data ordered device
	 * 
	 * @param  int  $id
	 * @return Response
	 */
	public function showLog($id)
	{

		$dataLog = OrderedDevices::join('master_device', function($join) {

										$join->on('id_devices','=','id_device');
			
									})->where('ordered_device.invoice','=', $id)
									  ->get();

		return View::make('orderdevices.showLog', array('logs'=>$dataLog));
	}

	/**
	 * Display struk order
	 * @param  int $id invoice
	 * @return response
	 */
	public function showStruk($invoice)
	{
		$order       = OrderDevice::find($invoice);
		
		$devices     = OrderDevice::find($invoice)->devices;
		
		$nameDevice  = array();
		$devicePrice = array();
		$qty         = array();
		$unitPrice   = array();
		$orderTotal  = 0;
		
		foreach ($devices as $device) {
			
			$name = MasterDevices::find($device->id_device);
			
			array_push($nameDevice, $name->nama_device);
			array_push($qty, $device->qty);
			array_push($devicePrice, $device->ordered_price);
			
			$temp = $device->ordered_price * $device->qty;
			array_push($unitPrice, $temp);
			
			$orderTotal += $temp;
			
		}

		return View::make('orderdevices.struk', array(
														'order'      => $order,
														'nameDevice' => $nameDevice,
														'devicePrice'=> $devicePrice,
														'qty'        => $qty,
														'unitPrice'  => $unitPrice,
														'orderTotal' => $orderTotal
													));
	}

}
