<?php

class UserManagementController extends BaseController {

	protected $layout = 'layouts.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        
		$user  = User::all();
		$nama  = array();
		$email = array();

        foreach ($user as $users) {
        	
        	$master = MasterUsers::select('nama','email')
        						 ->where('id_user','=', $users->ID)
        						 ->first();

        	array_push($nama, $master['nama']);
        	array_push($email, $master['email']);
        
        }
        
        return View::make('users.usersindex')->with('users', $user)->with('nama', $nama)->with('email', $email);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$witel = array(
				'Bangka Belitung (Pangkal Pinang)',
				'Banten Barat (Serang)',
				'Banten Timur (Tangerang)',
				'Bengkulu',
				'Jabar Barat (Bogor)',
				'Jabar Barat Utara (Bekasi)',
				'Jabar Selatan (Sukabumi)',
				'Jabar Tengah (Bandung)',
				'Jabar Timsel (Tasikmalaya)',
				'Jabar Timur (Cirebon)',
				'Jabar Utara (Karawang)',
				'Jakarta Barat',
				'Jakarta Pusat',
				'Jakarta Selatan',
				'Jakarta Timur',
				'Jakarta Utara',
				'Jambi',
				'Lampung',
				'NAD (Aceh)',
				'Riau Daratan (Pekanbaru)',
				'Riau Kepulauan (Batam)',
				'Sumatera Barat (Padang)',
				'Sumatera Selatan (Palembang)',
				'Sumut Barat (Medan)',
				'Sumut Timur (Pematang Siantar)',
				'Bali Selatan (Denpasar)',
				'Bali Utara (Singaraja)',
				'DI Yogyakarta',
				'Jateng Barat Selatan (Purwokerto)',
				'Jateng Barat Utara (Pekalongan)',
				'Jateng Selatan (Magelang)',
				'Jateng Tengah (Salatiga)',
				'Jateng Timur Selatan (Solo)',
				'Jateng Timur Utara (Kudus)',
				'Jateng Utara (Semarang)',
				'Jatim Barat (Madiun)',
				'Jatim Selatan (Malang)',
				'Jatim Selatan Timur (Pasuruan)',
				'Jatim Suramadu (Surabaya)',
				'Jatim Tengah (Kediri)',
				'Jatim Tengah Timur (Sidoarjo)',
				'Jatim Timur (Jember)',
				'Jatim Utara (Gresik)',
				'Kalbar (Pontianak)',
				'Kalsel (Banjarmasin)',
				'Kalteng (Palangkaraya)',
				'Kaltimsel (Balikpapan)',
				'Kaltimteng (Samarinda)',
				'Kaltimut (Tarakan)',
				'Maluku (Ambon)',
				'Maluku Utara (Ternate)',
				'NTB (Mataram)',
				'NTT (Kupang)',
				'Papua (Jayapura)',
				'Papua Barat (Sorong)',
				'Sulsel (Makassar)',
				'Sulsel Barat (Pare-pare)',
				'Sulteng (Palu)',
				'Sultra (Kendari)',
				'Sulut (Menado)',
				'Sulut Selatan (Gorontalo)'
			);

        return View::make('users.usernew')->with('propinsi', $witel);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$username = Input::get('username');
		$password = Hash::make(Input::get('password'));
		$nama     = Input::get('nama');
		$email    = Input::get('email');
		$telepon  = Input::get('telepon');
		$loker    = Input::get('loker');
		$level    = Input::get('level');

		$cekUsername = User::where('username', '=', $username)->first();
		$role = Role::where('role_name','=', Input::get('level'))->first();

		if (empty($cekUsername->username)) 
		{
			$user           = new User;
			$user->username = $username;
			$user->password = $password;
			$user->LEVEL    = $level;
			$user->role     = $role['id'];
			$store          = $user->save();

			if ($store) {
				
				$master             = new MasterUsers;
				$master->id_user    = $user->ID;
				$master->nama       = $nama;
				$master->email      = $email;
				$master->telepon    = $telepon;
				$master->loker      = $loker;
				$master->updated_by = Session::get('username');
				$save               = $master->save();
				
				if($store) {

					return Redirect::to('users')->with('message', 'akun '.$username.' berhasil ditambahkan');
				
				} else {
				
					User::destroy($user->ID);
				
					return Redirect::to('users/new')->with('message', 'terjadi kesalahan sistem, akun gagal ditambahkan');
				
				}
				
			
			} else {
				
				return Redirect::to('users/new')->with('message', 'terjadi kesalahan sistem, akun gagal ditambahkan');
			
			}

		} else {
			
			return Redirect::to('users/new')->with('message', 'username '.$username.' sudah ada. Silahkan gunakan username yang lain.');
		
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('usermanagements.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user   = User::find($id);
		$master = MasterUsers::where('id_user', '=', $user->ID)->first();
		$witel  = array(
				'Bangka Belitung (Pangkal Pinang)',
				'Banten Barat (Serang)',
				'Banten Timur (Tangerang)',
				'Bengkulu',
				'Jabar Barat (Bogor)',
				'Jabar Barat Utara (Bekasi)',
				'Jabar Selatan (Sukabumi)',
				'Jabar Tengah (Bandung)',
				'Jabar Timsel (Tasikmalaya)',
				'Jabar Timur (Cirebon)',
				'Jabar Utara (Karawang)',
				'Jakarta Barat',
				'Jakarta Pusat',
				'Jakarta Selatan',
				'Jakarta Timur',
				'Jakarta Utara',
				'Jambi',
				'Lampung',
				'NAD (Aceh)',
				'Riau Daratan (Pekanbaru)',
				'Riau Kepulauan (Batam)',
				'Sumatera Barat (Padang)',
				'Sumatera Selatan (Palembang)',
				'Sumut Barat (Medan)',
				'Sumut Timur (Pematang Siantar)',
				'Bali Selatan (Denpasar)',
				'Bali Utara (Singaraja)',
				'DI Yogyakarta',
				'Jateng Barat Selatan (Purwokerto)',
				'Jateng Barat Utara (Pekalongan)',
				'Jateng Selatan (Magelang)',
				'Jateng Tengah (Salatiga)',
				'Jateng Timur Selatan (Solo)',
				'Jateng Timur Utara (Kudus)',
				'Jateng Utara (Semarang)',
				'Jatim Barat (Madiun)',
				'Jatim Selatan (Malang)',
				'Jatim Selatan Timur (Pasuruan)',
				'Jatim Suramadu (Surabaya)',
				'Jatim Tengah (Kediri)',
				'Jatim Tengah Timur (Sidoarjo)',
				'Jatim Timur (Jember)',
				'Jatim Utara (Gresik)',
				'Kalbar (Pontianak)',
				'Kalsel (Banjarmasin)',
				'Kalteng (Palangkaraya)',
				'Kaltimsel (Balikpapan)',
				'Kaltimteng (Samarinda)',
				'Kaltimut (Tarakan)',
				'Maluku (Ambon)',
				'Maluku Utara (Ternate)',
				'NTB (Mataram)',
				'NTT (Kupang)',
				'Papua (Jayapura)',
				'Papua Barat (Sorong)',
				'Sulsel (Makassar)',
				'Sulsel Barat (Pare-pare)',
				'Sulteng (Palu)',
				'Sultra (Kendari)',
				'Sulut (Menado)',
				'Sulut Selatan (Gorontalo)'
			);

        return View::make('users.usersedit')->with('user', $user)->with('master', $master)->with('propinsi', $witel);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$user    = User::find($id);
		
		$nama    = Input::get('nama');
		$email   = Input::get('email');
		$telepon = Input::get('telepon');
		$loker   = Input::get('loker');

		Input::flash();

		// cek master table if masterid exist, then update. if not, insert new
		if(Input::has('masterid')) {
			
			$master             = MasterUsers::find(Input::get('masterid'));
			$master->nama       = $nama;
			$master->email      = $email;
			$master->telepon    = $telepon;
			$master->loker      = $loker;
			$master->updated_by = Session::get('username');
			$insert             = $master->save();
		
		} else {
		
			$master             = new MasterUsers;
			$master->id_user    = $id;
			$master->nama       = $nama;
			$master->email      = $email;
			$master->telepon    = $telepon;
			$master->loker      = $loker;
			$master->updated_by = Session::get('username');
			$insert             = $master->save();
		
		}

		if (Input::get('password')=='') {
			
			$role        = Role::where('role_name','=', Input::get('level'))->first();
			$user->LEVEL = Input::get('level');
			$user->role  = $role['id'];
			$store       = $user->save();
			
			if ($store) {

				return Redirect::to('users')->with('message', 'data '.$user->username.' telah dirubah');
			
			}
			
		} else {

			$role           = Role::where('role_name','=', Input::get('level'))->first();
			$password       = Hash::make(Input::get('password'));
			$user->password = $password;
			$user->LEVEL    = Input::get('level');
			$user->role 	= $role['id'];
			$store          = $user->save();

			if ($store) {

				return Redirect::to('users')->with('message', 'data '.$user->username.' telah dirubah');
			
			} else {
			
				return Redirect::to('users/edit/'.$id)->with('message', 'Terjadi kesalahan sistem. Data '.$user->username.' gagal dirubah');
			
			}
		
		}	
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		$master = MasterUsers::where('id_user', '=', $id)->delete();
		$user   = User::destroy($id);
		
		return Redirect::to('users')->with('message', 'Data telah dihapus');
	
	}

	/**
	 * show all resource of role
	 * @return response
	 */
	public function privilege()
	{
		$role = Role::orderBy('role_name')->get();
		
		return View::make('users.privilege', array('role'=>$role));
	}

	/**
	 * show form set privileges
	 * @param int $id id_role
	 */
	public function setPrivilege($id)
	{
		$privileges = Privileges::join('module', function($join)
							{
							
								$join->on('id_module','=', 'module.id');
							
							})->where('id_role','=', $id)
							  ->get();
		
		$temp    = 0;
		$arrData = array();

		foreach ($privileges as $item) {
		
			array_push($arrData, $item->id_module);
			$temp++;
		
		}					  

		$role   = Role::find($id);
		$module = Module::where('module_parent','=','0')
						->orWhere('module_parent','=',null)
						->get();					  

		return View::make('users.setprivilege', array(
													'privileges' => $arrData,
													'role'       => $role,
													'module'     => $module
													));
	}

	/**
	 * store data update privilege
	 * @param  int $id from id_role
	 * @return response     back to privilege page
	 */
	public function updatePrivilege($id)
	{

		$module = Module::where('module_parent','=','0')
						->orWhere('module_parent','=',null)
						->get();
		
		foreach ($module as $item) {

			if(Input::has('cek_'.$item->id) && Input::get('module_'.$item->id)=='0') {
				
				//cek apakah module memiliki child
				$parent = $item->module_parent;
				
				if($parent == '0') {
				
					// masukkan child ke dalam privileges sesuai role
					$child = Module::where('module_parent','=', $item->id)->get();
				
					foreach ($child as $node) {
						
						$updatePrivilege = Privileges::create(array('id_role'=>$id,'id_module'=>$node->id));
				
					}
				
				}

				// masukkan module ini ke dalam privilege sesuai role
				$updatePrivilege = Privileges::create(array('id_role'=>$id,'id_module'=>$item->id));

			} elseif(!Input::has('cek_'.$item->id) && Input::get('module_'.$item->id)=='1') {
				
				//cek apakah module memiliki child
				$parent = $item->module_parent;
				
				if($parent == '0') {
				
					// masukkan child ke dalam privileges sesuai role
					$child = Module::where('module_parent','=', $item->id)->get();
				
					foreach ($child as $node) {
						
						// hilangkan privilege modul ini untuk role
						$updatePrivilege = Privileges::where('id_role','=',$id)
													 ->where('id_module','=',$node->id)
													 ->delete();
					}
				
				}

				// hilangkan privilege modul ini untuk role
				$updatePrivilege = Privileges::where('id_role','=',$id)
											 ->where('id_module','=',$item->id)
											 ->delete();
			}
		
		} // end foreach modules 
		
		return Redirect::to('users/privilege')->with('message', 'Privilege telah diperbarui');
	}

}
