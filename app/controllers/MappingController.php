<?php

class MappingController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$dataFab = Fab::join('request_status', function($join)
						{
						
							$join->on('FAB.AccountID', '=', 'request_status.accountid');
						
						})
						->where('request_status.TransType', '=', 'ACTIVATION')
						->where('status_order', '=', 'Proses Order Device')
						->get();
						
		$exist = array();

		// cek apakah user telah ter-mapping
		foreach ($dataFab as $user) {

			$pelanggan = Pelanggan::select('userid')->where('AccountID','=', $user->AccountID)->first();
			
			if ($pelanggan['userid']!==NULL) {
		
				$status = '1'; // berarti mapping done
		
			} else {
		
				$status = '0'; // bisa start mapping
		
			}
		
			array_push($exist, $status);	
		
		}							

        return View::make('mappings.index', array('fabs'=> $dataFab, 'exist' => $exist));
	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($fabid, $reqid)
	{
		$data   = Fab::find($fabid);
		$order  = OrderDevice::select('cust_phone','invoice')->where('cust_id','=',$data->AccountID)->first();
		$assign = AssignSetter::select('id_setter', 'id')->where('invoice', '=', $order['invoice'])->first();

        return View::make('mappings.create', array(
												'data'     => $data, 
												'reqid'    => $reqid, 
												'idsetter' => $assign['id_setter'],
												'idassign' => $assign['id'], 
												'phone'    => $order['cust_phone']
											));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$reqid     = Input::get('reqid');
		$fabid     = Input::get('fabid');
		$transid   = Input::get('transid');
		$accountid = Input::get('accountid');
		$devemail  = Input::get('devemail');
		$package   = Input::get('package');
		$telkomid  = Input::get('telkomid');
		$gateway   = Input::get('macaddress');
		$address   = Input::get('alamat');
		$telp      = Input::get('telpon');
		$facebook  = Input::get('facebook');
		$twitter   = Input::get('twitter');
		$idSetter  = Input::get('idsetter');
		$idassign  = Input::get('idassign');
		$datetime  = date('Y-m-d H:i:s');

		Input::flash();
		
		// check installed gateway
		$checkExist = Gwaddress::where('gwaddress', '=', $gateway)->first();
		
		if (empty($checkExist['gwid'])) {

			return Redirect::to('mapping/new/'.$fabid.'/'.$reqid)
							->with('message', 'MAC Address '.$gateway.' yang anda masukkan salah atau tidak ditemukan di database.')
							->withInput();
		
		} 

		// if gateway exist, check at existing user
		else 
		{

			$checkInstalledUser = Pelanggan::where('gwid', '=', $checkExist['gwid'])->first();

			if (empty($checkInstalledUser['gwid'])) {

				// check existing username / email / telkomid
				$checkExistUsername = Pelanggan::where('username', '=', $telkomid)->first();

				if (empty($checkExistUsername['username'])) {
				
					// store data
					$plainPassword       = substr($gateway, -6);
					
					$customer            = new Pelanggan;
					$customer->username  = $telkomid;
					$customer->password  = md5($plainPassword);
					$customer->gwid      = $checkExist['gwid'];
					$customer->enabled   = '1';
					$customer->lastupd   = $datetime;
					$customer->active    = '0';
					$customer->id_pakage = $package;
					$customer->phone     = $telp;
					$customer->AccountID = $accountid;
					$customer->alamat    = $address;
					$customer->facebook  = $facebook;
					$customer->twitter   = $twitter;
					$customer->updatedBy = Session::get('username');
					$store               = $customer->save();

					$setterActivity                = new SetterActivity;
					$setterActivity->id_setter     = $idSetter;
					$setterActivity->id_req_status = $reqid;
					$setterActivity->id_user       = $customer->userid;
					$setterActivity->gwid          = $checkExist['gwid'];
					$setterActivity->last_update   = $datetime;
					$storeActivity                 = $setterActivity->save();

					if ($store && $storeActivity) {

						//send email ke pelanggan untuk informasi akun login
						$data = array('username'=>$telkomid, 'password'=>$plainPassword);
						$send = Mail::later(30, 'dashboard.pelangganmail', $data, function($message) use ($data)
							   {
							   
							   		$message->to($data['username'], 'User')->subject('Informasi account Home Automation');
							   
							   });


						return Redirect::to('mapping')
										->with('message', 'Data user untuk pelanggan baru sudah ditambahkan');
					
					}

				} else {
					
					return Redirect::to('mapping/new/'.$fabid.'/'.$reqid)
							->with('message', 'Email '.$telkomid.' yang anda masukkan sudah dipakai oleh pengguna lain.')
							->withInput();
				
				} // -----------------  end of check existing username -------------------------//
				
			} else {
				
				return Redirect::to('mapping/new/'.$fabid.'/'.$reqid)
							->with('message', 'MAC Address '.$gateway.' yang anda masukkan sudah dipakai oleh pengguna lain.')
							->withInput();

			} // --------------------  End of existing gateway used by user ---------------------------//


		} // --------------------------- end of gateway checking ---------------------------------------------------//

	}

}
