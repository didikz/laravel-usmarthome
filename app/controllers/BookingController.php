<?php

class BookingController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		$bookings = BookingOrder::where('id_am','=',Session::get('idam'))
								  ->orderBy('created_at', 'DESC')
								  ->get();
		
		$sum = array();
		
		foreach ($bookings as $booking) {
	  	
	  		$qty = DB::table('booking_device')
	  				 ->where('id_order','=',$booking->id)
	  				 ->sum('qty');
	  		
	  		array_push($sum, $qty);
	  	
	  	}						  
        
        return View::make('bookings.index', array('bookings'=>$bookings,'sum'=>$sum));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$devices = MasterDevices::status()->get();
        
        return View::make('bookings.create', array('devices'=>$devices));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$accountid = Input::get('accountid');
		$custname  = Input::get('custname');
		$ordertype = Input::get('ordertype');
		
		Input::flash();
		
		$count = Input::get('orderedCount');
		
		$BookingOrder                = new BookingOrder;
		$BookingOrder->id_am         = Session::get('idam');
		$BookingOrder->AccountID     = $accountid;
		$BookingOrder->customer_name = $custname;
		$BookingOrder->ordertype 	 = $ordertype;
		$store                       = $BookingOrder->save();
		$id_order                    = $BookingOrder->id;

		if ($store) {
			
			for ($i=1; $i <= $count; $i++) { 

				$idDevice  = Input::get('id_device_'.$i);
				$qtyDevice = Input::get('jumlah_'.$i);
				$harga = Input::get('harga_'.$i);
				
				if ($qtyDevice!=='' || $qtyDevice > 0) {
					
					// update current device stock
					$master        = MasterDevices::find($idDevice);
					$masterStock   = $master['stock'] - $qtyDevice;
					
					MasterDevices::where('id_devices','=',$idDevice)
								 ->update(array('stock'=>$masterStock));

					$BookingDevice            = new BookingDevices;
					$BookingDevice->id_order  = $id_order;
					$BookingDevice->id_device = $idDevice;
					$BookingDevice->qty       = $qtyDevice;
					$BookingDevice->harga_booking = $harga;
					$BookingDevice->save();
					
				}
			}
	
			return Redirect::to('bookingorder')->with('message', 'Booking order untuk pelanggan '.$custname.' berhasil dilakukan');
	
		} else {
	
			return Redirect::to('bookingorder/new')->with('message', 'Data gagal masuk database, error sistem');
	
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$bookingorder  = BookingOrder::find($id);
		
		$bookingdevice = BookingDevices::join('master_device', function($join) 
										{
										
											$join->on('id_devices','=', 'id_device');
										
										})->where('id_order','=',$id)
										  ->get();
    
        return View::make('bookings.show', array('booking'=>$bookingorder, 'devices'=>$bookingdevice));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$bookings     = BookingOrder::where('id', '=',$id)->where('status','=','0')->first();
		$devices      = MasterDevices::status()->get();
		
		$qty          = array();
		$bookedDevice = array();

		foreach ($devices as $device) {
	
			$temp = BookingDevices::where('id_order','=',$bookings->id)
									->where('id_device','=', $device->id_devices)
									->first();
			
			array_push($qty, $temp['qty']);
			array_push($bookedDevice, $temp['id']);
	
		}
        
        return View::make('bookings.edit', array(
											'bookings'     => $bookings, 
											'devices'      => $devices,
											'bookedDevice' => $bookedDevice,
											'qty'          => $qty
        									));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		$accountid    = Input::get('accountid');
		$custname     = Input::get('custname');
		$ordertype 	  = Input::get('ordertype');
		
		$cekFab = Fab::select('AccountID')
					 ->where('AccountID','=',$accountid)
					 ->where('TransType','=',strtoupper($ordertype))
					 ->first();

		$cekBooking = BookingOrder::select('AccountID')
								  ->where('AccountID','=',$accountid)
								  ->where('OrderType','=',$ordertype)
								  ->where('status','=','0')
								  ->first();							
		
		if ($cekFab['AccountID']!==NULL && $accountid!=='') {

			return Redirect::to('bookingorder/edit/'.$id)
							->with('message', 'Data gagal diperbarui. No. Speedy '.$accountid.' sudah dalam masa layanan Home Automation. Periksa kembali no. Speedy anda.');
		
		} elseif($cekBooking['AccountID'] !==NULL && $accountid!=='') {

			return Redirect::to('bookingorder/edit/'.$id)
							->with('message', 'Data gagal diperbarui. No. Speedy '.$accountid.' sudah dalam masa booking. Periksa kembali no. Speedy anda.');
		
		} else {

			// cek apakah jumlah yang dimasukkan tidak melebihi jumlah stock
			$count    = MasterDevices::status()->count();

			for ($i=1; $i <= $count; $i++) {

				if (Input::get('jumlah_'.$i)!=='' || Input::get('jumlah_'.$i) > 0) {

					if (Input::get('jumlah_'.$i) > Input::get('stock_'.$i)) {	

						return Redirect::to('bookingorder/edit/'.$id)->with('message', 'Jumlah order perangkat yang anda masukkan lebih besar dari stock perangkat yang tersedia. Kurangi jumlah order perangkat')->withInput();
						break;

					} 

				}

			}

			// update booking order data
			$bookingOrder = BookingOrder::where('id','=',$id)
										->update(array(
														'AccountID'     => $accountid,
														'customer_name' => $custname,
														'OrderType'		=> $ordertype
												));
				
			for ($i=1; $i <= $count; $i++) { 

				$idDevice  = Input::get('id_device_'.$i);
				$qtyDevice = Input::get('jumlah_'.$i);
				$idBooked  = Input::get('booked_'.$i);
				$harga     = Input::get('harga_'.$i);
				
				if ($qtyDevice !== '' || $qtyDevice > 0 && $idBooked !=='') {
					
					$cek = BookingDevices::where('id_device','=',$idDevice)
										 ->where('id_order','=',$id)
										 ->first();
	
					if ($cek['id_device']==NULL) {
						
						$BookingDevices                = new BookingDevices;
						$BookingDevices->id_order      = $id;
						$BookingDevices->id_device     = $idDevice;
						$BookingDevices->qty           = $qtyDevice;
						$BookingDevices->harga_booking = $harga;
						$BookingDevices->save();
						
						// update current device stock
						$master        = MasterDevices::find($idDevice);
						$master->stock = $master->stock - $qtyDevice;
						$master->save();

					} else {
						
						$getQty     = BookingDevices::where('id','=',$idBooked)
													->first();
						$bookingQty = $getQty['qty'];
						$newQty     = $qtyDevice;
						$stock      = $bookingQty - $newQty;

						// update current device stock
						$master        = MasterDevices::find($idDevice);
						$master->stock = $master->stock + $stock;
						$master->save();
	
						$BookingDevices = BookingDevices::where('id_order','=', $id)
														->where('id_device','=', $idDevice)
														->update(array('qty' => $qtyDevice));

					}
					
					
				} elseif ($qtyDevice == '' || $qtyDevice == 0 && $idBooked !== '') {
					
					$getQty = BookingDevices::where('id','=',$idBooked)
											->first();
					
					// update current device stock
					$master        = MasterDevices::find($idDevice);
					$master->stock = $master->stock + $getQty['qty'];
					$master->save();
					
					// delete booking device
					$BookingDevices = BookingDevices::where('id','=',$idBooked)
													->delete();

				} 


			}

			return Redirect::to('bookingorder')->with('message', 'data berhasil diperbarui');

		}
										
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		// update master device stock first
		$getQty = BookingDevices::select('id_device','qty')->where('id_order','=',$id)->get();

		foreach ($getQty as $qty) {
			
			$master        = MasterDevices::find($qty->id_device);
			$master->stock = $master->stock + $qty->qty;
			$master->save();
		
		}

		// delete data
		BookingOrder::where('id','=',$id)->delete();
		BookingDevices::where('id_order','=',$id)->delete();

		return Redirect::to('bookingorder')->with('message', 'Data berhasil dihapus');
	}

	/**
	 *  show confirm page before submit order booking
	 *  @return Response
	 */
	public function confirm()
	{
		$data['ordertype'] = Input::get('ordertype');
		$data['accountid'] = Input::get('accountid');
		$data['custname'] = Input::get('custname');
		$data['title'] = 'Confirm Booking';

		$count = MasterDevices::status()->count();
		$orderedCount = 0;

		$cekFab = Fab::select('AccountID')
					 ->where('AccountID','=',$data['accountid'])
					 ->where('TransType','=',strtoupper($data['ordertype']))
					 ->first();

		$cekBooking = BookingOrder::select('AccountID')
					   			  ->where('AccountID','=',$data['accountid'])
								  ->where('OrderType','=',$data['ordertype'])
								  ->where('status','=','0')
								  ->first();
		
		if (($cekFab['AccountID']!==NULL || $cekFab['AccountID']!=='') && $data['accountid']!=='') {
		
			return Redirect::to('bookingorder/new')
							->with('message', 'Terjadi kesalahan. No. Speedy '.$data['accountid'].' sudah digunakan. Periksa kembali no. Speedy anda.')->withInput();
		
		} elseif(($cekBooking['AccountID']!==NULL || $cekBooking['AccountID']!=='') && $data['accountid']!=='') {
		
			return Redirect::to('bookingorder/new')
							->with('message', 'Terjadi kesalahan. No. Speedy '.$data['accountid'].' sudah dalam masa booking. Periksa kembali no. Speedy anda.')->withInput();
		
		} else {

			// cek apakah jumlah yang dimasukkan tidak melebihi jumlah stock
			for ($i=1; $i <= $count; $i++) {
		
				if (Input::get('jumlah_'.$i)!=='' || Input::get('jumlah_'.$i) > 0) {
		
					if (Input::get('jumlah_'.$i) > Input::get('stock_'.$i)) {	
		
						return Redirect::to('bookingorder/new')->with('message', 'Jumlah order perangkat yang anda masukkan lebih besar dari stock perangkat yang tersedia. Kurangi jumlah order perangkat')->withInput();
						break;
		
					} 
				}
			}

			$temp = 1;
			for ($i=1; $i <= $count ; $i++) { 

				$idDevice = Input::get('id_device_'.$i);
				$qtyDevice = Input::get('jumlah_'.$i);
				$stock = Input::get('stock_'.$i);

				if ($qtyDevice!=='' || $qtyDevice > 0) {

					$data['id_device'][$temp] = $idDevice;
					$data['qty_device'][$temp] = $qtyDevice;
					$data['stock_device'][$temp] = $stock;

					$master = MasterDevices::find($idDevice);
					$data['nama_device'][$temp] = $master->nama_device;
					$data['harga_device'][$temp] = $master->harga;
				
					$orderedCount ++;
					$temp++;

				}

			}

			$data['orderedCount'] = $orderedCount;

			return View::make('bookings.confirm', array('data' => $data));
		}
		
	}

}
