<?php

class OrderManagementController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($order)
	{
		if ($order!=='aktivasi' && $order!=='modifikasi') {

			return App::abort(404, 'page '.$order.' not found');
		
		} else {

			if (Session::get('role')=='AM' && ($order=='aktivasi' || $order=='modifikasi')) {
				
				return View::make('ordermanagements.search')->with('order', $order);
				
			} else {

				$packageName = array();
				$durasi      = array();
				$statusOrder = array();
				$idRequest   = array();
				$curStat     = array();
				$orderResult = '';
				$TransType = '';

		        if($order == 'aktivasi') {

					$dataFab     = Fablog::Activationfab()->get();
					$orderResult = 'Activation Success';
					$TransType   = 'ACTIVATION';

				} elseif($order == 'modifikasi') {
					
					$dataFab     = Fablog::Modificationfab()->get();
					$orderResult = 'Modification Success';
					$TransType   = 'MODIFICATION';

				}	
				
				foreach ($dataFab as $data) {
					
					if($data->PackageID=='1'){
					
						$package = 'PSB';
					
					} elseif ($data->PackageID=='2') {
					
						$package = 'Ala Carte (Basic-1)';
					
					} elseif ($data->PackageID=='3') {
					
						$package = 'Ala Carte (Bronze-2)';
					
					} elseif ($data->PackageID=='4') {
					
						$package = 'Ala Carte (SILVER-3)';
					
					} elseif ($data->PackageID=='5') {
					
						$package = 'Ala Carte (GOLD-4)';
					
					} elseif ($data->PackageID=='6') {
					
						$package = 'PSB - Online Bronze';
					
					} elseif ($data->PackageID=='7') {
					
						$package = 'PSB - Online Silver';
					
					} elseif ($data->PackageID=='8') {
					
						$package = 'PSB - Online Gold';
					
					} elseif ($data->PackageID=='9') {
					
						$package = 'Lain-lain';
					
					}
					
					array_push($packageName, $package);

					$statusInstal = RequestStatus::join('FABlog', function($join)
											{
												$join->on('FABlog.AccountID', '=', 'request_status.accountid');
													
											})->where('request_status.TransType','=',$TransType)
											  ->where('request_status.accountid', '=', $data->AccountID)
											  ->where('result', 'LIKE', $orderResult.'%')
											  ->select('status_order', 'id_req_status','id_fab')
											  ->first();

					
					array_push($statusOrder, $statusInstal['status_order']);
					array_push($idRequest, $statusInstal['id_req_status']);
						
					// calculate order duration
					$requestDate = new DateTime($data->lastUpdt);
					$nowDate     = new DateTime(date("Y-m-d H:i:s"));
					$selisih     = $requestDate->diff($nowDate);
		            array_push($durasi, $selisih->d.' hari, '.$selisih->h.' jam, '.$selisih->i.' menit');

		            // status current FAB
		            $currentStatus = Fab::select('Status')->where('AccountID', '=', $data->AccountID)->first();
		            
		            if($currentStatus['Status']=='') {
		            
		            	$statusFAB = 'Terminated';
		            
		            } else {
		            
		            	$statusFAB = $currentStatus['Status'];
		            
		            }

		            array_push($curStat, $statusFAB);
		        
		        }   // end foreach 

					return View::make('ordermanagements.index', array(
																	'order'       => $order,
																	'dataFab'     => $dataFab,
																	'packageName' => $packageName,
																	'durasi'      => $durasi,
																	'statusOrder' => $statusOrder,
																	'idRequest'   => $idRequest,
																	'currentStatus'=> $curStat
																	
																));
			}
		}
		

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function createStatus($type, $idReqStatus)
	{

        $data = Fab::join('request_status', function($join)
        				{
						
							$join->on('id_fab', '=', 'id');
						
						})
						->where('id_req_status', '=', $idReqStatus)
						->first();
						
        return View::make('ordermanagements.orderstatus', array('data' => $data, 'type' => $type));
        								
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storeStatus($type)
	{
		$status    = Input::get('statusorder');
		$reqid     = Input::get('idreqstatus');
		$accountid = Input::get('accountid');
		$datetime  = date('Y-m-d H:i:s');
		
		if(Input::has('keterangan')) 
		{
			$keterangan = Input::get('keterangan');
		} else {
			$keterangan = NULL;
		}

		/*
		* insert to table request status log
		*/ 
		$statusOld = RequestStatus::where('id_req_status','=',$reqid)->first();
		$fab = Fab::find($statusOld->id_fab);

		if ($status == 'Closed')
		{
			// do something to close

		} else { // insert to log and update status when status not 'Closed'

			$statusLog                    = new RequestStatusLog;
			$statusLog->id_request_status = $reqid;
			$statusLog->status_order      = $statusOld->status_order;
			$statusLog->id_fab            = $statusOld->id_fab;
			$statusLog->last_update       = $datetime;
			$statusLog->start_update      = $statusOld->last_update;
			$statusLog->keterangan        = $statusOld->keterangan;
			$statusLog->updated_by        = Session::get('username');
			$save                         = $statusLog->save();
			
			$currentStatus               = RequestStatus::find($reqid);
			$currentStatus->status_order = $status;
			$currentStatus->last_update  = $datetime;
			$currentStatus->keterangan   = $keterangan;
			$currentStatus->updated_by   = Session::get('username');
			$update                      = $currentStatus->save();

			if ($save && $update)
			{
				return Redirect::to('ordermanagement/'.$type)
								->with('message', 'Status Account id '.$accountid.' telah dirubah menjadi '.$status);
			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($type, $fabid)
	{
		
		$fab       = Fablog::find($fabid);
		
		$status    = RequestStatus::where('id_fab', '=', $fabid)->first();
		
		$statusLog = RequestStatusLog::where('id_fab', '=', $fabid)->get();
		
		$package   = '';

		if($fab['PackageID']=='1'){
			
			$package = 'PSB';
		
		} elseif ($fab['PackageID']=='2') {
		
			$package = 'Ala Carte (Basic-1)';
		
		} elseif ($fab['PackageID']=='3') {
		
			$packageName = 'Ala Carte (Bronze-2)';
		
		} elseif ($fab['PackageID']=='4') {
		
			$package = 'Ala Carte (SILVER-3)';
		
		} elseif ($fab['PackageID']=='5') {
		
			$package = 'Ala Carte (GOLD-4)';
		
		} elseif ($fab['PackageID']=='6') {
		
			$package = 'PSB - Online Bronze';
		
		} elseif ($fab['PackageID']=='7') {
		
			$package = 'PSB - Online Silver';
		
		} elseif ($fab['PackageID']=='8') {
		
			$package = 'PSB - Online Gold';
		
		} elseif ($fab['PackageID']=='9') {
		
			$package = 'Lain-lain';
		
		}

		// status current FAB
        $fabstatus = Fab::select('Status')->where('AccountID', '=', $fab->AccountID)->first();
        
        if($fabstatus['Status']=='') {
        
        	$statusFAB = 'Terminated';
        
        } else {
        
        	$statusFAB = $fabstatus['Status'];
        
        }

        return View::make('ordermanagements.detailorder', array(
															
															'fab'       => $fab,
															'paket'     => $package,
															'status'    => $status,
															'fabstatus' => $statusFAB,
															'statusLog' => $statusLog
        												
        												));
	}

	public function searchNonAlacarte()
	{
		
		$accountid = Input::get('accountid');
		$order     = Request::segment(2);
		$transtype = '';
		$status    = '';

		Input::flash();

		if ($order == 'aktivasi') {

			$transtype = 'ACTIVATION';
			$status    = 'Instalasi';
			$data      = Fab::where('AccountID', '=', $accountid)
							->where('TransType', '=', $transtype)
							->where('Status', '=', $status)->first();
		
		} elseif($order == 'modifikasi') {
		
			$transtype = 'MODIFICATION';
			$status    = 'Aktif';
			$data      = Fab::where('AccountID', '=', $accountid)
							->where('TransType', '=', $transtype)
							->where('Status', '=', $status)->first();
		
		} 

		if ($data['AccountID']==NULL) {

			return Redirect::to('ordermanagement/'.$order)->with('message', 'No speedy '.$accountid.' tidak ditemukan.')->withInput();
		
		} else {

			// check accountid di order device
			$order = OrderDevice::where('cust_id', '=', $accountid)->first();

			if($order['invoice']!==NULL && $order=='aktivasi') {

				return Redirect::to('ordermanagement/aktivasi')->with('message', 'No Speedy '.$accountid.' sedang dalam proses order device')->withInput();
			
			} else {

				return View::make('ordermanagements.resultsearch')->with('data', $data);
			}
			
			
		}
	}

	public function searchAlacarte()
	{

		$accountid   = Input::get('accountid');
		$packageName = array();
		$kuota       = array();
		$deviceCount = array();
		$quota       = '';

		$dataFab     = Fab::where('Status', '=', 'Aktif')
							->where('AccountID','=',$accountid)
							->get();
		
		if(count($dataFab)==0) {

			return Redirect::to('ordermanagement/modifikasi')->with('message', 'Nomor Speedy '.$accountid.' tidak ditemukan');

		} else {

			foreach ($dataFab as $data) {
					
				if($data->PackageID=='1'){

					$package = 'PSB';
					$quota   = 3;

				} elseif ($data->PackageID=='2') {

					$package = 'Ala Carte (Basic-1)';
					$quota   = 13;

				} elseif ($data->PackageID=='3') {

					$package = 'Ala Carte (Bronze-2)';
					$quota   = 23;

				} elseif ($data->PackageID=='4') {
					
					$package = 'Ala Carte (SILVER-3)';
					$quota   = 33;
				
				} elseif ($data->PackageID=='5') {
				
					$package = 'Ala Carte (GOLD-4)';
					$quota   = 43;
				
				} elseif ($data->PackageID=='6') {
				
					$package = 'PSB - Online Bronze';
					$quota   = 3;
				
				} elseif ($data->PackageID=='7') {
				
					$package = 'PSB - Online Silver';
					$quota   = 3;
				
				} elseif ($data->PackageID=='8') {
				
					$package = 'PSB - Online Gold';
					$quota   = 3;
				
				} elseif ($data->PackageID=='9') {
				
					$package = 'Lain-lain';
				
				}

				array_push($packageName, $package);
				
				$totalDevice = OrderedDevices::join('order_device', function($join)
											{
											
												$join->on('order_device.invoice','=','ordered_device.invoice');
											
											})->select(DB::raw('SUM(qty) as jumlah'))
											  ->where('order_device.cust_id','=',$data->AccountID)
											  ->first();

				$delta = $quota - $totalDevice['jumlah'];
				
				array_push($deviceCount, $totalDevice['jumlah']);
				array_push($kuota, $delta);							  
			}			

			return View::make('ordermanagements.alacarteindex', array(
																	'dataFab'     => $dataFab, 
																	'package'     =>$packageName,
																	'totalDevice' =>$deviceCount,
																	'kuota'       =>$kuota
																	)
			);
		}			

	}

}
