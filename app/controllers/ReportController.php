<?php

class ReportController extends BaseController
{

	/**
	 *  index data menu report order tahunan
	 */
	public function indexTahunan()
	{

		if(Input::has('year')) {

			$year = Input::get('year');
		
		} else {
		
			$year = date('Y');
		
		}
		
		$month        = array('01','02','03','04','05','06','07','08','09','10','11','12');
		$activation   = array();
		$modification = array();
		$resumption   = array();
		$suspension   = array();
		$termination  = array();
		$monthly      = array();
		$monthlyCount = 0;
		
		for ($i=0; $i < 12; $i++) { 
			
			// activation report
			$activationLog = Fablog::YearlyOrder('ACTIVATION', $month[$i], $year)->count();
			array_push($activation, $activationLog);

			// modification report
			$modificationLog = Fablog::YearlyOrder('MODIFICATION', $month[$i], $year)->count();
			array_push($modification, $modificationLog);

			// suspension report
			$suspensionLog = Fablog::YearlyOrder('SUSPENSION', $month[$i], $year)->count();
			array_push($suspension, $suspensionLog);

			// Resumption report
			$resumptionLog = Fablog::YearlyOrder('RESUMPTION', $month[$i], $year)->count();
			array_push($resumption, $resumptionLog);

			// termination report
			$terminationLog = Fablog::YearlyOrder('TERMINATION', $month[$i], $year)->count();
			array_push($termination, $terminationLog);

			$monthlyCount = ($activationLog + $modificationLog + $suspensionLog + $resumptionLog + $terminationLog);
			array_push($monthly, $monthlyCount);
			$monthlyCount = 0;

		}

		// dynamic year for filter
		$dynYear  = array();
		$initYear = 2012;
		$dif      = date('Y') - $initYear;
		
		for ($y=0; $y < $dif ; $y++) { 

			$initYear += 1;
			array_push($dynYear, $initYear); 
		
		}
		
		if(Input::has('download')) {

			if (Input::has('year')) {

				$yearReport = Input::get('year');	
			
			} else {
			
				$yearReport = $year;
			
			}
			
			$reportTitle = 'Rekapitulasi order tahun '.$yearReport;

			$output = View::make('reportgenerated.tahunan', array(

																'activation'   => $activation, 
																'modification' => $modification,
																'suspension'   => $suspension,
																'resumption'   => $resumption,
																'termination'  => $termination,
																'monthly'	   => $monthly,
																'title'		   => $reportTitle
													
															));

			$headers = array(

							'Pragma'                    => 'public',
							'Expires'                   => 'public',
							'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
							'Cache-Control'             => 'private',
							'Content-Type'              => 'application/vnd.ms-excel',
							'Content-Disposition'       => 'attachment; filename=rekaporder'.$yearReport.'.xls',
							'Content-Transfer-Encoding' => ' binary'
				
					    );

			return Response::make($output, 200, $headers);
		
		}

		return View::make('reports.ordertahunan', array(
														'activation'   => $activation, 
														'modification' => $modification,
														'suspension'   => $suspension,
														'resumption'   => $resumption,
														'termination'  => $termination,
														'monthly'	   => $monthly,
														'month'		   => $month,
														'year'		   => $year,
														'dynYear'	   => $dynYear
														
														));
	}

	/**
	 *  Index data report order bulanan
	 */
	public function indexBulanan()
	{
		if(Input::has('year') && Input::has('month')) {
			
			$paramYear    = Input::get('year');
			$paramMonth   = Input::get('month');
			$defaultMonth = '';
			$defaultYear  = '';

			if ($paramYear=='0' && $paramMonth=='0') {

				return Redirect::to('report/bulanan');
			
			} elseif($paramYear=='0' && $paramMonth!=='0') {
			
				$defaultMonth = $paramMonth;
				$defaultYear = date('Y');
			
			} elseif($paramYear!=='0' && $paramMonth=='0') {
			
				$defaultMonth = date('m');
				$defaultYear = $paramYear;
			
			} else {
			
				$defaultMonth = $paramMonth;
				$defaultYear = $paramYear;
			
			}
		
		} else {

			$defaultMonth = date('m');
			$defaultYear  = date('Y');
		
		}
		
		$defaultDays  = cal_days_in_month(CAL_GREGORIAN, $defaultMonth, $defaultYear);
		$activation   = array();
		$modification = array();
		$resumption   = array();
		$suspension   = array();
		$termination  = array();
		$daily 		  = array();
		$dailyCount   = 0;
		$temp         = '';
		$monthName    = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
		
		for ($i=1; $i <= $defaultDays; $i++) { 
			
			if ($i<10) {

				$temp = '0'.$i;

			} else {

				$temp = $i;

			}

			$activationLog = Fablog::MonthlyOrder('ACTIVATION', $temp, $defaultMonth, $defaultYear)->count();
			array_push($activation, $activationLog);

			$modificationLog = Fablog::MonthlyOrder('MODIFICATION', $temp, $defaultMonth, $defaultYear)->count();
			array_push($modification, $modificationLog);

			$suspensionLog = Fablog::MonthlyOrder('SUSPENSION', $temp, $defaultMonth, $defaultYear)->count();
			array_push($suspension, $suspensionLog);

			$resumptionLog = Fablog::MonthlyOrder('RESUMPTION', $temp, $defaultMonth, $defaultYear)->count();
			array_push($resumption, $resumptionLog);

			$terminationLog = Fablog::MonthlyOrder('TERMINATION', $temp, $defaultMonth, $defaultYear)->count();
			array_push($termination, $terminationLog);

			$dailyCount = ($activationLog + $modificationLog + $suspensionLog + $resumptionLog + $terminationLog);
			array_push($daily, $dailyCount);
			$dailyCount = 0;
		}

		// dynamic year for filter
		$dynYear  = array();
		$initYear = 2012;
		$dif      = $defaultYear - $initYear;
		
		for ($y=0; $y < $dif ; $y++) { 

			$initYear += 1;
			array_push($dynYear, $initYear); 

		}

		if (Input::has('download')) {
			
			$reportTitle = 'Rekapitulasi order bulan '.$monthName[$defaultMonth].' tahun '.$defaultYear;

			$output = View::make('reportgenerated.bulanan', array(

																'activation'   => $activation, 
																'modification' => $modification,
																'suspension'   => $suspension,
																'resumption'   => $resumption,
																'termination'  => $termination,
																'daily'        => $daily,
																'days'         => $defaultDays,
																'title'		   => $reportTitle
															
															));

			$headers = array(

							'Pragma'                    => 'public',
							'Expires'                   => 'public',
							'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
							'Cache-Control'             => 'private',
							'Content-Type'              => 'application/vnd.ms-excel',
							'Content-Disposition'       => 'attachment; filename=rekaporder'.$monthName[$defaultMonth].'tahun'.$defaultYear.'.xls',
							'Content-Transfer-Encoding' => ' binary'
				
					    );

			return Response::make($output, 200, $headers);

		}

		return View::make('reports.orderbulanan', array(
														'activation'   => $activation, 
														'modification' => $modification,
														'suspension'   => $suspension,
														'resumption'   => $resumption,
														'termination'  => $termination,
														'daily'        => $daily,
														'days'         => $defaultDays,
														'dynYear'	   => $dynYear,
														'year'		   => $defaultYear,
														'month'  	   => $defaultMonth,
														'monthName'	   => $monthName[$defaultMonth]
				));
	}

	/**
	 *  Index menu search order by witel
	 */
	public function indexWitel()
	{
		// witel data
		$witel = array(
				'Bangka Belitung (Pangkal Pinang)',
				'Banten Barat (Serang)',
				'Banten Timur (Tangerang)',
				'Bengkulu',
				'Jabar Barat (Bogor)',
				'Jabar Barat Utara (Bekasi)',
				'Jabar Selatan (Sukabumi)',
				'Jabar Tengah (Bandung)',
				'Jabar Timsel (Tasikmalaya)',
				'Jabar Timur (Cirebon)',
				'Jabar Utara (Karawang)',
				'Jakarta Barat',
				'Jakarta Pusat',
				'Jakarta Selatan',
				'Jakarta Timur',
				'Jakarta Utara',
				'Jambi',
				'Lampung',
				'NAD (Aceh)',
				'Riau Daratan (Pekanbaru)',
				'Riau Kepulauan (Batam)',
				'Sumatera Barat (Padang)',
				'Sumatera Selatan (Palembang)',
				'Sumut Barat (Medan)',
				'Sumut Timur (Pematang Siantar)',
				'Bali Selatan (Denpasar)',
				'Bali Utara (Singaraja)',
				'DI Yogyakarta',
				'Jateng Barat Selatan (Purwokerto)',
				'Jateng Barat Utara (Pekalongan)',
				'Jateng Selatan (Magelang)',
				'Jateng Tengah (Salatiga)',
				'Jateng Timur Selatan (Solo)',
				'Jateng Timur Utara (Kudus)',
				'Jateng Utara (Semarang)',
				'Jatim Barat (Madiun)',
				'Jatim Selatan (Malang)',
				'Jatim Selatan Timur (Pasuruan)',
				'Jatim Suramadu (Surabaya)',
				'Jatim Tengah (Kediri)',
				'Jatim Tengah Timur (Sidoarjo)',
				'Jatim Timur (Jember)',
				'Jatim Utara (Gresik)',
				'Kalbar (Pontianak)',
				'Kalsel (Banjarmasin)',
				'Kalteng (Palangkaraya)',
				'Kaltimsel (Balikpapan)',
				'Kaltimteng (Samarinda)',
				'Kaltimut (Tarakan)',
				'Maluku (Ambon)',
				'Maluku Utara (Ternate)',
				'NTB (Mataram)',
				'NTT (Kupang)',
				'Papua (Jayapura)',
				'Papua Barat (Sorong)',
				'Sulsel (Makassar)',
				'Sulsel Barat (Pare-pare)',
				'Sulteng (Palu)',
				'Sultra (Kendari)',
				'Sulut (Menado)',
				'Sulut Selatan (Gorontalo)'
			);

		// dynamic year for filter
		$dynYear     = array();
		$defaultYear = date('Y');
		$initYear    = 2012;
		$dif         = $defaultYear - $initYear;
		
		for ($y=0; $y < $dif ; $y++) { 

			$initYear += 1;
			array_push($dynYear, $initYear); 

		}
		
		return View::make('reports.witel', array('witels'=>$witel,'dynYear'=>$dynYear));
	}

	/**
	 *  display detail total data tahunan order by type
	 */
	public function detailTahunan($type, $date)
	{
		
		$split       = explode('-', $date);
		$monthEx     = $split[0];
		$year        = $split[1];
		$packageName = array();
		$month       = '';
		
		if ($monthEx!=='total') {
			
			$month = $monthEx;

			if ($type=='activation') {

				$Fablog = Fablog::YearlyOrder('ACTIVATION', $month, $year)->join('request_status', function($join)
									{
										$join->on('id_fab','=','id');
									})
								->get();

				$statusOrder = 1;

			} elseif($type=='modification') {

				$Fablog = Fablog::YearlyOrder('MODIFICATION', $month, $year)->join('request_status', function($join)
									{
										$join->on('id_fab','=','id');
									})
								->get();

				$statusOrder = 1;

			} elseif($type=='suspension'){

				$Fablog = Fablog::YearlyOrder('SUSPENSION', $month, $year)->get();
				$statusOrder = 0;

			} elseif($type=='resumption'){

				$Fablog = Fablog::YearlyOrder('RESUMPTION', $month, $year)->get();
				$statusOrder = 0;

			} elseif($type=='termination'){

				$Fablog = Fablog::YearlyOrder('TERMINATION', $month, $year)->get();
				$statusOrder = 0;

			} elseif($type=='monthly') {

				$Fablog = Fablog::YearlyOrderByMonth($month, $year)->get();
				$statusOrder = 0;

			} 

		} else {

			if($type=='activation' || $type=='modification') {
				
				$Fablog = Fablog::YearlyOrderTotalByType(strtoupper($type), $year)
								->join('request_status', function($join)
									{
										$join->on('id_fab','=','id');
									})
								->get();

				$statusOrder = 1;


			} else {

				$Fablog      = Fablog::YearlyOrderTotalByType(strtoupper($type), $year)->get();
				$statusOrder = 0;

			}	
			
		}

		foreach ($Fablog as $log) {

			if ($log->PackageID=='1') {
			
				$paket = 'PSB';
			
			} elseif ($log->PackageID=='2') { //Ala carte Basic - 1
			
				$paket = 'Ala carte Basic - 1';
			
			} elseif($log->PackageID=='3') { // Ala carte Bronze - 2
			
				$paket = 'Ala carte Bronze - 2';
			
			} elseif($log->PackageID=='4') { // Ala carte Silver - 3
			
				$paket = 'Ala carte Silver - 3';
			
			} elseif($log->PackageID=='5') { // Ala carte Gold - 4
			
				$paket = 'Ala carte Gold - 4';
			
			} else {
			
				$paket = $log->PackageID;
			
			}

			array_push($packageName, $paket);
		}

		return View::make('reports.detailtahunan', array(
													'fab'         => $Fablog,
													'paket'       => $packageName,
													'year'        => $year,
													'statusOrder' => $statusOrder
													)
						);
	}

	/**
	 *  Display all data order by year
	 */
	public function totalTahunan($year)
	{
		
		$packageName = array();
		$Fablog      = Fablog::YearlyTotalAll($year)->get();
		$statusOrder = 0;
		
		foreach ($Fablog as $log) {

			if ($log->PackageID=='1') {
			
				$paket = 'PSB';
			
			} elseif ($log->PackageID=='2') { //Ala carte Basic - 1
			
				$paket = 'Ala carte Basic - 1';
			
			} elseif($log->PackageID=='3') { // Ala carte Bronze - 2
			
				$paket = 'Ala carte Bronze - 2';
			
			} elseif($log->PackageID=='4') { // Ala carte Silver - 3
			
				$paket = 'Ala carte Silver - 3';
			
			} elseif($log->PackageID=='5') { // Ala carte Gold - 4
			
				$paket = 'Ala carte Gold - 4';
			
			} else {
			
				$paket = $log->PackageID;
			
			}

			array_push($packageName, $paket);
		}

		return View::make('reports.detailtahunan', array(
													'fab'   => $Fablog,
													'paket' => $packageName,
													'year'  => $year,
													'statusOrder' => $statusOrder
													)
						);
	}

	/**
	 *  disiplay data order by id
	 */
	public function detailOrderAll($id)
	{

		$Fablog = Fablog::find($id);
		
		if ($Fablog->PackageID=='1') {
		
			$paket = 'PSB';
		
		} elseif ($Fablog->PackageID=='2') { //Ala carte Basic - 1
		
			$paket = 'Ala carte Basic - 1';
		
		} elseif($Fablog->PackageID=='3') { // Ala carte Bronze - 2
		
			$paket = 'Ala carte Bronze - 2';
		
		} elseif($Fablog->PackageID=='4') { // Ala carte Silver - 3
		
			$paket = 'Ala carte Silver - 3';
		
		} elseif($Fablog->PackageID=='5') { // Ala carte Gold - 4
		
			$paket = 'Ala carte Gold - 4';
		
		} else {
		
			$paket = $Fablog->PackageID;
		
		}

		return View::make('reports.detailorder', array('fab'=>$Fablog,'paket'=>$paket));
	}

	/**
	 *  display data order bulanan by periode (daily)
	 */
	public function detailBulanan($type, $date)
	{

		$year         = date('Y');
		$packageName  = array();
		$explode      = explode('-', $date);
		$defaultDays  = $explode[2];
		$defaultMonth = $explode[1];
		$defaultYear  = $explode[0];


		if ($type=='daily') {

			$Fablog = Fablog::MonthlyOrderByDay($temp, $defaultMonth, $defaultYear)->get();
			$statusOrder = 0;
		
		} else {
		
			if ($defaultDays<10) {

				$temp = '0'.$defaultDays;

			} else {

				$temp = $defaultDays;
				
			}

			if ($type=='activation' || $type='modification') {

				$Fablog = Fablog::MonthlyOrder(strtoupper($type), $temp, $defaultMonth, $defaultYear)
							->join('request_status', function($join)
								{
									$join->on('id_fab','=','id');
								})
							->get();

				$statusOrder = 1;

			} else {

				$Fablog = Fablog::MonthlyOrder(strtoupper($type), $temp, $defaultMonth, $defaultYear)->get();

				$statusOrder = 0;

			}
			
		
		}

		foreach ($Fablog as $log) {

			if ($log->PackageID=='1') {
			
				$paket = 'PSB';
			
			} elseif ($log->PackageID=='2') { //Ala carte Basic - 1
			
				$paket = 'Ala carte Basic - 1';
			
			} elseif($log->PackageID=='3') { // Ala carte Bronze - 2
			
				$paket = 'Ala carte Bronze - 2';
			
			} elseif($log->PackageID=='4') { // Ala carte Silver - 3
			
				$paket = 'Ala carte Silver - 3';
			
			} elseif($log->PackageID=='5') { // Ala carte Gold - 4
			
				$paket = 'Ala carte Gold - 4';
			
			} else {
			
				$paket = $log->PackageID;
			
			}

			array_push($packageName, $paket);
		}

		return View::make('reports.detailtahunan', array(
													'fab'   => $Fablog,
													'paket' => $packageName,
													'year'  => $year,
													'statusOrder' => $statusOrder
													)
						);

	}

	/**
	 *  Display detail data total bulanan by type and periode
	 */
	public function detailTotalBulanan($type, $date)
	{
		$split       = explode('-', $date);
		$paramYear   = $split[0];
		$paramMonth  = $split[1];
		
		$packageName = array();

		
		if($type=='activation' || $type=='modification') {
				
				$Fablog = Fablog::MonthlyOrderTotalByType(strtoupper($type), $paramMonth, $paramYear)
								->join('request_status', function($join)
									{
										$join->on('id_fab','=','id');
									})
								->get();

				$statusOrder = 1;

		} else {

			$Fablog      = Fablog::MonthlyOrderTotalByType(strtoupper($type), $paramMonth, $paramYear)->get();
			$statusOrder = 0;
		}

		foreach ($Fablog as $log) {

			if ($log->PackageID=='1') {
			
				$paket = 'PSB';
			
			} elseif ($log->PackageID=='2') { //Ala carte Basic - 1
			
				$paket = 'Ala carte Basic - 1';
			
			} elseif($log->PackageID=='3') { // Ala carte Bronze - 2
			
				$paket = 'Ala carte Bronze - 2';
			
			} elseif($log->PackageID=='4') { // Ala carte Silver - 3
			
				$paket = 'Ala carte Silver - 3';
			
			} elseif($log->PackageID=='5') { // Ala carte Gold - 4
			
				$paket = 'Ala carte Gold - 4';
			
			} else {
			
				$paket = $log->PackageID;
			
			}

			array_push($packageName, $paket);
		}

		return View::make('reports.detailtahunan', array(
													'fab'   => $Fablog,
													'paket' => $packageName,
													'year'  => $paramYear,
													'statusOrder' => $statusOrder
													)
						);

	}

	/**
	 *  Display total order by periode (month and year)
	 */
	public function totalBulanan($periode)
	{
		
		$split       = explode('-', $periode);
		$year        = $split[0];
		$month       = $split[1];
		$packageName = array();
		$statusOrder = 0;

		$Fablog = Fablog::YearlyOrderByMonth($month, $year)->get();
		
		foreach ($Fablog as $log) {

			if ($log->PackageID=='1') {
			
				$paket = 'PSB';
			
			} elseif ($log->PackageID=='2') { //Ala carte Basic - 1
			
				$paket = 'Ala carte Basic - 1';
			
			} elseif($log->PackageID=='3') { // Ala carte Bronze - 2
			
				$paket = 'Ala carte Bronze - 2';
			
			} elseif($log->PackageID=='4') { // Ala carte Silver - 3
			
				$paket = 'Ala carte Silver - 3';
			
			} elseif($log->PackageID=='5') { // Ala carte Gold - 4
			
				$paket = 'Ala carte Gold - 4';
			
			} else {
			
				$paket = $log->PackageID;
			
			}

			array_push($packageName, $paket);
		}

		return View::make('reports.detailtahunan', array(
													'fab'   => $Fablog,
													'paket' => $packageName,
													'year'  => $year,
													'statusOrder' => $statusOrder
													)
						);
	}

	/**
	 *  Process data from filter by date range
	 */
	public function filterByRange()
	{
		$dayFrom   = Input::get('dayFrom');
		$monthFrom = Input::get('monthFrom');
		$yearFrom  = Input::get('yearFrom');
		$dayTo     = Input::get('dayTo');
		$monthTo   = Input::get('monthTo');
		$yearTo    = Input::get('yearTo');
		
		if ($dayFrom<10) {

			$dayFrom = '0'.$dayFrom;
		
		}
		
		if ($dayTo<10) {
		
			$dayTo = '0'.$dayTo;
		
		}
		
		$dateFrom = $yearFrom.'-'.$monthFrom.'-'.$dayFrom;
		$dateTo   = $yearTo.'-'.$monthTo.'-'.$dayTo; 

		$activationCount   = Fablog::Searchbyrange('ACTIVATION', $dateFrom, $dateTo)->count();
		$modificationCount = Fablog::Searchbyrange('MODIFICATION', $dateFrom, $dateTo)->count();
		$suspensionCount   = Fablog::Searchbyrange('SUSPENSION', $dateFrom, $dateTo)->count();
		$resumptionCount   = Fablog::Searchbyrange('RESUMPTION', $dateFrom, $dateTo)->count();
		$terminationCount  = Fablog::Searchbyrange('TERMINATION', $dateFrom, $dateTo)->count();
		$total = $activationCount + $modificationCount + $suspensionCount + $resumptionCount + $terminationCount;
		
		// dynamic year for filter
		$dynYear  = array();
		$initYear = 2012;
		$dif      = date('Y') - $initYear;
		
		for ($y=0; $y < $dif ; $y++) { 

			$initYear += 1;
			array_push($dynYear, $initYear); 
			
		}				
		
		return View::make('reports.resultfilterbyrange', array(
															'dynYear'           => $dynYear,
															'activationCount'   => $activationCount,
															'modificationCount' => $modificationCount,
															'suspensionCount'   => $suspensionCount,
															'resumptionCount'   => $resumptionCount,
															'terminationCount'  => $terminationCount,
															'total'             => $total
														)
						);

	}

	/**
	 *  Process data from filter by witel
	 */
	public function filterWitel()
	{

		$witel     = Input::get('witel');
		$month     = Input::get('month');
		$year      = Input::get('year');
		$monthName = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');

		if($witel == '0' && $month == '0' && $year == '0') {

			return Redirect::to('report/witel')->with('message', 'Tidak ada data. Silahkan pilih witel dan periode order.');
		
		} elseif($witel == '0' && ($month !== '0' || $year !== '0')) {
		
			return Redirect::to('report/witel')->with('message', 'Tidak ada data. Silahkan pilih witel.');
		
		} elseif($witel !=='0' && ($month!=='0' && $year == '0')) {
		
			return Redirect::to('report/witel')->with('message', 'Tidak ada data. Tahun harus diisi.');
		
		} elseif($witel !== '0' && ($month == '0' && $year !== '0')) {
		
			$count['title'] = 'Rekap Witel '.$witel.' tahun '.$year;
			$count['year']  = $year;
			$count['month'] = '0';

			$searchActivation   = MasterAM::OrderWitelPeriod($witel, $count['month'], $count['year'], 'activation')->count();
			$searchModification = MasterAM::OrderWitelPeriod($witel, $count['month'], $count['year'], 'modification')->count();
			// $searchActivation = MasterAM::OrderWitelbyYear($witel, $year, 'alacarte')->count();

		} elseif($witel !=='0' && ($month=='0' && $year == '0')) {
		
			$count['title'] = 'Rekap Witel '.$witel;
			$count['year']  = '0';
			$count['month'] = '0';

			$searchActivation   = MasterAM::OrderWitelPeriod($witel, $count['month'], $count['year'], 'activation')->count();
			$searchModification = MasterAM::OrderWitelPeriod($witel, $count['month'], $count['year'], 'modification')->count();
			// $searchActivation = MasterAM::OrderWitelbyYear($witel, $year, 'alacarte')->count();
		
		} elseif($witel !=='0' && ($month !== '0' && $year !== '0')) {
			
			$count['title'] = 'Rekap Witel '.$witel.' periode '.$monthName[$month].' tahun '.$year;
			$count['year']  = $year;
			$count['month'] = $month;

			$searchActivation   = MasterAM::OrderWitelPeriod($witel, $count['month'], $count['year'], 'activation')->count();
			$searchModification = MasterAM::OrderWitelPeriod($witel, $count['month'], $count['year'], 'modification')->count();
		
		}

		$count['activation']   = $searchActivation;
		$count['modification'] = $searchModification;
		$count['witel']        = $witel;

		return View::make('reports.showwitel', array('data'=>$count)); 
	}

	public function detailFilterWitel($type, $witel, $month, $year)
	{
		$allData = MasterAM::OrderWitelPeriod(urldecode($witel), $month, $year, $type)
							->join('status_order_device', function($join)
									{
										$join->on('order_device.invoice','=','status_order_device.invoice');
									})
							->get();
		
		$title = 'All Order Witel '.urldecode($witel);

		return View::make('reports.allorderwitel', array('allData'=>$allData, 'title'=>$title));
	}

}
