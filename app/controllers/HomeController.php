<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showLogin()
	{
		return View::make('login');
	}

	public function postlogin() {
		
		$username = Input::get('username');
		$password = Input::get('password');	
		
		$user = User::where('username', $username)->first();

	    $this->message = '';

	    try {

	        if ( ! $user || ! Auth::attempt(array('username' => $user->username, 'password' => $password)) ) {
	            return Redirect::to('/')->with('message', 'username atau password salah');
	        }

	        Session::put('username', $user->username);
	        Session::put('role', $user->LEVEL);
	        Session::put('roleid', $user->role);
	        
	        if ($user->LEVEL == 'AM' || $user->role=='5') {
	        
	        	MasterAM::where('username', '=', $user->username)->update(array('last_login'=>date('Y-m-d H:i:s')));
	        	$am = MasterAM::select('id_am')->where('id_user','=',Auth::user()->ID)->first();
	        	Session::put('idam', $am['id_am']);
	        
	        } elseif($user->LEVEL == 'setter' || $user->role=='3') {
	        	
	        	$setter = MasterSetter::select('id_setter')->where('id_users','=',Auth::user()->ID)->first();
	        	Session::put('idsetter', $setter['id_setter']);
	        	Session::put('idvendor', $setter['id_vendor']);
	        
	        } elseif($user->LEVEL == 'mitra' || $user->role=='2') {
	        
	        	$mitra = UserVendor::where('id_user','=',Auth::user()->ID)->first();
	        	Session::put('idvendor', $mitra['id_vendor']);
	        
	        }
	        
	        return Redirect::intended('dashboard');

	    } catch (Exception $e) {
	        $this->message = $e->getMessage();
	       
	    }
	    	
	}

}