<?php

class AmManagementController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		if (Session::get('role') == 'CO AM') {

			$am = MasterAM::where('updated_by', '=', Session::get('username'))->get();

		} else {

			$am = MasterAM::all();
		
		}

		$orderCount = array();
		
		foreach ($am as $data)
		{

			$order = OrderDevice::where('updated_by', '=', $data->username)->get();		
			array_push($orderCount, count($order));
		
		}

        return View::make('ammanagements.index', array('ams'=>$am, 'count'=>$orderCount));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		$witel = array(
				'Bangka Belitung (Pangkal Pinang)',
				'Banten Barat (Serang)',
				'Banten Timur (Tangerang)',
				'Bengkulu',
				'Jabar Barat (Bogor)',
				'Jabar Barat Utara (Bekasi)',
				'Jabar Selatan (Sukabumi)',
				'Jabar Tengah (Bandung)',
				'Jabar Timsel (Tasikmalaya)',
				'Jabar Timur (Cirebon)',
				'Jabar Utara (Karawang)',
				'Jakarta Barat',
				'Jakarta Pusat',
				'Jakarta Selatan',
				'Jakarta Timur',
				'Jakarta Utara',
				'Jambi',
				'Lampung',
				'NAD (Aceh)',
				'Riau Daratan (Pekanbaru)',
				'Riau Kepulauan (Batam)',
				'Sumatera Barat (Padang)',
				'Sumatera Selatan (Palembang)',
				'Sumut Barat (Medan)',
				'Sumut Timur (Pematang Siantar)',
				'Bali Selatan (Denpasar)',
				'Bali Utara (Singaraja)',
				'DI Yogyakarta',
				'Jateng Barat Selatan (Purwokerto)',
				'Jateng Barat Utara (Pekalongan)',
				'Jateng Selatan (Magelang)',
				'Jateng Tengah (Salatiga)',
				'Jateng Timur Selatan (Solo)',
				'Jateng Timur Utara (Kudus)',
				'Jateng Utara (Semarang)',
				'Jatim Barat (Madiun)',
				'Jatim Selatan (Malang)',
				'Jatim Selatan Timur (Pasuruan)',
				'Jatim Suramadu (Surabaya)',
				'Jatim Tengah (Kediri)',
				'Jatim Tengah Timur (Sidoarjo)',
				'Jatim Timur (Jember)',
				'Jatim Utara (Gresik)',
				'Kalbar (Pontianak)',
				'Kalsel (Banjarmasin)',
				'Kalteng (Palangkaraya)',
				'Kaltimsel (Balikpapan)',
				'Kaltimteng (Samarinda)',
				'Kaltimut (Tarakan)',
				'Maluku (Ambon)',
				'Maluku Utara (Ternate)',
				'NTB (Mataram)',
				'NTT (Kupang)',
				'Papua (Jayapura)',
				'Papua Barat (Sorong)',
				'Sulsel (Makassar)',
				'Sulsel Barat (Pare-pare)',
				'Sulteng (Palu)',
				'Sultra (Kendari)',
				'Sulut (Menado)',
				'Sulut Selatan (Gorontalo)'
			);

        return View::make('ammanagements.create', array('propinsi' => $witel));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$am_name  = Input::get('am_name');
		$nik      = Input::get('nik');
		$email    = Input::get('email');
		$telepon  = Input::get('telepon');
		$alamat   = Input::get('alamat');
		$witel    = Input::get('witel');
		$kota_am  = Input::get('kota_am');
		$username = Input::get('am_username');
		$password = Input::get('am_password');

		Input::flash();

		// cek username availability
		$cekUsername = User::where('username', '=', $username)->first();
		
		if (empty($cekUsername->username)) {
			
			/**
			 * Store data AM, insert to tblusers first
			 * get inserted id and store to master AM
			 */
			$user           = new User;
			$user->username = $username;
			$user->password = Hash::make($password);
			$user->LEVEL    = 'AM';
			$user->role     = '5';
			$storeUser      = $user->save();

			if($storeUser) {

				$id                       = $user->ID;
				
				$masterAM               = new MasterAM;
				$masterAM->id_user      = $id;
				$masterAM->username     = $username;
				$masterAM->nama_lengkap = $am_name;
				$masterAM->nik          = $nik;
				$masterAM->email        = $email;
				$masterAM->telepon      = $telepon;
				$masterAM->alamat       = $alamat;
				$masterAM->witel        = $witel;
				$masterAM->updated_by   = Session::get('username');
				$storeMaster            = $masterAM->save();

				if ($storeMaster) {
				
					return Redirect::to('am')->with('message', 'Account Manager '.$am_name.' sudah ditambahkan.');
				
				} else {
				
					User::destroy($id);
					return Redirect::to('am')->with('message', 'Account Manager '.$am_name.' gagal ditambahkan.');
				
				}
			
			}

		} else {

			return Redirect::to('am/new')
					->with('message', 'username '.$username.' sudah ada. Silahkan gunakan username yang lain.')
					->withInput();
		
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		$count      = array();
		$masterAM   = MasterAM::find($id);
		$order      = OrderDevice::where('id_am', '=', $masterAM->id_am)->get();
		$countOrder = OrderDevice::where('id_am', '=', $masterAM->id_am)->count();
		
		foreach ($order as $temp) {
		
			$ordered = OrderedDevices::where('invoice','=',$temp->invoice)->count();
			array_push($count, $ordered);
		
		}

        return View::make('ammanagements.show', array(
													'am'          => $masterAM, 
													'order'       => $order, 
													'count'       => $countOrder, 
													'countDevice' => $count
        											));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
		$witel = array(
				'Bangka Belitung (Pangkal Pinang)',
				'Banten Barat (Serang)',
				'Banten Timur (Tangerang)',
				'Bengkulu',
				'Jabar Barat (Bogor)',
				'Jabar Barat Utara (Bekasi)',
				'Jabar Selatan (Sukabumi)',
				'Jabar Tengah (Bandung)',
				'Jabar Timsel (Tasikmalaya)',
				'Jabar Timur (Cirebon)',
				'Jabar Utara (Karawang)',
				'Jakarta Barat',
				'Jakarta Pusat',
				'Jakarta Selatan',
				'Jakarta Timur',
				'Jakarta Utara',
				'Jambi',
				'Lampung',
				'NAD (Aceh)',
				'Riau Daratan (Pekanbaru)',
				'Riau Kepulauan (Batam)',
				'Sumatera Barat (Padang)',
				'Sumatera Selatan (Palembang)',
				'Sumut Barat (Medan)',
				'Sumut Timur (Pematang Siantar)',
				'Bali Selatan (Denpasar)',
				'Bali Utara (Singaraja)',
				'DI Yogyakarta',
				'Jateng Barat Selatan (Purwokerto)',
				'Jateng Barat Utara (Pekalongan)',
				'Jateng Selatan (Magelang)',
				'Jateng Tengah (Salatiga)',
				'Jateng Timur Selatan (Solo)',
				'Jateng Timur Utara (Kudus)',
				'Jateng Utara (Semarang)',
				'Jatim Barat (Madiun)',
				'Jatim Selatan (Malang)',
				'Jatim Selatan Timur (Pasuruan)',
				'Jatim Suramadu (Surabaya)',
				'Jatim Tengah (Kediri)',
				'Jatim Tengah Timur (Sidoarjo)',
				'Jatim Timur (Jember)',
				'Jatim Utara (Gresik)',
				'Kalbar (Pontianak)',
				'Kalsel (Banjarmasin)',
				'Kalteng (Palangkaraya)',
				'Kaltimsel (Balikpapan)',
				'Kaltimteng (Samarinda)',
				'Kaltimut (Tarakan)',
				'Maluku (Ambon)',
				'Maluku Utara (Ternate)',
				'NTB (Mataram)',
				'NTT (Kupang)',
				'Papua (Jayapura)',
				'Papua Barat (Sorong)',
				'Sulsel (Makassar)',
				'Sulsel Barat (Pare-pare)',
				'Sulteng (Palu)',
				'Sultra (Kendari)',
				'Sulut (Menado)',
				'Sulut Selatan (Gorontalo)'
			);

		$am = MasterAM::find($id);

        return View::make('ammanagements.edit', array('propinsi' => $witel, 'am' => $am));
	
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$am_name  = Input::get('am_name');
		$nik      = Input::get('nik');
		$email    = Input::get('email');
		$telepon  = Input::get('telepon');
		$alamat   = Input::get('alamat');
		$witel    = Input::get('witel');
		$password = Input::get('am_password');

		Input::flash();

		$masterAM               = MasterAM::find($id);
		$masterAM->nama_lengkap = $am_name;
		$masterAM->nik          = $nik;
		$masterAM->email        = $email;
		$masterAM->telepon      = $telepon;
		$masterAM->alamat       = $alamat;
		$masterAM->witel        = $witel;
		$masterAM->updated_by   = Session::get('username');
		$updateMaster           = $masterAM->save();

		if (!empty($password)) {

			$user           = User::find($masterAM->id_user);
			$user->password = Hash::make($password);
			$updateAccount  = $user->save();	
	
		}

		if ($updateMaster || $updateAccount) {

			return Redirect::to('am')->with('message', 'Account Manager '.$am_name.' berhasil diperbarui.');
		
		}
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// get the user_id
		$am     = MasterAM::find($id);
		$nama   = $am->nama_lengkap;
		$userid = $am->id_user;

		MasterAM::destroy($id);
		User::destroy($userid);

		return Redirect::to('am')->with('message', 'Account Manager '.$nama.' sudah dihapus');
	
	}

}
