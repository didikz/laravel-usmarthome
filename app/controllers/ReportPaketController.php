<?php

class ReportPaketController extends BaseController
{

	public function indexTahunan()
	{
		if(Input::has('year')) {
			
			if(Input::get('year')!=='0') {
			
				$defaultYear = Input::get('year');
			
			} else {
			
				return Redirect::to('reportpaket/tahunan');
			
			}
			
		} else {
			
			$defaultYear = date('Y');
		
		}
		
		$month       = array('01','02','03','04','05','06','07','08','09','10','11','12');
		$paket       = array(
							'1' => 'PSB',
							'2' => 'Alacarte (Basic 1)',
							'3' => 'Alacarte (Bronze 2)',
							'4' => 'Alacarte (Silver 3)',
							'5' => 'Alacarte (Gold 4)',
							'6' => 'Online Recording (Bronze)',
							'7' => 'Online Recording (Silver)',
							'8' => 'Online Recording (Gold)',
							'9' => 'Paket Lainnya'
							);
		
		$jumlahPaket = count($paket);

		$data['title']       = 'Report Paket Tahun '.$defaultYear;
		$data['jumlahPaket'] = $jumlahPaket;
		$data['year']		 = $defaultYear;
		
		for($i=1; $i<= $jumlahPaket; $i++) {

			for ($j=0; $j < 12; $j++) { 
			
				$count[$i][$j] = Fablog::ReportPaketYearly($i, $month[$j], $defaultYear)->count();
			
			}
		
		}

		// dynamic year for filter
		$data['dynYear'] = array();
		$initYear        = 2012;
		$dif             = date('Y') - $initYear;
		
		for ($y=0; $y < $dif ; $y++) { 
			$initYear += 1;
			array_push($data['dynYear'], $initYear); 
		}

		// generate report and download report
		if (Input::has('download')) {
			
			if (Input::has('year')) {

				$yearReport = Input::get('year');	
			
			} else {
			
				$yearReport = $defaultYear;
			
			}

			$reportTitle = 'Rekap Paket Tahun '.$yearReport;
			
			$output = View::make('reportgenerated.pakettahunan', array(
																
																	'data'  => $data,
																	'count' => $count,
																	'month' => $month,
																	'year'  => $defaultYear,
																	'paket' => $paket,
																	'title' => $reportTitle
																
																	));

			$headers = array(

							'Pragma'                    => 'public',
							'Expires'                   => 'public',
							'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
							'Cache-Control'             => 'private',
							'Content-Type'              => 'application/vnd.ms-excel',
							'Content-Disposition'       => 'attachment; filename=rekappaket'.$yearReport.'.xls',
							'Content-Transfer-Encoding' => ' binary'
				
					    );

			return Response::make($output, 200, $headers);

		}

		return View::make('reportpaket.indextahunan', array(
														'data' => $data,
														'count' => $count,
														'month' => $month,
														'year'  => $defaultYear,
														'paket' => $paket ));
	}

	public function indexBulanan()
	{
		if(Input::has('year') && Input::has('month')) {
			
			if(Input::get('year')!=='0' && Input::get('month')!=='0') {
			
				$defaultMonth = Input::get('month');
				$defaultYear  = Input::get('year');
			
			} elseif(Input::get('month')!=='0' && Input::get('year')=='0') {
			
				$defaultMonth = Input::get('month');
				$defaultYear  = date('Y');
			
			} elseif(Input::get('month')=='0' && Input::get('year')!=='0') {
			
				$defaultMonth = date('m');
				$defaultYear  = Input::get('year');
			
			} else {
			
				return Redirect::to('reportpaket/bulanan');
			
			}

		} else {
			
			$defaultMonth = date('m');
			$defaultYear  = date('Y');
		
		}

		$defaultDays  = cal_days_in_month(CAL_GREGORIAN, $defaultMonth, $defaultYear);
		$dailyCount   = 0;
		$temp         = '';
		$monthName    = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
		$paket        = array(
							'1' => 'PSB',
							'2' => 'Alacarte (Basic 1)',
							'3' => 'Alacarte (Bronze 2)',
							'4' => 'Alacarte (Silver 3)',
							'5' => 'Alacarte (Gold 4)',
							'6' => 'Online Recording (Bronze)',
							'7' => 'Online Recording (Silver)',
							'8' => 'Online Recording (Gold)',
							'9' => 'Paket Lainnya'
							);
		
		$jumlahPaket         = count($paket);
		$data['title']       = 'Rekap Paket Bulan '.$monthName[$defaultMonth].' Tahun '.$defaultYear;
		$data['days']        = $defaultDays; 
		$data['month']       = $defaultMonth;
		$data['year']		 = $defaultYear;
		$data['jumlahPaket'] = $jumlahPaket;

		for($i=1; $i<= $jumlahPaket; $i++) {

			for ($j=1; $j <= $defaultDays; $j++) { 
			
				if ($j<10) {

					$temp = '0'.$j;
				
				} else {
				
					$temp = $j;
				
				}

				$count[$i][$j] = Fablog::ReportPaketMonthly($i, $temp, $defaultMonth, $defaultYear)->count();
			}
		
		}

		// dynamic year for filter
		$data['dynYear'] = array();
		$initYear        = 2012;
		$dif             = date('Y') - $initYear;
		
		for ($y=0; $y < $dif ; $y++) { 

			$initYear += 1;
			array_push($data['dynYear'], $initYear); 
		
		}

		// generate report and download
		if (Input::has('download')) {
			
			$reportTitle = 'Rekapitulasi paket bulan '.$monthName[$defaultMonth].' tahun '.$defaultYear;

			$output = View::make('reportgenerated.paketbulanan', array(
																	
																	'data'  => $data,
																	'count' => $count,
																	'paket' => $paket,
																	'title' => $reportTitle
																	
																	));

			$headers = array(

							'Pragma'                    => 'public',
							'Expires'                   => 'public',
							'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
							'Cache-Control'             => 'private',
							'Content-Type'              => 'application/vnd.ms-excel',
							'Content-Disposition'       => 'attachment; filename=rekappaket'.$monthName[$defaultMonth].'tahun'.$defaultYear.'.xls',
							'Content-Transfer-Encoding' => ' binary'
				
					    );

			return Response::make($output, 200, $headers);

		}	
		
		return View::make('reportpaket.indexbulanan',array(
															'data' => $data,
															'count' => $count,
															'paket' => $paket
															));
	}

	public function showTahunan($package, $period)
	{
		$paket  = array(
						'1' => 'PSB',
						'2' => 'Alacarte (Basic 1)',
						'3' => 'Alacarte (Bronze 2)',
						'4' => 'Alacarte (Silver 3)',
						'5' => 'Alacarte (Gold 4)',
						'6' => 'Online Recording (Bronze)',
						'7' => 'Online Recording (Silver)',
						'8' => 'Online Recording (Gold)',
						'9' => 'Paket Lainnya'
						);
		
			$explode = explode('-', $period);

			if(count($explode)==2) {

				$year  = $explode[0];
				$month = $explode[1];

				$fab   = Fablog::ReportPaketYearly($package, $month, $year)->get();

			} else {

				$fab = Fablog::ReportPaketYearlyAll($package, $explode[0])->get();

			}

		return View::make('reportpaket.showtahunan', array(
															'fab'=>$fab,
															'paket'=>$paket
															));
	}

	public function showBulanan($package, $period)
	{
		$paket  = array(
						'1' => 'PSB',
						'2' => 'Alacarte (Basic 1)',
						'3' => 'Alacarte (Bronze 2)',
						'4' => 'Alacarte (Silver 3)',
						'5' => 'Alacarte (Gold 4)',
						'6' => 'Online Recording (Bronze)',
						'7' => 'Online Recording (Silver)',
						'8' => 'Online Recording (Gold)',
						'9' => 'Paket Lainnya'
						);

		$explode = explode('-', $period);
		
		if(count($explode)==3) {
		
			$year  = $explode[0];
			$month = $explode[1];
			$day   = $explode[2];
			$fab   = Fablog::ReportPaketMonthly($package, $day, $month, $year)->get();
		
		} else {

			$fab = Fablog::ReportPaketYearly($package, $explode[1], $explode[0])->get();
		
		}

		return View::make('reportpaket.showtahunan', array(
															'fab'=>$fab,
															'paket'=>$paket
															));
	}

	public function detailPaketTahunan($id)
	{
		$Fablog = Fablog::find($id);
		
		if ($Fablog->PackageID=='1') {
	
			$paket = 'PSB';
	
		} elseif ($Fablog->PackageID=='2') { //Ala carte Basic - 1
	
			$paket = 'Ala carte Basic - 1';
	
		} elseif($Fablog->PackageID=='3') { // Ala carte Bronze - 2
	
			$paket = 'Ala carte Bronze - 2';
	
		} elseif($Fablog->PackageID=='4') { // Ala carte Silver - 3
	
			$paket = 'Ala carte Silver - 3';
	
		} elseif($Fablog->PackageID=='5') { // Ala carte Gold - 4
	
			$paket = 'Ala carte Gold - 4';
	
		} else {
	
			$paket = $Fablog->PackageID;
	
		}

		return View::make('reports.detailorder', array('fab'=>$Fablog,'paket'=>$paket));
	}

}
